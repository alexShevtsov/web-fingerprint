//var now = (function() {

/* Returns the number of milliseconds elapsed since either the browser
 * navigationStart event or the UNIX epoch, depending on availability.
 * Where the browser supports 'performance' we use that as it is more accurate
 * (microsoeconds will be returned in the fractional part) and more reliable as
 * it does not rely on the system time. Where 'performance' is not available, we
 * will fall back to Date().getTime().
 *
 * source: http://goo.gl/rn8VQ7
 *
 * array_access_exp
 *
 * access an array of array_size elements and returns the elapsed time of the
 * experiment.
 * 2 supported modes: (1) access the array attempting to create and recycle as
 * many cache entries as possible by doing memory accesses that do not exhibit
 * locality (accessesing array elements that can not be in the same cache line).
 * (2) access the array trying to read always elements from the same cache line
 * so as the accesses to be always in cache (cache hit).
 *
 * args:
 *         array_size:  (number)  The size of the array to be created and tested
 *         locality:    (boolean) TRUE if we want the accesses to exhibit
                                  locality (2), FALSE otherwise (1).
 * returns:
 *         the elapsed time of the experiment
 *
 */
var Zero_time;

 //----------------------------------------------------------------------------//
 //----------------------------CLEAR FIELDS-----------------------------------//
 //---------------------------------------------------------------------------//
function clear_fields(){
  document.getElementById("loc_mean").value = "";
  document.getElementById("loc_dev").value = "";
  document.getElementById("loc_var").value = "";
  document.getElementById("loc_med").value = "";
  document.getElementById("loc_rep").value = "";
  document.getElementById("loc_w").value = "";


  document.getElementById("nonl_mean").value = "";
  document.getElementById("nonl_dev").value = "";
  document.getElementById("nonl_var").value = "";
  document.getElementById("nonl_med").value = "";
  document.getElementById("nonl_rep").value = "";
  document.getElementById("nonl_w").value = "";

  document.getElementById("fib_mean").value = "";
  document.getElementById("fib_dev").value = "";
  document.getElementById("fib_var").value = "";
  document.getElementById("fib_med").value = "";
  document.getElementById("fib_rep").value = "";
  document.getElementById("fib_w").value = "";

  document.getElementById("nonz_totalf").value = "";
  document.getElementById("nonz_fobject").value = "";
  document.getElementById("nonz_finaltime").value = "";
  document.getElementById("nonz_min").value = "";
  document.getElementById("nonz_max").value = "";
  document.getElementById("nonz_avg").value = "";
  document.getElementById("nonz_stats").value = "";
  document.getElementById("nonz_start").value = "";
  document.getElementById("nonz_step").value = "";

  document.getElementById("z_totalf").value = "";
  document.getElementById("z_fobject").value = "";
  document.getElementById("z_finaltime").value = "";
  document.getElementById("z_min").value = "";
  document.getElementById("z_max").value = "";
  document.getElementById("z_avg").value = "";
  document.getElementById("z_stats").value = "";
  document.getElementById("z_start").value = "";
  document.getElementById("z_step").value = "";

  document.getElementById("cube_total").value = "";
  document.getElementById("cube_fps").value = "";
  document.getElementById("cube_ft").value = "";
  document.getElementById("cube_time").value = "";
  document.getElementById("cube_add").value = "";

  document.getElementById("cpu_cor").value = "";
  document.getElementById("cpu_w").value = "";
  document.getElementById("cpu_s").value = "";
}

var result_bar = document.getElementById("results");
result_bar.innerHTML = "";
var state_bar = document.getElementById("state");

var str2DOMElement = function(html) {
    var frame = document.createElement('iframe');
    frame.style.display = 'none';
    document.body.appendChild(frame);
    frame.contentDocument.open();
    frame.contentDocument.write(html);
    frame.contentDocument.close();
    var el = frame.contentDocument.body.firstChild;
    document.body.removeChild(frame);
    return el;
}
//----------------------------------------------------------------------------//
//----------------------------END OF CLEAR FIELDS-----------------------------//
//----------------------------------------------------------------------------//


//----------------------------------------------------------------------------//
//------------------------------------GPU TEST--------------------------------//
//----------------------------------------------------------------------------//
function GPU_TEST(zbuf, callafter, start, fast_mode){
  //variables to create the scene, objects, camera
  state_bar.innerHTML = "GPU test";
  // result_bar.innerHTML = "";

  var container;
  var camera, renderer;
  var scene = new THREE.Scene();
  var geometry, material;
  var GLOBAL_START_START, GLOBAL_START_END, GLOBAL_START_STEP;
  var GLOBAL_STEP_START, GLOBAL_STEP_END, GLOBAL_STEP_STEP;
  if(zbuf){
    GLOBAL_START_START = parseInt(document.getElementById("g2start_start").value);
    GLOBAL_START_END = parseInt(document.getElementById("g2start_end").value);
    GLOBAL_START_STEP = parseInt(document.getElementById("g2start_step").value);
    GLOBAL_STEP_START = parseInt(document.getElementById("g2step_start").value);
    GLOBAL_STEP_END = parseInt(document.getElementById("g2step_end").value);
    GLOBAL_STEP_STEP = parseInt(document.getElementById("g2step_step").value);
  }else{
    GLOBAL_START_START = parseInt(document.getElementById("gstart_start").value);
    GLOBAL_START_END = parseInt(document.getElementById("gstart_end").value);
    GLOBAL_START_STEP = parseInt(document.getElementById("gstart_step").value);
    GLOBAL_STEP_START = parseInt(document.getElementById("gstep_start").value);
    GLOBAL_STEP_END = parseInt(document.getElementById("gstep_end").value);
    GLOBAL_STEP_STEP = parseInt(document.getElementById("gstep_step").value);
  }
  //check if it start from zero (0)
  if(GLOBAL_START_START == 0){
    GLOBAL_START_START = GLOBAL_START_STEP;
  }
  if(GLOBAL_STEP_START == 0){
    GLOBAL_STEP_START = GLOBAL_STEP_STEP;
  }
  if(fast_mode){
    GLOBAL_START_STEP = 1500;
  }

  if(start <= GLOBAL_START_START){
    start = GLOBAL_START_START;
    clear_fields();
  }

  var timeframe = 0, calcFrame = 0;	//these are used for timing the scene renders
  var frameNum = 0;			//current frame number of the benchmark
  var endFrame = 100;			//'end' of the benchmark, which is extended if more data is needed
  var badData = 10;			//the # of data points we truncate from the beginning of the data to cut out bad data
  var numSquares = 0;			//keeps track of number of squares in the scene
  var maxYaxis = 100;			//max render time (ms) shown on the graph
  var requestAnim;		//holds the animation frame request
  var frameNumSquares = new Array(endFrame - badData);
  var frameTime = new Array(endFrame - badData);
  var inc = 0.1;			//used in AddShape() for moving future objects
  var addObjects = 1;		//number of objects to add per frame

  var runTextCount = 0;
  var min, max, mean;		//variables for statistics at the bottom fo the page
  var gl; //used to access WebGL context (renderer.context)

  // Square shape objects
  var x = 5000;
  var y = 5000;
  var squareShape = new THREE.Shape();
  squareShape.moveTo( 0, 0 );
  squareShape.lineTo( 0, y );
  squareShape.lineTo( x, y );
  squareShape.lineTo( x, 0 );
  squareShape.lineTo( 0, 0 );


  //Check if browser supports WebGL
   if (Detector.webgl) {
     init();
     animate();
   } else {
     //if WebGl not found we just go to the next step with out submit data
     //we show one pop-up with the WedGl error messange
     var warning = Detector.getWebGLErrorMessage();
     alert("WebGl not detected. "+ warning);
     if(callafter){
       cpu_cores(true);
     }
   }


  //setup the back end (camera, renderer, stats.js)
  function init()
  {
    container = document.getElementById("ggpu");

    //setup camera
    camera = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 1, 10000 );
    camera.position.y = window.innerHeight / 2;
    camera.position.z = 1000;
    camera.position.set( 2500, 2500, 500 );

    //setup and add renderer
    renderer = new THREE.WebGLRenderer();
    renderer.setSize( window.innerWidth, window.innerHeight );
    renderer.sortObjects = false;
    renderer.sortElements = false;
    container.appendChild( renderer.domElement );
    gl = renderer.context;

    //set false to disable Z-Buffering
  //  renderer.setDepthTest(false);
    renderer.setDepthTest(zbuf);
    // Testing other methods of changing depth test
    //renderer.context.depthMask( false );
    //gl.disable(gl.DEPTH_TEST);
    //console.log(gl.getParameter(gl.DEPTH_TEST));

    //add stats
    // stats = new Stats();
    // stats.getDomElement().style.position = 'absolute';
    // stats.getDomElement().style.left = '0px';
    // stats.getDomElement().style.top = '0px';
    // document.body.appendChild( stats.getDomElement() );

    //my--------------------my//

    var i;

    for(i = 0; i < start; i++)
    {
      addShape( squareShape, '#'+Math.floor(Math.random()*16777215).toString(16), 150, 100, -20, 0, 0, 0, 1 );
    }
    //my--------------------my//
  }


  //Using the passed in parameters, add the shape to the scene
  function addShape( shape, color, x, y, z, rx, ry, rz, s )
  {
    var geometry = new THREE.ShapeGeometry( shape );
    var material = new THREE.MeshBasicMaterial( { color: color} );

    var mesh = new THREE.Mesh( geometry, material );
    mesh.position.set( x, y, z + (frameNum*inc) );		//move each new object closer to the camera
    mesh.rotation.set( rx, ry, rz );
    scene.add( mesh );
    mesh.scale.set( s, s, s );

    numSquares = numSquares+1;
  }


  //This function: requests the next animation frame, records the render times,
  //checks for end of benchmark, adds more objects, updates stats.js,
  //renders the scene, and verifies that the rendering is finished before continuing.
  function animate()
  {
    var i;
    //creation of the dynamic 'Running test' text
    if(frameNum % 5 == 0)	//update every 5 frames
    {
      if(runTextCount >= 10)	//reset the text after 10 periods
      {
      	state_bar.innerHTML = "GPU test"
      	runTextCount = 0;
      }
      else	//add another period
      {
      	state_bar.innerHTML = state_bar.innerHTML + ".";
      	runTextCount = runTextCount + 1;
      }
    }

    // note: three.js includes requestAnimationFrame shim
    requestAnim = requestAnimationFrame( animate );

    //record the time between each frame
    frameNum = frameNum+1;
    calcFrame = performance.now() - timeframe;
    timeframe = performance.now();

    frameNumSquares[frameNum] = numSquares;
    frameTime[frameNum] = calcFrame;

    //check if we are at the end of the program
    if(frameNum >= endFrame)
    {

      if( extendGraph() )		//check if it needs to extend length of benchmark
      {
        endFrame += 60
      }
      else	//benchmark is complete
      {
        //hide running text
        //runText.style.visibility = 'hidden';

        //clean up and create graph
        cancelAnimationFrame(requestAnim);
        removeSceneObjects();
        fixData();
        calcStats();
        //smoothData();
        //createGraph();

        //resulting statistics message
        // finishText.innerHTML = "<b>---Results---</b>"
        // 						+"<br>Total Frames rendered: " +frameNum
        // 						+"<br>Objects in final frame: " +numSquares
        // 						+"<br>Final frame render time: " +frameTime[frameNum]
        // 						+"<br><br>"
        // 						+"<b>---Render Time Data---</b>"
        // 						+"<br>Mininum: " +min
        // 						+"<br>Maximum: " +max
        // 						+"<br>Average: " +mean;

        var step, totalf, fobject, finaltime, gmin, gmax, gavg, gstats, gstart, gstep;

        if(zbuf){//case used z buffer
          totalf = document.getElementById("z_totalf");
          fobject = document.getElementById("z_fobject");
          finaltime = document.getElementById("z_finaltime");
          gmin = document.getElementById("z_min");
          gmax = document.getElementById("z_max");
          gavg = document.getElementById("z_avg");
          gstats = document.getElementById("z_stats");
          gstart = document.getElementById("z_start");
          gstep = document.getElementById("z_step");
          //step = parseInt(document.getElementById("g2step").value);
          // gstep.value = document.getElementById("g2step").value;
          // gstart.value = document.getElementById("g2start").value;
        }else{//case without zbuffer
          totalf = document.getElementById("nonz_totalf");
          fobject = document.getElementById("nonz_fobject");
          finaltime = document.getElementById("nonz_finaltime");
          gmin = document.getElementById("nonz_min");
          gmax = document.getElementById("nonz_max");
          gavg = document.getElementById("nonz_avg");
          gstats = document.getElementById("nonz_stats");
          gstart = document.getElementById("nonz_start");
          gstep = document.getElementById("nonz_step");
          //step = parseInt(document.getElementById("gstep").value);
          // gstep.value = document.getElementById("gstep").value;
          // gstart.value = document.getElementById("gstart").value;
        }
        gstart.value = start;
        // step = GLOBAL_STEP_START;
        /*result_bar.innerHTML ="Total Frames rendered: " +frameNum
                    +"<br>Objects in final frame: " +numSquares
                    +"<br>Final frame render time: " +frameTime[frameNum]
                    +"<br>Mininum: " + min
                    +"<br>Maximum: " + max
                    +"<br>Average: " + mean;*/
        // for(step = GLOBAL_STEP_START; step <= GLOBAL_STEP_END; step += GLOBAL_STEP_STEP){
          totalf.value = frameNum;
          fobject.value = numSquares;
          finaltime.value = frameTime[frameNum];
          gmin.value = min
          gmax.value = max
          gavg.value = mean
          gstep.value = GLOBAL_STEP_START;
          gstats.value = "";
          // console.log("Frames rendered: " + frameNum);
          // console.log("Objects in final frame: " + numSquares);
          // console.log("Final frame render time: " + frameTime[frameNum]);
          // console.log("Minimum: " + min);
          // console.log("Maximum: " + max);
          // console.log("Average: " + mean);
          // console.log("Step: " + GLOBAL_STEP_START);
          for(i=0; i<frameNumSquares.length; i+=step){
            /*result_bar.innerHTML +="<br> frame:"+i+
                                  " Objects:"+frameNumSquares[i]+
                                  " render time:"+frameTime[i]+" in milliseconds";*/
            gstats.value += i+":"+frameNumSquares[i]+":"+frameTime[i]+"|";
          }
          // console.log("Data: " + gstats.value);
          //submit data
          document.getElementById("Sub_button").click();

        // }


        start += GLOBAL_START_STEP;
        if( start <= GLOBAL_START_END ){
          // start += GLOBAL_START_STEP;
          GPU_TEST(zbuf, callafter, start);
        }else{
          if(callafter){
            if(zbuf){
              result_bar.innerHTML = "<h3>Normal test done. Runing time:" +
                                        (performance.now() - Zero_time) +
                                        " in miliseconds. </h3>";
            }else{
              GPU_TEST(true, true, -1);
            }
          }

        }
        //   if(callafter){
        //     if(zbuf){
        //       state_bar.innerHTML = "Big test is done!";
        //       // GPU_CUBES(true,-1,-1);
        //     }else{
        //       GPU_TEST(true,true,-1);
        //     }
        //   }else{
        //     state_bar.innerHTML = "Done";
        //   }
        // }
        //tipText.innerHTML = "<b>Note:</b> you can mouse over the line graph to see <br> data at any part of the benchmark.";

        //create the blue background color for the finished screen
        //addShape( squareShape, '#6495ED', 150, 100, 0, 0, 0, 0, 1 );
      }
    }
    else	//keep adding more objects
    {
      var i;
      for(i = 0; i < addObjects; i++)
      {
        addShape( squareShape, '#'+Math.floor(Math.random()*16777215).toString(16), 150, 100, -20, 0, 0, 0, 1 );
      }
    }

    //stats.update();

    renderer.render( scene, camera );

    //ensure renderer is finished before moving on to the next frame
    renderer.context.finish();
  }


  //returns true if we need to extend the benchmark in order to see a trend
  function extendGraph()
  {
    var i;
    var frameAvg = 0;

    if(frameNum > 5)
    {
      //get the average render time of the last 5 frames
      for(i = frameNum; i > frameNum-5; i--)
      {
        frameAvg += frameTime[i];
      }

      frameAvg = frameAvg / (frameNum-i);
    }

    //if the average render time is under the limit, continue benchmarking
    if(frameAvg <= 55)
      return true;
    else	//time to end the benchmark
      return false;
  }


  //Calculate statistics for the results at the end. It takes the data
  //from the frameTime array that was built during the benchmark.
  function calcStats()
  {
    var i, sum = 0;

    for(i = 0; i < frameTime.length; i++)
    {
      //check for new minimum
      if(frameTime[i] < min || !min)
      {
        min = frameTime[i];
      }

      //check for new maximum
      if(frameTime[i] > max || !max)
      {
        max = frameTime[i];
      }

      sum = sum + frameTime[i];
    }

    mean = sum/i;
  }

  //Protects the data from going out of the graph, and remove bad data
  //from the beginning of the test.
  function fixData()
  {
    var i;

    //replace original arrays with the identical array minus the first 10 values
    frameTime = frameTime.slice(badData, endFrame+1);
    frameNumSquares = frameNumSquares.slice(badData, endFrame+1);
    frameNum = frameNum - badData;

    //keep the graph data from shooting off of the graph when there are outliers
    for(i = 1; i < endFrame; i++)
    {
      if (frameTime[i] > maxYaxis)
      {
        frameTime[i] = maxYaxis;
      }
    }
  }


  //Smooth the data on the graph to make it easier to see the overall graph trend.
  function smoothData()
  {
    var i;
    for(i = 3; i < endFrame; i++)
    {
      //if the current render time value is a lot bigger than the previous
      //then just set it to the average time of the previous 3 frames
      if(frameTime[i] >= (2* frameTime[i-1]) )
      {
        frameTime[i] = ( (frameTime[i] + frameTime[i-1] + frameTime[i-2]) / 3);
      }
    }
  }


  //remove all objects from the scene
  function removeSceneObjects()
  {
    var obj, i;
    for ( i = scene.children.length - 1; i >= 0 ; i -- )
    {
      obj = scene.children[i];
      if (obj !== camera)
      {
        scene.remove(obj);
      }
    }
  }
}
//----------------------------------------------------------------------------//
//-------------------------------END OF GPU TEST------------------------------//
//----------------------------------------------------------------------------//



//----------------------------------------------------------------------------//
//-------------------------------FAST GPU TEST--------------------------------//
//----------------------------------------------------------------------------//
function GPU_FAST_TEST(zbuf, callafter, start){
  //variables to create the scene, objects, camera
  state_bar.innerHTML = "GPU test";

  var container;
  var camera, renderer;
  var scene = new THREE.Scene();
  var geometry, material;
  var GLOBAL_START_START, GLOBAL_START_END;
  if(zbuf){
    GLOBAL_START_START = parseInt(document.getElementById("g2start_start").value);
    GLOBAL_START_END = parseInt(document.getElementById("g2start_end").value);
  }else{
    GLOBAL_START_START = parseInt(document.getElementById("gstart_start").value);
    GLOBAL_START_END = parseInt(document.getElementById("gstart_end").value);
  }
  //check if it start from zero (0)
  if(GLOBAL_START_START == 0){
    GLOBAL_START_START = GLOBAL_START_STEP;
  }
  if(start <= GLOBAL_START_START){
    start = GLOBAL_START_START;
  }
  clear_fields();
  var start_time;
  var timeframe = 0, calcFrame = 0;	//these are used for timing the scene renders
  var frameNum = 0;			//current frame number of the benchmark
  var endFrame = 100;			//'end' of the benchmark, which is extended if more data is needed
  var badData = 10;			//the # of data points we truncate from the beginning of the data to cut out bad data
  var numSquares = 0;			//keeps track of number of squares in the scene
  var maxYaxis = 100;			//max render time (ms) shown on the graph
  var requestAnim;		//holds the animation frame request
  var frameNumSquares = new Array(endFrame - badData);
  var frameTime = new Array(endFrame - badData);
  var inc = 0.1;			//used in AddShape() for moving future objects
  var addObjects = 1;		//number of objects to add per frame

  var runTextCount = 0;
  var min, max, mean;		//variables for statistics at the bottom fo the page
  var gl; //used to access WebGL context (renderer.context)

  // Square shape objects
  var x = 5000;
  var y = 5000;
  var squareShape = new THREE.Shape();
  squareShape.moveTo( 0, 0 );
  squareShape.lineTo( 0, y );
  squareShape.lineTo( x, y );
  squareShape.lineTo( x, 0 );
  squareShape.lineTo( 0, 0 );

  //-----------------------Code starts here-----------------------

  //Check if browser supports WebGL
   if (Detector.webgl) {
     start_time = performance.now();
     init();
     animate();
   } else {
     //if WebGl not found we just go to the next step with out submit data
     //we show one pop-up with the WedGl error messange
     var warning = Detector.getWebGLErrorMessage();
     alert("WebGl not detected. "+ warning);
     if(callafter){
       cpu_cores(true);
     }
   }


  //setup the back end (camera, renderer, stats.js)
  function init(){
    container = document.getElementById("ggpu");

    //setup camera
    camera = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 1, 10000 );
    camera.position.y = window.innerHeight / 2;
    camera.position.z = 1000;
    camera.position.set( 2500, 2500, 500 );

    //setup and add renderer
    renderer = new THREE.WebGLRenderer();
    renderer.setSize( window.innerWidth, window.innerHeight );
    renderer.sortObjects = false;
    renderer.sortElements = false;
    container.appendChild( renderer.domElement );
    gl = renderer.context;

    //set Z-buffer value (True or False)
    renderer.setDepthTest(zbuf);

    var i;
    for(i = 0; i < start; i++)
      addShape( squareShape, '#'+Math.floor(Math.random()*16777215).toString(16), 150, 100, -20, 0, 0, 0, 1 );
  }


  //Using the passed in parameters, add the shape to the scene
  function addShape( shape, color, x, y, z, rx, ry, rz, s ){
    var geometry = new THREE.ShapeGeometry( shape );
    var material = new THREE.MeshBasicMaterial( { color: color} );

    var mesh = new THREE.Mesh( geometry, material );
    mesh.position.set( x, y, z + (frameNum*inc) );		//move each new object closer to the camera
    mesh.rotation.set( rx, ry, rz );
    scene.add( mesh );
    mesh.scale.set( s, s, s );

    numSquares = numSquares+1;
  }


  //This function: requests the next animation frame, records the render times,
  //checks for end of benchmark, adds more objects, updates stats.js,
  //renders the scene, and verifies that the rendering is finished before continuing.
  function animate(){
    var i;
    //creation of the dynamic 'Running test' text
    if(frameNum % 5 == 0){	//update every 5 frames
      if(runTextCount >= 10){	//reset the text after 10 periods
      	state_bar.innerHTML = "GPU test"
      	runTextCount = 0;
      }else{	//add another period
      	state_bar.innerHTML = state_bar.innerHTML + ".";
      	runTextCount = runTextCount + 1;
      }
    }

    // note: three.js includes requestAnimationFrame shim
    requestAnim = requestAnimationFrame( animate );

    //record the time between each frame
    frameNum = frameNum+1;
    calcFrame = performance.now() - timeframe;
    timeframe = performance.now();

    frameNumSquares[frameNum] = numSquares;
    frameTime[frameNum] = calcFrame;

    //check if time is out of 10 seconds already
    if((performance.now() - start_time) >= 10000){
        end_of_benchmark();
    }else{
      //check if all frames is alredy rendered
      if(frameNum >= endFrame){
        endFrame += 60
      }else{	//keep adding more objects
        var i;
        for(i = 0; i < addObjects; i++)
          addShape( squareShape, '#'+Math.floor(Math.random()*16777215).toString(16), 150, 100, -20, 0, 0, 0, 1 );
      }
    }
    //stats.update();
    renderer.render( scene, camera );
    //ensure renderer is finished before moving on to the next frame
    renderer.context.finish();
  }

  function end_of_benchmark(){
    //clean up and create graph
    cancelAnimationFrame(requestAnim);
    removeSceneObjects();
    fixData();
    calcStats();

    var step, totalf, fobject, finaltime, gmin, gmax, gavg, gstats, gstart, gstep;

    if(zbuf){//case used z buffer
      totalf = document.getElementById("z_totalf");
      fobject = document.getElementById("z_fobject");
      finaltime = document.getElementById("z_finaltime");
      gmin = document.getElementById("z_min");
      gmax = document.getElementById("z_max");
      gavg = document.getElementById("z_avg");
      gstats = document.getElementById("z_stats");
      gstart = document.getElementById("z_start");
      gstep = document.getElementById("z_step");
    }else{//case without zbuffer
      totalf = document.getElementById("nonz_totalf");
      fobject = document.getElementById("nonz_fobject");
      finaltime = document.getElementById("nonz_finaltime");
      gmin = document.getElementById("nonz_min");
      gmax = document.getElementById("nonz_max");
      gavg = document.getElementById("nonz_avg");
      gstats = document.getElementById("nonz_stats");
      gstart = document.getElementById("nonz_start");
      gstep = document.getElementById("nonz_step");
    }
    //put data in the form text boxes
    gstart.value = start;
    totalf.value = frameNum;
    fobject.value = numSquares;
    finaltime.value = frameTime[frameNum];
    gmin.value = min
    gmax.value = max
    gavg.value = mean
    gstep.value = 50;
    gstats.value = "";
    // console.log("Frames rendered: " + frameNum);
    // console.log("Objects in final frame: " + numSquares);
    // console.log("Final frame render time: " + frameTime[frameNum]);
    // console.log("Minimum: " + min);
    // console.log("Maximum: " + max);
    // console.log("Average: " + mean);
    // console.log("Step: " + GLOBAL_STEP_START);
    for(i=0; i<frameNumSquares.length; i+=step)
      gstats.value += i+":"+frameNumSquares[i]+":"+frameTime[i]+"|";
    //submit data
    document.getElementById("Sub_button").click();
    start += 1500;
    if( start <= GLOBAL_START_END ){
      GPU_FAST_TEST(zbuf, callafter, start);
    }else if(callafter){
        if(zbuf){
          result_bar.innerHTML = "<h3>Fast test done. Runing time:" +
                                    (performance.now() - Zero_time) +
                                    " in miliseconds. </h3>";
        }else{
          GPU_FAST_TEST(true,true,-1);
        }
      }
  }

  //Calculate statistics for the results at the end. It takes the data
  //from the frameTime array that was built during the benchmark.
  function calcStats(){
    var i, sum = 0;

    for(i = 0; i < frameTime.length; i++){
      //check for new minimum
      if(frameTime[i] < min || !min)
        min = frameTime[i];

      //check for new maximum
      if(frameTime[i] > max || !max)
        max = frameTime[i];

      sum = sum + frameTime[i];
    }

    mean = sum/i;
  }

  //Protects the data from going out of the graph, and remove bad data
  //from the beginning of the test.
  function fixData(){
    var i;

    //replace original arrays with the identical array minus the first 10 values
    frameTime = frameTime.slice(badData, endFrame+1);
    frameNumSquares = frameNumSquares.slice(badData, endFrame+1);
    frameNum = frameNum - badData;

    //keep the graph data from shooting off of the graph when there are outliers
    for(i = 1; i < endFrame; i++){
      if (frameTime[i] > maxYaxis)
        frameTime[i] = maxYaxis;

    }
  }


  //Smooth the data on the graph to make it easier to see the overall graph trend.
  function smoothData(){
    var i;
    for(i = 3; i < endFrame; i++){
      //if the current render time value is a lot bigger than the previous
      //then just set it to the average time of the previous 3 frames
      if(frameTime[i] >= (2* frameTime[i-1]) )
        frameTime[i] = ( (frameTime[i] + frameTime[i-1] + frameTime[i-2]) / 3);

    }
  }


  //remove all objects from the scene
  function removeSceneObjects(){
    var obj, i;
    for ( i = scene.children.length - 1; i >= 0 ; i -- ){
      obj = scene.children[i];
      if (obj !== camera)
        scene.remove(obj);

    }
  }

}
//----------------------------------------------------------------------------//
//----------------------------END OF FAST GPU TEST----------------------------//
//----------------------------------------------------------------------------//


//
// function array_access_exp(array_size, locality)
// {
//   var i = 0, cnt=0;
//   var N = array_size;
//   var myArray = new Array (N);
//
//
//   for (i=0; i< N; i++) {
//     myArray[i] = i;
//   }
//
//    // alert(myArray.length);
//
//   // No locality accesses case: Try to always produce cache misses
//
//   if (locality == false){
//     // get a timestamp before running the test
//     var mark_start = performance.now();
//
//     for (i=0; i < N; i+=64) {
//       var elem = myArray[i];
//       cnt += elem;
//     }
//
//     // get another timestamp after the test is complete
//     var mark_end = performance.now();
//   }
//
//   // Locality accesses case: Try to always produce cache hits
//
//   else{
//     // get a timestamp before running the test
//     var mark_start = performance.now();
//
//     for (i=0; i < N; i+=64) {
//       var elem = myArray[0];
//       cnt += elem;
//     }
//
//
//     // get another timestamp after the test is complete
//     var mark_end = performance.now();
//   }
//
//   myArray = 0;
//
//   // return the elapsed time
//   return (mark_end - mark_start);
// }
//
// function median(values) {
//     values.sort( function(a,b) {return a - b;} );
//     var half = Math.floor(values.length/2);
//     if(values.length % 2)
//         return values[half];
//     else
//         return (values[half-1] + values[half]) / 2.0;
// }
//
// stats = function(a) {
//   var r = {mean: 0, variance: 0, deviation: 0, median: 0}, t = a.length;
//   for(var m, s = 0, l = t; l--; s += a[l]);
//   for(m = r.mean = s / t, l = t, s = 0; l--; s += Math.pow(a[l] - m, 2));
//   r.median = median(a);
//   return r.deviation = Math.sqrt(r.variance = s / t), r;
// }
//

//----------------------------------------------------------------------------//
//---------------------------------FIBONACII----------------------------------//
//----------------------------------------------------------------------------//
function run_fibonacci(callafter, fast_mode){
  var times = [];
  state_bar.innerHTML = "Run fibonacci...";
  // result_bar.innerHTML = "";
  var GLOBAL_LOOP_START = parseInt(document.getElementById("fib_loop_start").value);
  var GLOBAL_LOOP_STEP = parseInt(document.getElementById("fib_loop_step").value);
  var GLOBAL_LOOP_END = parseInt(document.getElementById("fib_loop_end").value);
  var GLOBAL_DEEP_START = parseInt(document.getElementById("fib_deep_start").value);
  var GLOBAL_DEEP_END = parseInt(document.getElementById("fib_deep_end").value);
  var GLOBAL_DEEP_STEP = parseInt(document.getElementById("fib_deep_step").value);
  //check if it starts from zero (0)
  if(GLOBAL_LOOP_START == 0){
    GLOBAL_LOOP_START = GLOBAL_LOOP_STEP;
  }
  if(fast_mode){
    GLOBAL_DEEP_STEP = 500;
    GLOBAL_LOOP_STEP = 500;
  }
  if(GLOBAL_DEEP_START == 0){
    GLOBAL_DEEP_START = GLOBAL_DEEP_STEP;
  }

  stats = function(a) {
    var r = {mean: 0, variance: 0, deviation: 0, median: 0}, t = a.length;
    for(var m, s = 0, l = t; l--; s += a[l]);
    for(m = r.mean = s / t, l = t, s = 0; l--; s += Math.pow(a[l] - m, 2));

    function median(values) {
        values.sort( function(a,b) {return a - b;} );
        var half = Math.floor(values.length/2);
        if(values.length % 2)
            return values[half];
        else
            return (values[half-1] + values[half]) / 2.0;
    }
    r.median = median(a);
    return r.deviation = Math.sqrt(r.variance = s / t), r;
  }

  var loop;
  var deep;
  clear_fields();
  for(loop = GLOBAL_LOOP_START; loop <= GLOBAL_LOOP_END; loop += GLOBAL_LOOP_STEP){
    for(deep = GLOBAL_DEEP_START; deep <= GLOBAL_DEEP_END; deep += GLOBAL_DEEP_STEP){
      //if loop or deep is less or equal to zero not execute the cicle
      // if(loop > 0 && deep > 0){
        for(i=0; i<=loop; i++){
          var j;
          var fib =[];
          var mark_start = performance.now();
          fib[0] = 0;
          fib[1] = 1;
          for(j=2; j <= deep; j++){
            fib[j] = fib[j-2] + fib[j-1];
          }
          var mark_end = performance.now();
          var measured_time = (mark_end - mark_start);
          times.push(measured_time);
        }
        // document.getElementById("a").textContent = times;
        // remove the first element from the array
        times.splice(0, 1);
        var stats_fib = stats(times);

        console.log("Mean: " + stats_fib.mean);
        console.log("Deviation: " + stats_fib.deviation);
        console.log("Variance: " + stats_fib.variance);
        console.log("Median: " + stats_fib.median);
        console.log("Loop: " + loop);
        console.log("Deep: " + deep);

        // result_bar.innerHTML += "Fibonacci | Mean: " + stats_fib.mean +
        //            " Deviation: " + stats_fib.deviation + " Variance: " +
        //            stats_fib.variance + " Median: " + stats_fib.median+"<br>"
        //            +"Deep:"+ deep +"<br> Loop:"+loop;
        document.getElementById("fib_mean").value = stats_fib.mean;
        document.getElementById("fib_dev").value = stats_fib.deviation;
        document.getElementById("fib_var").value = stats_fib.variance;
        document.getElementById("fib_med").value = stats_fib.median;
        document.getElementById("fib_rep").value = loop;
        document.getElementById("fib_w").value = deep;

        document.getElementById("Sub_button").click();

      // }
    }
  }

  if(callafter){
    state_bar.innerHTML = "Done Fibonacci";
    if(fast_mode){
      GPU_FAST_TEST(false, true, -1);
    }else{
      GPU_TEST(false , true, -1);
    }
  }else{
    state_bar.innerHTML = "Done";
  }
}
//----------------------------------------------------------------------------//
//----------------------------END OF FAST FIBONACII---------------------------//
//----------------------------------------------------------------------------//


//----------------------------------------------------------------------------//
//---------------------------------LOCALITY-----------------------------------//
//----------------------------------------------------------------------------//
function run_locality(callafter, fast_mode){
  var times = [];
  state_bar.innerHTML = "Run locality...";
  // result_bar.innerHTML = "";
  var GLOBAL_LOOP_START = parseInt(document.getElementById("loc_loop_start").value);
  var GLOBAL_LOOP_STEP = parseInt(document.getElementById("loc_loop_step").value);
  var GLOBAL_LOOP_END = parseInt(document.getElementById("loc_loop_end").value);
  var GLOBAL_DEEP_START = parseInt(document.getElementById("loc_deep_start").value);
  var GLOBAL_DEEP_END = parseInt(document.getElementById("loc_deep_end").value);
  var GLOBAL_DEEP_STEP = parseInt(document.getElementById("loc_deep_step").value);
  //check if it starts from zero (0)
  if(GLOBAL_LOOP_START == 0){
    GLOBAL_LOOP_START = GLOBAL_LOOP_STEP;
  }
  if(fast_mode){
    GLOBAL_DEEP_STEP = 500;
    GLOBAL_LOOP_STEP = 500;
  }
  if(GLOBAL_DEEP_START == 0){
    GLOBAL_DEEP_START = GLOBAL_DEEP_STEP;
  }

  var loop;
  var deep;
  clear_fields();

  function array_access_exp(array_size){
    var i = 0, cnt=0;
    var N = array_size;
    var myArray = new Array (N);


    for (i=0; i< N; i++) {
      myArray[i] = i;
    }

    // get a timestamp before running the test
    var mark_start = performance.now();
    for (i=0; i < N; i+=64) {
      var elem = myArray[0];
      cnt += elem;
    }
    // get another timestamp after the test is complete
    var mark_end = performance.now();

    myArray = 0;
    // return the elapsed time
    return (mark_end - mark_start);
  }



  stats = function(a) {
    var r = {mean: 0, variance: 0, deviation: 0, median: 0}, t = a.length;
    for(var m, s = 0, l = t; l--; s += a[l]);
    for(m = r.mean = s / t, l = t, s = 0; l--; s += Math.pow(a[l] - m, 2));
    function median(values) {
        values.sort( function(a,b) {return a - b;} );
        var half = Math.floor(values.length/2);
        if(values.length % 2)
            return values[half];
        else
            return (values[half-1] + values[half]) / 2.0;
    }
    r.median = median(a);
    return r.deviation = Math.sqrt(r.variance = s / t), r;
  }

  for(loop = GLOBAL_LOOP_START; loop <= GLOBAL_LOOP_END; loop += GLOBAL_LOOP_STEP){
    for(deep = GLOBAL_DEEP_START; deep <= GLOBAL_DEEP_END; deep += GLOBAL_DEEP_STEP){
      // if(loop > 0 && deep > 0){
        // do the tests 100K times (locality accesses)
        for (i=0; i<loop; i++){
          // run array accesses code and get the elapsed time
          var measured_time = array_access_exp(deep, true);
          // append measured time value to array
          times.push(measured_time);
          //results += "\n" + measured_time;
        }

        // remove the first element from the array
        times.splice(0, 1);
        var stats_loc = stats(times);

        // console.log("Mean: " + stats_loc.mean);
        // console.log("Deviation: " + stats_loc.deviation);
        // console.log("Variance: " + stats_loc.variance);
        // console.log("Median: " + stats_loc.median);
        // console.log("Loop: " + loop);
        // console.log("Deep: " + deep);

        // result_bar.innerHTML += "<br> Locality (cache hits) | Mean: " + stats_loc.mean +
        //            " Deviation: " + stats_loc.deviation + " Variance: " +
        //            stats_loc.variance + " Median: " + stats_loc.median+"<br>"
        //            +"Deep:"+ deep +"<br> Loop:"+loop;
       document.getElementById("loc_mean").value = stats_loc.mean;
       document.getElementById("loc_dev").value = stats_loc.deviation;
       document.getElementById("loc_var").value = stats_loc.variance;
       document.getElementById("loc_med").value = stats_loc.median;
       document.getElementById("loc_rep").value = loop;
       document.getElementById("loc_w").value = deep;

       document.getElementById("Sub_button").click();
      // }
    }
  }
  if(callafter){
    state_bar.innerHTML = "Done Locality";
    run_nonlocality(true, fast_mode);
  }else{
    state_bar.innerHTML = "Done";
  }
}
//----------------------------------------------------------------------------//
//------------------------------END OF LOCALITY-------------------------------//
//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
//---------------------------------NON LOCALITY-------------------------------//
//----------------------------------------------------------------------------//
function run_nonlocality(callafter,fast_mode){
    var times = [];
    state_bar.innerHTML = "Run non locality...";
    // result_bar.innerHTML = "";
    var GLOBAL_LOOP_START = parseInt(document.getElementById("non_loop_start").value);
    var GLOBAL_LOOP_STEP = parseInt(document.getElementById("non_loop_step").value);
    var GLOBAL_LOOP_END = parseInt(document.getElementById("non_loop_end").value);
    var GLOBAL_DEEP_START = parseInt(document.getElementById("non_deep_start").value);
    var GLOBAL_DEEP_END = parseInt(document.getElementById("non_deep_end").value);
    var GLOBAL_DEEP_STEP = parseInt(document.getElementById("non_deep_step").value);
    //check if it starts from zero (0)
    if(GLOBAL_LOOP_START == 0){
      GLOBAL_LOOP_START = GLOBAL_LOOP_STEP;
    }
    if(fast_mode){
      GLOBAL_DEEP_STEP = 500;
      GLOBAL_LOOP_STEP = 500;
    }
    if(GLOBAL_DEEP_START == 0){
      GLOBAL_DEEP_START = GLOBAL_DEEP_STEP;
    }
    var loop;
    var deep;
    clear_fields();

    function array_access_exp(array_size){
      var i = 0, cnt=0;
      var N = array_size;
      var myArray = new Array (N);

      for (i=0; i< N; i++) {
        myArray[i] = i;
      }
      // get a timestamp before running the test
      var mark_start = performance.now();

      for (i=0; i < N; i+=64) {
        var elem = myArray[i];
        cnt += elem;
      }

      // get another timestamp after the test is complete
      var mark_end = performance.now();
      myArray = 0;
      // return the elapsed time
      return (mark_end - mark_start);
    }



    stats = function(a) {
      var r = {mean: 0, variance: 0, deviation: 0, median: 0}, t = a.length;
      for(var m, s = 0, l = t; l--; s += a[l]);
      for(m = r.mean = s / t, l = t, s = 0; l--; s += Math.pow(a[l] - m, 2));
      function median(values) {
          values.sort( function(a,b) {return a - b;} );
          var half = Math.floor(values.length/2);
          if(values.length % 2)
              return values[half];
          else
              return (values[half-1] + values[half]) / 2.0;
      }
      r.median = median(a);
      return r.deviation = Math.sqrt(r.variance = s / t), r;
    }

    for(loop = GLOBAL_LOOP_START; loop <= GLOBAL_LOOP_END; loop += GLOBAL_LOOP_STEP){
      for(deep = GLOBAL_DEEP_START; deep <= GLOBAL_DEEP_END; deep += GLOBAL_DEEP_STEP){
        //if loop or deep is less or equal to zero not execute the cicle
        // if(loop > 0 && deep > 0){
          // do the tests 100K times (locality accesses)
          for (i=0; i<loop; i++){
            // run array accesses code and get the elapsed time
            var measured_time = array_access_exp(deep);
            // append measured time value to array
            times.push(measured_time);
            //results += "\n" + measured_time;
          }

          // remove the first element from the array
          times.splice(0, 1);
          var stats_noloc = stats(times);
          console.log("Mean: " + stats_noloc.mean);
          console.log("Deviation: " + stats_noloc.deviation);
          console.log("Variance: " + stats_noloc.variance);
          console.log("Median: " + stats_noloc.median);
          console.log("Loop: " + loop);
          console.log("Deep: " + deep);

          // result_bar.innerHTML = "<br> No Locality (cache misses) | Mean: " + stats_noloc.mean +
          //            " Deviation: " + stats_noloc.deviation + " Variance: " +
          //            stats_noloc.variance + " Median: " + stats_noloc.median+"<br>"
          //            +"Deep:"+ deep +"<br> Loop:"+loop;
         document.getElementById("nonl_mean").value = stats_noloc.mean;
         document.getElementById("nonl_dev").value = stats_noloc.deviation;
         document.getElementById("nonl_var").value = stats_noloc.variance;
         document.getElementById("nonl_med").value = stats_noloc.median;
         document.getElementById("nonl_rep").value = loop;
         document.getElementById("nonl_w").value = deep;
         document.getElementById("Sub_button").click();
        // }
      }
    }
    if(callafter){
      state_bar.innerHTML = "Done Non locality";
      run_fibonacci(true, fast_mode);
    }else{
      state_bar.innerHTML = "Done";
    }
}
//----------------------------------------------------------------------------//
//----------------------------END OF NON LOCALITY-----------------------------//
//----------------------------------------------------------------------------//


function run_benchmarks() {
  result_bar.innerHTML = "<h3>Phase 1 of test is running. Please wait ~ 15 minutes.</h3>";
  Zero_time = performance.now();
  run_locality(true, false);
  // run_nonlocality(false, false);
  // run_fibonacci(false, false);
  // GPU_TEST(true , false, -1);
  // GPU_TEST(false , false, -1);
}
function run_fast_mode(){
  result_bar.innerHTML = "<h3>Fast test runing.</h3>";
  Zero_time = performance.now();
  run_locality(true, true);
}
/*Useless test's*/
/*
//-----block for cores count start------

function cpu_cores(callafter) {

  // create worker concurrency estimation code as blob
  var glob_cores = 0;
  var samp ;
  var wor ;
  var GLOBAL_SAMPLES_START = parseInt(document.getElementById("cpu_samples_start").value);
  var GLOBAL_SAMPLES_END = parseInt(document.getElementById("cpu_samples_end").value);
  var GLOBAL_SAMPLES_STEP = parseInt(document.getElementById("cpu_samples_step").value);
  var GLOBAL_WORKERS_START = parseInt(document.getElementById("cpu_workers_start").value);
  var GLOBAL_WORKERS_END = parseInt(document.getElementById("cpu_workers_end").value);
  var GLOBAL_WORKERS_STEP = parseInt(document.getElementById("cpu_workers_step").value);
  //check is it start from zero (0);
  if( GLOBAL_SAMPLES_START == 0){
    GLOBAL_SAMPLES_START = GLOBAL_SAMPLES_STEP;
  }
  if(GLOBAL_WORKERS_START == 0){
    GLOBAL_WORKERS_START = GLOBAL_SAMPLES_STEP;
  }
  samp = GLOBAL_SAMPLES_START;
  wor = GLOBAL_WORKERS_START;

  state_bar.innerHTML = "Testing cpu";
  function help1() {
    self.addEventListener('message', function(e) {
      // run worker for 4 ms
      var st = Date.now();
      var et = st + 4;
      while(Date.now() < et);
      self.postMessage({st: st, et: et});
    });
  }
  var blobUrl = URL.createObjectURL(new Blob(['(',help1.toString(),')()'], {type: 'application/javascript'}));

  function rr (err, cores) {
    //result_bar.innerHTML += cores ;

    //result_bar.innerHTML +="Estimate cores: "+cores+"<br>";
    document.getElementById("cpu_cor").value = cores;
    document.getElementById("cpu_w").value = wor;
    document.getElementById("cpu_s").value = samp;
    console.log("Workes: " + wor);
    console.log("Samples: " + samp);
    console.log("Cores: " + cores);
    console.log("-----------------")
    //submit data
    document.getElementById("Sub_button").click();
    wait(5000);

    // if(!navigator.hardwareConcurrency){
    document.getElementById("cpu_cor").value = "";
    document.getElementById("cpu_w").value = "";
    document.getElementById("cpu_s").value = "";

    if(samp < GLOBAL_SAMPLES_END){
      samp += GLOBAL_SAMPLES_STEP;

      //old wait and call
      //setTimeout(sample([], samp, wor, rr),50000);

      //new wait and call
      //wait(2000);//wait 2 sec.
      sample([],samp,wor,rr);
    }else if(wor < GLOBAL_WORKERS_END){
        wor += GLOBAL_WORKERS_STEP;
        samp = GLOBAL_SAMPLES_START;

        //old wait and call
        //setTimeout(sample([], samp, wor, rr),50000);

        //new wait and call
        //wait(2000);//wait 2 sec.
        sample([],samp,wor,rr);
    }else if(callafter){
          //redirect browser to script page
          document.getElementById("file_form").submit();
        }
    // }else if(callafter){
    //     document.getElementById("file_form").submit();
    //   }
  }

  function help2() {
    clear_fields();
    // var GLOBAL_SAMPLES_START = parseInt(document.getElementById("cpu_samples_start").value);
    // var GLOBAL_SAMPLES_END = parseInt(document.getElementById("cpu_samples_end").value);
    // var GLOBAL_SAMPLES_STEP = parseInt(document.getElementById("cpu_samples_step").value);
    // var GLOBAL_WORKERS_START = parseInt(document.getElementById("cpu_workers_start").value);
    // var GLOBAL_WORKERS_END = parseInt(document.getElementById("cpu_workers_end").value);
    // var GLOBAL_WORKERS_STEP = parseInt(document.getElementById("cpu_workers_step").value);
    // //check is it start from zero (0);
    // if( GLOBAL_SAMPLES_START == 0){
    //   GLOBAL_SAMPLES_START = GLOBAL_SAMPLES_STEP;
    // }
    // if(GLOBAL_WORKERS_START == 0){
    //   GLOBAL_WORKERS_START = GLOBAL_SAMPLES_STEP;
    // }

    // if(!navigator.hardwareConcurrency){
      //on my standart take 30 samples using 16 workers
      // for(wor = GLOBAL_WORKERS_START; wor <= GLOBAL_WORKERS_END; wor += GLOBAL_WORKERS_STEP){
      //   for(samp = GLOBAL_SAMPLES_START; samp <= GLOBAL_SAMPLES_END; samp += GLOBAL_SAMPLES_STEP){
            sample([], samp, wor, rr);
      //     }
      // }
    // }else{
    //   samp = 0;
    //   wor = 0;
    //   rr(null , navigator.hardwareConcurrency);
    // }

  }
  help2();

  function sample(max, samples, numWorkers, callback){
    if(samples === 0) {
      // get overlap average
      var avg = Math.floor(max.reduce(function(avg, x) {
        return avg + x;
      }, 0) / max.length);
      avg = Math.max(1, avg);
      return callback(null, avg);
    }
    map(numWorkers, function(err, results) {
      max.push(reduce(numWorkers, results));
      sample(max, samples - 1, numWorkers, callback);
    });
  }

  function map(numWorkers, callback){
    var workers = [];
    var results = [];
    for(var i = 0; i < numWorkers; ++i) {
      var worker = new Worker(blobUrl);
      worker.addEventListener('message', function(e) {
        results.push(e.data);
        if(results.length === numWorkers) {
          for(var i = 0; i < numWorkers; ++i) {
            workers[i].terminate();
          }
          callback(null, results);
        }
      });
      workers.push(worker);
    }
    for(var i = 0; i < numWorkers; ++i) {
      workers[i].postMessage(i);
    }
  }

  function reduce(numWorkers, results) {
    // find overlapping time windows
    var overlaps = [];
    for(var n = 0; n < numWorkers; ++n) {
      var r1 = results[n];
      var overlap = overlaps[n] = [];
      for(var i = 0; i < numWorkers; ++i) {
        if(n === i) {
          continue;
        }
        var r2 = results[i];
        if((r1.st > r2.st && r1.st < r2.et) ||
          (r2.st > r1.st && r2.st < r1.et)) {
          overlap.push(i);
        }
      }
    }
    // get maximum overlaps ... don't include overlapping worker itself
    // as the main JS process was also being scheduled during the work and
    // would have to be subtracted from the estimate anyway
    return overlaps.reduce(function(max, overlap) {
      return Math.max(max, overlap.length);
    }, 0);
  }
}

//-----block for cores count end--------


//////////////--------//////////////////////
//-----block gpu cubes start----------------
////////////////////////////////////////////
function GPU_CUBES(callafter , work_time , cubeAdd){

  var start_time; //start time;
  var GLOBAL_TIME_START = parseInt(document.getElementById("ctime_start").value);
  var GLOBAL_TIME_END = parseInt(document.getElementById("ctime_end").value);
  var GLOBAL_TIME_STEP = parseInt(document.getElementById("ctime_step").value);
  var GLOBAL_ADD_START = parseInt(document.getElementById("cstep_start").value);
  var GLOBAL_ADD_END = parseInt(document.getElementById("cstep_end").value);
  var GLOBAL_ADD_STEP = parseInt(document.getElementById("cstep_step").value);
  //check if start with zero (0)
  if(GLOBAL_TIME_START == 0){
    GLOBAL_TIME_START = GLOBAL_TIME_STEP;
  }
  if(GLOBAL_ADD_START == 0){
    GLOBAL_ADD_START = GLOBAL_ADD_STEP;
  }

  if( cubeAdd <= GLOBAL_ADD_START ){
    cubeAdd = GLOBAL_ADD_START;
  }
  if( work_time < GLOBAL_TIME_START ){
    work_time = GLOBAL_TIME_START;
  }
  if(work_time == GLOBAL_TIME_START && cubeAdd == GLOBAL_ADD_START){
    clear_fields();
  }
  var numObjects = 2000;			//max objects
  var targetFPS = 50;			//highest fps before adding objects
  var varyFPS = targetFPS / 10;		//allowed range of FPS
  var lowestFPS = targetFPS - varyFPS;	//lowest FPS before taking out objects
  var startVal = 50;			//starting object number
  var removeNum = startVal;		//mesh array position, for removing objects
  var container, stats;
  var camera, scene, renderer;
  var geometry, material, material2;
  var meshes = new Array(numObjects);
  var colors = new Array(numObjects);
  var addFlag = 0;			// -1 = remove, 1 = add, 0 = nothing
  var waitFrames = 0;			// wait time between adding/removing objects
  var waitTime = 10;			// amount of frames to wait



  // Check that browser supports WebGL, else exit nicely
  if (Detector.webgl) {
    //setupTextBox();
    init();
    start_time = performance.now();
    animate();
  } else {
    var warning = Detector.getWebGLErrorMessage();
    alert("WebGl not detected. "+ warning);
    if(callafter){
      cpu_cores(true);
    }
  }

  function init()
  {
    container = document.getElementById("ggpu");
    // container = document.createElement( 'div' );
    // document.body.appendChild( container );

    camera = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 1, 10000 );
    camera.position.y = 500;
    camera.position.z = 2000;

    scene = new THREE.Scene();

    //assign random colors
    for(var i = 0; i < numObjects; i++)
    {
      colors[i] = '#'+Math.floor(Math.random()*16777215).toString(16);
    }
    var color1 = '#'+Math.floor(Math.random()*16777215).toString(16);
    var color2 = '#'+Math.floor(Math.random()*16777215).toString(16);
    var color3 = '#'+Math.floor(Math.random()*16777215).toString(16);
    var color4 = '#'+Math.floor(Math.random()*16777215).toString(16);

    //use random colors on new materials
    material1 = new THREE.MeshBasicMaterial( { color: color1, wireframe: false } );
    material2 = new THREE.MeshBasicMaterial( { color: color2, wireframe: false } );
    material3 = new THREE.MeshBasicMaterial( { color: color3, wireframe: false } );
    material4 = new THREE.MeshBasicMaterial( { color: color4, wireframe: false } );

    geometry = new THREE.CubeGeometry( 200, 200, 200 );
    geometry2 = new THREE.CubeGeometry( 200, 400, 200 );

    for(var i = 0; i < meshes.length; i++)
    {

      var compute = (Math.floor(Math.random()*4));
      if( compute == 1)
        meshes[i] = new THREE.Mesh( geometry, material1 );
      else if(compute == 2)
        meshes[i] = new THREE.Mesh( geometry, material2 );
      else if(compute == 3)
        meshes[i] = new THREE.Mesh( geometry, material3 );
      else
        meshes[i] = new THREE.Mesh( geometry, material4 );


      //randomize the positions and initial rotation for each object
      meshes[i].rotation.x += Math.floor(Math.random()*2)
      meshes[i].position.x += Math.floor(Math.random()*1100);
      meshes[i].position.x *= (Math.floor(Math.random()*3)-1);

      meshes[i].rotation.y += Math.floor(Math.random()*2)
      meshes[i].position.y += Math.floor(Math.random()*1100);
      meshes[i].position.y *= (Math.floor(Math.random()*3)-1);

      meshes[i].rotation.z += Math.floor(Math.random()*2)
      meshes[i].position.z += Math.floor(Math.random()*1100);
      meshes[i].position.z *= (Math.floor(Math.random()*3)-1);
    }

    for(var i = 0; i < startVal; i++)
    {
      scene.add( meshes[i] );
    }

    // create a point light
    var pointLight = new THREE.PointLight( 0xFFFFFF );

    // set its position
    pointLight.position.x = 10;
    pointLight.position.y = 50;
    pointLight.position.z = 130;

    renderer = new THREE.WebGLRenderer();
    renderer.setSize( window.innerWidth, window.innerHeight );

  //  document.body.appendChild( renderer.domElement );

    // //add stats
    stats = new Stats();
    //
    // //add statistics object
    // stats.getDomElement().style.position = 'absolute';
    // stats.getDomElement().style.left = '0px';
    // stats.getDomElement().style.top = '0px';
    // document.body.appendChild( stats.getDomElement() );
  }

  function animate()
  {
    var all_time = (performance.now()-start_time);
    if(all_time < work_time){
      // note: three.js includes requestAnimationFrame shim
      requestAnimationFrame( animate );
    }else{
      console.log("Objects: " + removeNum);
      console.log("FPS: " + stats.getFps());
      console.log("FPS/objects: " + removeNum/stats.getFps());
      console.log("Time: " + all_time);
      console.log("Step: " + cubeAdd);
      // result_bar.innerHTML ="Total objects: " +removeNum
      //             +"<br>FPS:"+stats.getFps()
      //             +"<br>FPS/Object:"+removeNum/stats.getFps()
      //             +"<br>time: " +all_time;
      document.getElementById("cube_total").value = removeNum;
      document.getElementById("cube_fps").value = stats.getFps();
      document.getElementById("cube_ft").value = removeNum/stats.getFps();
      document.getElementById("cube_time").value = all_time;
      document.getElementById("cube_add").value = cubeAdd;

      document.getElementById("Sub_button").click();
      cubeAdd += GLOBAL_ADD_STEP;
      if(cubeAdd <= GLOBAL_ADD_END){
        GPU_CUBES(callafter, work_time, cubeAdd);
      }else{
        cubeAdd = GLOBAL_ADD_START;
        work_time += GLOBAL_TIME_STEP;
        if(work_time <= GLOBAL_TIME_END){
          GPU_CUBES(callafter, work_time, cubeAdd);
        }else if(callafter){
          cpu_cores(true);
        }
      }

    }
    //fps meter update
    stats.update();

    //rotate all the mesh objects
    for(var i = 0; i < meshes.length; i++)
    {
      meshes[i].rotation.x += 0.01;
      meshes[i].rotation.y += 0.03;
    }

    //rotate camera position
    var timer = Date.now() * 0.0002;
    camera.position.x = Math.cos( timer ) * 2000;
    camera.position.z = Math.sin( timer ) * 2000;
    camera.lookAt( scene.position );

    // check if done waiting yet
    if( waitFrames == 0 )
    {

        var tmp;
        for(tmp=0; tmp<cubeAdd; tmp++){
          scene.add( meshes[removeNum++] );
         }
      // }

      waitFrames = waitTime;
      //updateTextBox();
    }
    else	//keep waiting
    {
      waitFrames--;
    }

    renderer.render( scene, camera );
  }


  function fpsUpdate(value)
  {
    targetFPS = value;			//highest fps before adding objects
    varyFPS = Math.ceil(targetFPS / 10);	//allowed range of FPS
    lowestFPS = targetFPS - varyFPS;	//lowest FPS before taking out objects
  }
}
////////////////////////////////////////////
//-----blcok gpu cubes end------------------
//////////////--------//////////////////////
*/
