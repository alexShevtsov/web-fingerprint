/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Alexander
 */
public class Script extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        response.setContentType("text/html;charset=UTF-8");
//        try (PrintWriter out = response.getWriter()) {
//            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet File</title>");            
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>Servlet File at " + request.getContextPath() + "</h1>");
//            out.println("</body>");
//            out.println("</html>");
//        }
    }
    
    //Function to return text of WINDOWS script with embended ID
//    private String Windows_Text(String id){
//         String str = "@echo off\n"+
//                "setlocal enabledelayedexpansion\n"+
//                "set GPU_NAME=%1\n"+
//                "for /F \"tokens=* skip=1\" %%n in ('WMIC path Win32_VideoController get Name ^| findstr \".\"') do set GPU_NAME=!GPU_NAME!%%n\n"+
//                "for /F \"tokens=* skip=1\" %%n in ('WMIC cpu get name ^| findstr \".\"') do set CPU_NAME=%%n\n"+
//                "set RAM_I=%1\n"+
//                "for /F \"tokens=* skip=1\" %%n in ('wmic memorychip get capacity ^| findstr \".\"') do set RAM_I=!RAM_I!%%n\n"+
//                "for /F \"tokens=* skip=1\" %%n in ('wmic memorychip get speed ^| findstr \".\"') do set RAM_SPEED=%%n\n"+
//                "for /F \"tokens=* skip=1\" %%n in ('wmic os get Caption ^| findstr \".\"') do set OS_NAME=%%n\n"+
//                "for /F \"tokens=* skip=1\" %%n in ('wmic os get OSArchitecture ^| findstr \".\"') do set OS_ARCH=%%n\n"+
//                "echo strFileURL=^\"http://5.55.188.77:8084/WebApplication3/NewServlet?"+id+"cpu=%CPU_NAME%^&gpu=%GPU_NAME%^&ram=%RAM_I%^&ramspeed=%RAM_SPEED%^&os=%OS_NAME%^&arch=%OS_ARCH%^\" > test.vbs\n"+
//                "echo strHDLocation=^\"stream.temp^\" >> test.vbs\n"+
//                "echo Set^ objXMLHTTP=CreateObject(^\"MSXML2.XMLHTTP^\") >> test.vbs\n"+
//                "echo objXMLHTTP.open^ ^\"GET^\",^ strFileURL,^ false >> test.vbs\n"+
//                "echo objXMLHTTP.send() >> test.vbs\n"+
//                "cscript.exe test.vbs\n"+
//                "del test.vbs";
//    return str;
//    }
    
    //Function to return text of LINUX script with embended ID
//    private String Linux_Text(String id){
//        String str = "#!/bin/bash\n"+
//                "CpU=$(cat /proc/cpuinfo | grep 'model name' -m 1 | tr -d '\\040\\011\\012\\015' | sed \"s/modelname://\" | tr ' ' '+')\n"+
//                "GpU=$(lshw -c display | grep product | sed \"s/product://\" | tr -d '\\040\\015' | tr '\\n' '+' | tr ' ' '+')\n"+
//                "GpU=${GpU%?}\n"+
//                "ArCH=$(lscpu | grep 'Architecture' | sed \"s/Architecture://\" | tr -d '\\040\\011\\012\\015' | tr ' ' '+')\n"+
//                "RaM=$(lshw -c memory | grep size: | sed \"s/size://\" | tr -d '\\040' | tr ' ' '+')\n"+
//                "RaMSPeeD=$(dmidecode | grep MHz | grep Configured -m 1| sed \"s/Configured Clock Speed://\" | tr -d '\\040\\011\\012\\015')\n"+
//                "OSNaME=$(lsb_release -a | grep Description | sed \"s/Description://\" | tr -d '\\040' | tr ' ' '+')\n"+
//                "PathUrL=\"http://5.55.188.77:8084/WebApplication3/NewServlet?"+id+"cpu=$CpU\"\n"+
//                "PathUrL=\"$PathUrL &arch=$ArCH &os=$OSNaME &ram=$RaM &ramspeed=null &gpu=virtual\"\n"+
//                "PathUrL=$(echo $PathUrL | tr -d '\\040\\011\\012\\015')\n"+
//                "wget --delete-after $(echo $PathUrL)";
//        return str;
//    }
    
    //Function to return text of LINUX script with embended ID
//    private String Mac_Text(String id){
//        String str = "#!/bin/sh\n" +
//                "CpU=$(sysctl -n machdep.cpu.brand_string | tr -d '\\040\\012' )\n" +
//                "GpU=$(system_profiler SPDisplaysDataType | grep Chipset | sed -E 's/Chipset Model://g' | tr -d '\\040')\n" +
//                "RaMI=$(sysctl hw.memsize | sed 's/hw.memsize://' | tr -d '\\040')\n" +
//                "RaMSS=$(system_profiler | grep -A 9 \"DIMM\" | grep -m 1 Speed | sed 's/Speed://' | tr -d '\\040\\012')\n" +
//                "OSN=$(system_profiler SPSoftwareDataType | grep Version | grep System | sed 's/System Version: //' | tr -d '\\040')\n" +
//                "ArCH=$(uname -p | tr -d '\\040')\n" +
//                "curl 5.55.188.77:8084/WebApplication3/NewServlet?"+id+"cpu=$CpU\\&gpu=$GpU\\&ram=$RaMI\\&ramspeed=$RaMSS\\&os=$OSN\\&arch=$ArCH";
//        return str;
//    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String webInfPath = getServletConfig().getServletContext().getRealPath("WEB-INF");
        System.out.println("Web inf patch:"+webInfPath);
        String fileName="";
        File newFile;
        FileOutputStream fout = null;
//        String TEXT="";
        String arch = request.getParameter("arch");
        String OS = request.getParameter("os");
        
//        if(ID != null && ID.length() > 0 && OS != null && OS.length() > 2 ){
            
            if( OS.equalsIgnoreCase("windows") ){
                
                //fileName = webInfPath+"/win_script.bat";
                if(arch.equalsIgnoreCase("64")){
                    fileName = "/var/lib/tomcat7/webapps/fingerprint/WEB-INF/win_script64.exe";
                    //fileName = "C:/Program Files/Apache Software Foundation/Tomcat 7.0/webapps/fingerprint/WEB-INF/win_script64.exe";
                }else{
                    fileName = "/var/lib/tomcat7/webapps/fingerprint/WEB-INF/win_script32.exe";
                   //fileName = "C:/Program Files/Apache Software Foundation/Tomcat 7.0/webapps/fingerprint/WEB-INF/win_script32.exe";
                }
                //fileName = fileName.replace("file:", "");
                //System.out.println("FileName:"+fileName+"end\n");
                newFile = new File(fileName);
//                fout = new FileOutputStream(newFile);
//                newFile.createNewFile();
//                ID ="id="+ID+"^&";
//                TEXT = Windows_Text(ID);
            }else if( OS.equalsIgnoreCase("linux") ){
//                fileName = webInfPath+"/linux_script.sh";
                if(arch.equalsIgnoreCase("64")){
                    fileName = "/var/lib/tomcat7/webapps/fingerprint/WEB-INF/linux_script64.x";
                    //fileName = "C:/Program Files/Apache Software Foundation/Tomcat 7.0/webapps/fingerprint/WEB-INF/linux_script64.x";
                }else{
                    fileName = "/var/lib/tomcat7/webapps/fingerprint/WEB-INF/linux_script32.x";
                    //fileName = "C:/Program Files/Apache Software Foundation/Tomcat 7.0/webapps/fingerprint/WEB-INF/linux_script32.x";
                }
                newFile = new File(fileName);
//                fout = new FileOutputStream(newFile);
//                newFile.createNewFile();
//                ID ="id="+ID+"&";
//                TEXT = Linux_Text(ID);
            }else if( OS.equalsIgnoreCase("mac") ){
//                fileName = webInfPath+"/mac_script.command";
//                fileName = request.getServletContext().getResource("/WEB-INF/mac_script.bat").toString();
//                fileName = fileName.replace("file:", "");
                if(arch.equalsIgnoreCase("64")){
                    fileName = "/var/lib/tomcat7/webapps/fingerprint/WEB-INF/mac_script64";
                    //fileName = "C:/Program Files/Apache Software Foundation/Tomcat 7.0/webapps/fingerprint/WEB-INF/mac_script64";
                }else{
                    fileName = "/var/lib/tomcat7/webapps/fingerprint/WEB-INF/mac_script32";
                }
                newFile = new File(fileName);
//                fout = new FileOutputStreasm(newFile);
//                newFile.createNewFile();
//                ID ="id="+ID+"\\&";
//                TEXT = Mac_Text(ID);
            }else{
                response.setContentType("text/html;charset=UTF-8");
                try (PrintWriter out = response.getWriter()) {
                    /* TODO output your page here. You may use following sample code. */
                    out.println("<!DOCTYPE html>");
                    out.println("<html>");
                    out.println("<head>");
                    out.println("<title>Servlet File</title>");            
                    out.println("</head>");
                    out.println("<body>");
                    out.println("<h1>Servlet File: os version field wrong!!</h1>");
                    out.println("</body>");
                    out.println("</html>");
                }
                processRequest(request, response);
                return;
            }
    //        System.out.println("ID:"+ID);

//            byte[] contentInBytes = TEXT.getBytes();
//            fout.write(contentInBytes);
//            fout.close();
            FileInputStream inStream = new FileInputStream(fileName);


            // obtains ServletContext
            ServletContext context = getServletContext();

            // gets MIME type of the file
            String mimeType = context.getMimeType(fileName);
            if (mimeType == null) {        
                // set to binary type if MIME mapping not found
                mimeType = "application/octet-stream";
            }
//            System.out.println("MIME type: " + mimeType);

            // modifies response
            response.setContentType(mimeType);
            response.setContentLength((int) newFile.length());

            // forces download
            String headerKey = "Content-Disposition";
            String headerValue = String.format("attachment; filename=\"%s\"", newFile.getName());
            response.setHeader(headerKey, headerValue);

            // obtains response's output stream
            OutputStream outStream = response.getOutputStream();

            byte[] buffer = new byte[4096];
            int bytesRead = -1;

            while ((bytesRead = inStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }

            inStream.close();
            outStream.close();     
//        }else{
//            response.setContentType("text/html;charset=UTF-8");
//                try (PrintWriter out = response.getWriter()) {
//                    /* TODO output your page here. You may use following sample code. */
//                    out.println("<!DOCTYPE html>");
//                    out.println("<html>");
//                    out.println("<head>");
//                    out.println("<title>Servlet Script</title>");            
//                    out.println("</head>");
//                    out.println("<body>");
//                    out.println("<h1>Servlet Script: id and os version field's its wrong!!</h1>");
//                    out.println("</body>");
//                    out.println("</html>");
//                }
//        }
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
