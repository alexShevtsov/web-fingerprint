/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */




import java.sql.Statement;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Alexander
 */
public class Data extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        response.setContentType("text/html;charset=UTF-8");
//        try (PrintWriter out = response.getWriter()) {
//            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet File</title>");
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>Servlet File at " + request.getContextPath() + "</h1>");
//            out.println("</body>");
//            out.println("</html>");
//        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String Table = request.getParameter("table");
        String Key = request.getParameter("key");
        String KeyValue = request.getParameter("keyText");

        String Column = "" ; //
        String col_ar[] = new String[9];
        Integer count = 0;

//        String USER = "root";
//        String PASS = "mazahaka";
        String JDBC_DRIVER="com.mysql.jdbc.Driver";
        String DB_URL="jdbc:mysql://localhost/fingerprint";

        String Sql_query="";

        //  Database credentials
         String USER = "root";
         String PASS = "123qwe!@#QWE";

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE html>\n" +
            "<html>\n" +
            "<head>\n" +
            "  <title>Fingerprint</title>\n" +
            "  <meta charset=\"UTF-8\">\n" +
            "  <link href = \"style.css\" rel=\"stylesheet\">\n" +
            "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
            "</head>\n" +
            "\n" +
            "<body>\n" +
            "  <div id=\"page\">\n" +
            "  <header><br><h1>Fingerprint</h1></header>\n" +
            "  <nav><a href=\"index.html\">Home</a>\n" +
            "      <a href=\"data.html\">Data</a>\n" +
            "      <a href=\"about.html\">About</a>\n" +
            "      <a href=\"contact.html\">Contact</a>\n" +
            "      </nav>\n" +
            "  <section>\n");
        if(Table == null){
            out.println("<h4>Choise some table!</h4>>" +
                "</section>\n" +
                "  <footer><p><strong>This page made by ics forth</strong></p>\n" +
                "  </footer>\n" +
                "  </div>\n" +
                "</body>\n" +
                "</html>");
            processRequest(request, response);
        }else if(request.getParameter("type") == null){
            if( Table.equalsIgnoreCase("locality") ||
                Table.equalsIgnoreCase("nonlocality") ||
                Table.equalsIgnoreCase("fibonacci") ){

                if(request.getParameter("timeSTP").equalsIgnoreCase("yes")){
                    Column = "TIME_STMP";
                    col_ar[count++] = "TIME_STMP";
                }
                if(request.getParameter("ip").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", IP";
                    else Column = "IP";
                    col_ar[count++] = "IP";
                }
                if(request.getParameter("agent").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", AGENT";
                    else Column = "AGENT";
                    col_ar[count++] = "AGENT";
                }
                if(request.getParameter("mean").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", MEAN";
                    else Column = "MEAN";
                    col_ar[count++] = "MEAN";
                }
                if(request.getParameter("deav").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", DEV";
                    else Column = "DEV";
                    col_ar[count++] = "DEV";
                }
                if(request.getParameter("var").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", VAR";
                    else Column = "VAR";
                    col_ar[count++] = "VAR";
                }
                if(request.getParameter("med").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", MED";
                    else Column = "MED";
                    col_ar[count++] = "MED";
                }
                if(request.getParameter("dep").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", DEP";
                    else Column = "DEP";
                    col_ar[count++] = "DEP";
                }
                if(request.getParameter("rep").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", REP";
                    else Column = "REP";
                    col_ar[count++] = "REP";
                }

            }else if(Table.equalsIgnoreCase("gpu_z") ||
                     Table.equalsIgnoreCase("gpu_nonz")){

                if(request.getParameter("timeSTP").equalsIgnoreCase("yes")){
                    Column = "TIME_STMP";
                    col_ar[count++] = "TIME_STMP";
                }
                if(request.getParameter("ip").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", IP";
                    else Column = "IP";
                    col_ar[count++] = "IP";
                }
                if(request.getParameter("agent").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", AGENT";
                    else Column = "AGENT";
                    col_ar[count++] = "AGENT";
                }
                if(request.getParameter("totalf").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", TOTALF";
                    else Column = "TOTALF";
                    col_ar[count++] = "TOTALF";
                }
                if(request.getParameter("objf").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", OBJF";
                    else Column = "OBJF";
                    col_ar[count++] = "OBJF";
                }
                if(request.getParameter("fft").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", FFT";
                    else Column = "FFT";
                    col_ar[count++] = "FFT";
                }
                if(request.getParameter("min").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", MIN";
                    else Column = "MIN";
                    col_ar[count++] = "MIN";
                }
                if(request.getParameter("max").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", MAX";
                    else Column = "MAX";
                    col_ar[count++] = "MAX";
                }
                if(request.getParameter("ave").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", AVE";
                    else Column = "AVE";
                    col_ar[count++] = "AVE";
                }
                if(request.getParameter("data").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", DATA";
                    else Column = "DATA";
                    col_ar[count++] = "DATA";
                }
                if(request.getParameter("start").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", START";
                    else Column = "START";
                    col_ar[count++] = "START";
                }
                if(request.getParameter("step").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", STEP";
                    else Column = "STEP";
                    col_ar[count++] = "STEP";
                }

            }else if( Table.equalsIgnoreCase("gpu_cube") ){

                if(request.getParameter("timeSTP").equalsIgnoreCase("yes")){
                    Column = "TIME_STMP";
                    col_ar[count++] = "TIME_STMP";
                }
                if(request.getParameter("ip").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", IP";
                    else Column = "IP";
                    col_ar[count++] = "IP";
                }
                if(request.getParameter("agent").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", AGENT";
                    else Column = "AGENT";
                    col_ar[count++] = "AGENT";
                }
                if(request.getParameter("totalO").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", TOTALO";
                    else Column = "TOTALO";
                    col_ar[count++] = "TOTALO";
                }
                if(request.getParameter("fps").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", FPS";
                    else Column = "FPS";
                    col_ar[count++] = "FPS";
                }
                if(request.getParameter("fo").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", FO";
                    else Column = "FO";
                    col_ar[count++] = "FO";
                }
                if(request.getParameter("time").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", TIME";
                    else Column = "TIME";
                    col_ar[count++] = "TIME";
                }
                if(request.getParameter("step").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", STEP";
                    else Column = "STEP";
                    col_ar[count++] = "STEP";
                }

            }else if( Table.equalsIgnoreCase("cpu") ){

                if(request.getParameter("timeSTP").equalsIgnoreCase("yes")){
                    Column = "TIME_STMP";
                    col_ar[count++] = "TIME_STMP";
                }
                if(request.getParameter("ip").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", IP";
                    else Column = "IP";
                    col_ar[count++] = "IP";
                }
                if(request.getParameter("agent").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", AGENT";
                    else Column = "AGENT";
                    col_ar[count++] = "AGENT";
                }
                if(request.getParameter("core").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", CPU_CORE";
                    else Column = "CPU_CORE";
                    col_ar[count++] = "CPU_CORE";
                }
                if(request.getParameter("sample").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", CPU_SAMPLE";
                    else Column = "CPU_SAMPLE";
                    col_ar[count++] = "CPU_SAMPLE";
                }
                if(request.getParameter("worker").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", CPU_WORK";
                    else Column = "CPU_WORK";
                    col_ar[count++] = "CPU_WORK";
                }

            } else if( Table.equalsIgnoreCase("hardware_info") ){

                if(request.getParameter("ip").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", IP";
                    else Column = "IP";
                    col_ar[count++] = "IP";
                }
                if(request.getParameter("cpu").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", CPU";
                    else Column = "CPU";
                    col_ar[count++] = "CPU";
                }
                if(request.getParameter("gpu").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", GPU";
                    else Column = "GPU";
                    col_ar[count++] = "GPU";
                }
                if(request.getParameter("ram").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", RAM";
                    else Column = "RAM";
                    col_ar[count++] = "RAM";
                }
                if(request.getParameter("ramS").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", RAMSPEED";
                    else Column = "RAMSPEED";
                    col_ar[count++] = "RAMSPEED";
                }
                if(request.getParameter("os").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", OS";
                    else Column = "OS";
                    col_ar[count++] = "OS";
                }
                if(request.getParameter("arch").equalsIgnoreCase("yes")){
                    if(Column.length() > 0) Column += ", ARCH";
                    else Column = "ARCH";
                    col_ar[count++] = "ARCH";
                }

            } else {
                out.println("<h4>Wrong Table name!</h4>"+"</section>\n" +
                    "  <footer><p><strong>This page made by ics forth</strong></p>\n" +
                    "  </footer>\n" +
                    "  </div>\n" +
                    "</body>\n" +
                    "</html>");
                processRequest(request, response);
            }

            if(Key.equalsIgnoreCase("all")){
              Key = "";
            }else{
              Key = " where "+Key + " = '" + KeyValue + "'";
            }
            Sql_query = "Select " + Column + " from "+ Table+
                        Key;
            //connect to database , execute query and print the result table into page
            try {
                    Connection con;
                    Class.forName("com.mysql.jdbc.Driver");

                    // Open a connection
                    con = DriverManager.getConnection(DB_URL,USER,PASS);
                    Statement stmt = con.createStatement();
                    //execute query and get the result set
                    ResultSet results = stmt.executeQuery(Sql_query);

                    out.println("<table border=\"1\" style=\"width:100%\">\n"
                            +"<tr>\n");
                    Integer tmp=0;
                    while(tmp < count){
                        out.println("<td>" + col_ar[tmp++] +"</td>\n");
                    }

                    out.println("</tr>\n");

                    while( results.next() ){
                        out.println("<tr>\n");
                        tmp=0;
                        while(tmp < count){
                            out.println("<td>" + results.getString( col_ar[tmp++] )
                                    + "</td>\n");
                        }
                        out.println("</tr>\n");
                    }
                    out.println("</table\n>");
                    con.close();

                } catch (SQLException | ClassNotFoundException ex) {
                    Logger.getLogger(Servlet.class.getName()).log(Level.SEVERE, null, ex);
                }

            out.println("</section>\n" +
                "  <footer><p><strong>This page made by ics forth</strong></p>\n" +
                "  </footer>\n" +
                "  </div>\n" +
                "</body>\n" +
                "</html>");
            processRequest(request, response);
        }else if(request.getParameter("type").equalsIgnoreCase("calc")){
            if( Table.equalsIgnoreCase("locality") ||
                Table.equalsIgnoreCase("nonlocality") ||
                Table.equalsIgnoreCase("fibonacci") ){
                col_ar = new String []{"MEAN","DEV","VAR","MED","DEP","REP"};
                Sql_query = "Select MEAN, DEV, VAR, MED, DEP, REP , TIME_STMP from "+Table+
                        " where IP = '"+KeyValue+"'";
                //connect to database , execute query and print the result table into page
            try {
                    Connection con;
                    Class.forName("com.mysql.jdbc.Driver");

                    // Open a connection
                    con = DriverManager.getConnection(DB_URL,USER,PASS);
                    Statement stmt = con.createStatement();
                    //execute query and get the result set
                    ResultSet results = stmt.executeQuery(Sql_query);

                    out.println("<table border=\"1\" style=\"width:100%\">\n"
                            +"<tr>\n");
                    Integer tmp=0;
                    while(tmp < 4){
                        out.println("<td>" + col_ar[tmp++] +"</td>\n");
                    }

                    out.println("</tr>\n");
                    int RepStart = Integer.parseInt( request.getParameter("repStart") );
                    int RepEnd = Integer.parseInt( request.getParameter("repEnd") );
                    int DepStart = Integer.parseInt( request.getParameter("depStart") );
                    int DepEnd = Integer.parseInt( request.getParameter("depEnd") );
                    double s1= 0.0;
                    double s2 = 0.0;
                    double s3 = 0.0;
                    double s4 = 0.0;
                    int rep, dep;
                    tmp = 0;
                    String cmp = "";
                    while( results.next() ){
                        if(cmp.length() < 1){
                            cmp = results.getString("TIME_STMP");
                            cmp = cmp.substring(0, cmp.indexOf(":"));
                            out.println("<h4>Time stamp to comp:"+cmp+"</h4>");
                        }
                        rep = Integer.parseInt( results.getString("REP") );
                        dep =Integer.parseInt(results.getString("DEP"));
                        if(rep >= RepStart && rep <=RepEnd &&
                                dep >= DepStart && dep <= DepEnd){
                            s1 += Double.parseDouble( results.getString( col_ar[0] ) );
                            s2 += Double.parseDouble( results.getString( col_ar[1] ) );
                            s3 += Double.parseDouble( results.getString( col_ar[2] ) );
                            s4 += Double.parseDouble( results.getString( col_ar[3] ) );
                        }
                        tmp++;
                        if( !results.getString("TIME_STMP").contains(cmp) ){
                            cmp="";
                            out.println("<tr>\n<td>"+ s1/tmp + "</td>\n"+
                                        "<td>\n"+s2/tmp+"</td>\n"+
                                        "<td>\n"+s3/tmp+"</td>\n"+
                                        "<td>\n"+s4/tmp+"</td>\n</tr>");
                            tmp=0;
                            s1=0.0;
                            s2=0.0;
                            s3=0.0;
                            s4=0.0;
                        }

                    }
                    if(tmp!=0){
                        out.println("<tr>\n<td>"+ s1/tmp + "</td>\n"+
                                        "<td>\n"+s2/tmp+"</td>\n"+
                                        "<td>\n"+s3/tmp+"</td>\n"+
                                        "<td>\n"+s4/tmp+"</td>\n</tr>");
                    }
                    out.println("</table\n>");
                    con.close();

                } catch (SQLException | ClassNotFoundException ex) {
                    Logger.getLogger(Servlet.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
