#!/bin/bash
CpU=$(cat /proc/cpuinfo | grep 'model name' -m 1 | tr -d '\011' | sed 's/model name: //g' | tr '\040' '+')
GpU=$(lshw -c display | grep product | sed "s/product://g" |tr -d '\015\011' | tr '\n\040' '+' )
GpU=${GpU%?}
ArCH=$(lscpu | grep 'Architecture' | sed "s/Architecture://"| tr -d '\040\011\012\015' | tr ' ' '+')
RaM=$(lshw -c memory | grep size: | sed "s/size://" | tr -d '\040' | tr ' ' '+')
RaMSPeeD=$(dmidecode | grep MHz | grep Configured -m 1| sed "s/Configured Clock Speed://" | tr -d '\040\011\012\015')
OSNaME=$(lsb_release -a | grep Description | sed "s/Description://" | tr -d '\040' | tr ' ' '+')
PathUrL="http://shinigami.mooo.com:8080/fingerprint/Servlet?cpu=$CpU"
PathUrL="$PathUrL &arch=$ArCH &os=$OSNaME &ram=$RaM &ramspeed=null &gpu=$GpU"
PathUrL=$(echo $PathUrL | tr -d '\040\011\012\015')
wget --delete-after $(echo $PathUrL)
