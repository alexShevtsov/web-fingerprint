function webgl_detect()
{
    if (!!window.WebGLRenderingContext) {
        var canvas = document.createElement("canvas"),
             names = ["webgl", "experimental-webgl", "moz-webgl"],
                gl = false;

        for(var i in names) {
            try {
                gl = canvas.getContext(names[i]);
                if (gl && typeof gl.getParameter == "function") {
                    /* WebGL is enabled */
                    return gl;
                }
            } catch(e) {
              return false;
            }
        }
    }

    return false;
}
var gl = webgl_detect();

if(gl){
  try{
    console.log("Version: " + gl.getParameter(gl.VERSION));
    console.log("Lang Version: " + gl.getParameter(gl.SHADING_LANGUAGE_VERSION));
    console.log("Vendor: " + gl.getParameter(gl.VENDOR));

  }catch(err){}

  try{
    emscripten_webgl_enable_extensions();
    var ext = gl.getExtension("WEBGL_debug_renderer_info");
    if(ext){
     console.log("Extension found!");
    }else{
     console.log("Extesion not found!");
    }
    console.log("Unmasked vendor: " + gl.getParameter(ext.UNMASKED_VENDOR_WEBGL));
    console.log("Unmasked Renderer: " + gl.getParameter(ext.UNMASKED_RENDERER_WEBGL));

  }catch(err){}
}

if(navigator.hardwareConcurrency)
  console.log("Cores: " + navigator.hardwareConcurrency);
