rm(list=ls())
setwd("/home/parker/FORTH/fingerprint/fingerprint")
#
##---Read all data from file
A<-read.table("./DATA/gpu_nonz.txt",header = T)
A<-read.table("./DATA/test_gpu_nonz.txt",header = T)
A<-read.table("./DATA/test_gpu_nonz_2.txt",header = T)
#
##---Take all unique id's
UN_ID <- unique(A["RUN_ID"])
#sort id to enc order
UN_ID[] <- sort(UN_ID[,])


##---Put all data to a list
All_Data<-list();
for(data_count in 1:dim(UN_ID)[1]){
  Data_1 <- A[A["RUN_ID"] == UN_ID[data_count,],]
  # Data_1 <- Data_1[Data_1["STEP"] == 70 ,]
  #  Data_1 <- Data_1[Data_1["START"] == 100 | Data_1["START"] == 300 | Data_1["START"] == 500 | Data_1["START"] == 1000 | Data_1["START"] == 1500 | Data_1["START"] == 1750 | Data_1["START"] == 2000 | Data_1["START"] == 2500 | Data_1["START"] == 3000 | Data_1["START"] == 1300 | Data_1["START"] == 1900 | Data_1["START"] == 2500 | Data_1["START"] == 2800 |  Data_1["START"] ==3100,]
  All_Data[[data_count]] <- unique(Data_1);
}

##########################################################
####---Help function that find position of RUN_ID (id)--##
##########################################################
find_run_id<-function(id){
  for(i in 1:length(All_Data)){
    if(All_Data[[i]]["RUN_ID"][1,] == id){
      return(i)
    }
  }
}

##Data explain
#           # TOTALF - Total frame rendered
#           # OBJF - Object in the end of run
#           # FFT - Final Frame Time
#           # Min - minimum Frame rendering time
#           # Max - maximum Frame rendering time
#           # Ave - mean of rendering time
#           # Start - start number of objects

#as.matrix(All_Data[[12]])[All_Data[[12]]["STEP"] == 50,]

DataColor<-c("orange2", "brown", "gray", "red", "black", "blue", "green","yellow", "darkorchid", "deeppink")

color_set <-function(data_set){
  ip <- c()
  for(i in data_set){
    ip <- c(ip, as.vector(All_Data[[i]]["IP"][1,1]) )
  }
  u_ip <- unique(ip)
  color_data <- c()
  for(i in ip){
    color_data <- c(color_data , DataColor[which(u_ip == i)])
  }
  return (color_data)
}

####----------####----------####----------####----------####----------####----------####----------####----------####----------
#---------------------------------------------Plot every single ip in other plot---------------------------------------------#
####----------####----------####----------####----------####----------####----------####----------####----------####----------
PLOT2 <- function(to_file){
  color_c <- 1;
  yLabel <- "Render time (miliseconds)"
  xLabel <- "Number of Objects"
  gLabel <- "Gpu (non z-buffer) rendering"
  for(l in 1:length(All_Data)){
    ##---Plot mean data  
    ##---Calculate min and max for first plot
    #take sub of data
    Data<-All_Data[[l]][All_Data[[l]]["STEP"] == 50,]
    
    ##---Calculate min and max for second plot
    DataMin <- min(Data["MIN"][,1])
    DataMax <- max(Data["MAX"][,1])
    if(!to_file){
      dev.new()
    }else{
      name <- gsub(" ","",paste( "plots/gpu-nonz_res/single/gpu-z_" , substr(as.matrix(Data["IP"])[1,],8,13), ".jpg"))
      jpeg(filename = name,
           width = 800 , height = 600)
    }
    Plot2 <- seq(DataMin, DataMax, by=((DataMax-DataMin) / (length(Data["START"][, 1]) -1 ) ));
    
    plot(Data["START"][, 1],
         Plot2,
         type="n",
         col=DataColor[color_c], 
         xlab=xLabel,
         ylab=yLabel, 
         main=gLabel
    );
    legendIP<-c()
    legendIP[1]<- substr(as.matrix(Data["IP"])[1,],8,13)
    xData <- Data["START"][,1]
    for(k in 1:length(xData)){
      Max <- Data["MIN"][,1][k] 
      Min <- Data["MAX"][,1][k]
      points(c(xData[k], xData[k]), c(Max, Min), type="l")
      points(c(xData[k], xData[k]), c(Max, Min), type="o",col=DataColor[color_c])
      points(xData[k],Data["AVE"][,1][k],pch="+",col=DataColor[color_c])
    }
    
    print(DataColor[color_c])
    print(Data["IP"][1,1])
    
    legend(min(Data["START"][,1]),DataMax, 
           legendIP,DataColor[c(0,color_c)])
    color_c<-color_c+1;
    if(color_c > length(DataColor)) color_c <-1
    if(to_file) dev.off()
  }
}
####----------####----------####----------####----------####----------####----------####----------####----------
#--------------------------------------------Plot all data in groups-------------------------------------------#
####----------####----------####----------####----------####----------####----------####----------####----------
PLOT3<-function(data_num, to_file, myname, aveg, my_label){
  ######------------------------Mean Plot------------------------------------------####
  ##---Plot pair data of same ip 
  
  color_c <- 1;
  yLabel <- "Render time (miliseconds)"
  xLabel <- "Number of Objects"
  gLabel <- paste("Gpu (non z-buffer) rendering ",my_label)
  
  ##---Calculate min and max for first plot
  DataList<-c()
  if(aveg){
    for(i in 1:dim(UN_ID)[1]){
      DataList <- c(DataList, All_Data[[ i ]]["AVE"][,1])
    }
  }else{
    for(i in 1:dim(UN_ID)[1]){
      DataList <- c(DataList, All_Data[[ i ]]["MIN"][,1],All_Data[[ i ]]["MAX"][,1])
    }
  }
  
  DataMin <- min(DataList)
  DataMax <- max(DataList)
  color_data <- color_set(data_num)
  ##
  if(!to_file){
    dev.new()
  }else{
    if(myname == ""){
      name <- gsub(" ","",paste( "plots/gpu-nonz_res/single/gpu-z_" , substr(as.matrix(All_Data[[ data_num[1] ]]["IP"])[1,],8,13), ".jpg"))
    }else{
      name <- gsub(" ","",paste("plots/gpu-nonz_res/group/",myname))
    }
    jpeg(filename = name,
         width = 800 , height = 600)
  }
  Plot1 <- seq(DataMin, DataMax, by=((DataMax-DataMin) / (length(All_Data[[ 1 ]]["START"][, 1]) -1 ) ));
  ## -- ploting
  legendIP <- c();
  plot(All_Data[[1]]["START"][,1], Plot1,type="n",xlab=xLabel,ylab=yLabel,main=gLabel);
  
  for(i in data_num){
    legendIP[which(data_num == i)]<-substr(as.matrix(All_Data[[i]]["IP"])[1,],8,13)
    
    xData <- All_Data[[i]]["START"][,1]
    if(aveg){
      for(k in 1:length(xData)){ 
        points(xData[k],All_Data[[i]]["AVE"][,1][k],pch="+",col=color_data[which(data_num == i)])
        if(k+1 <= length(xData)) points(c(xData[k], xData[k+1]), c(All_Data[[i]]["AVE"][,1][k], All_Data[[i]]["AVE"][,1][k+1]), type="l",col=color_data[which(data_num == i)])
      }
    }else{
      for(k in 1:length(xData)){
        Max <- All_Data[[i]]["MIN"][,1][k] 
        Min <- All_Data[[i]]["MAX"][,1][k]
        points(c(xData[k], xData[k]), c(Max, Min), type="l")
        points(c(xData[k], xData[k]), c(Max, Min), type="o",col=color_data[which(data_num == i)])
        points(xData[k],All_Data[[i]]["AVE"][,1][k],pch="+",col=color_data[which(data_num == i)])
      }
    }
    print(color_data[i])
    print(All_Data[[i]]["IP"][1,1])
  }
  
  legend(min(All_Data[[1]]["START"][,1]),DataMax, 
         legendIP,color_data)
  if(to_file) dev.off()
}
####----------####----------####----------####----------####----------####----------####----------####----------
#--------------------------------------------Plot all data in groups of fast and small test-------------------------------------------#
####----------####----------####----------####----------####----------####----------####----------####----------
PLOT4<-function(data_num, label, to_file, myname, aveg, my_label){
  ######------------------------Mean Plot------------------------------------------####
  ##---Plot pair data of same ip 
  
  color_c <- 1;
  
  xLabel <- "Initializing number of objects"
  gLabel <- paste(" ",my_label)
  
  ##---Calculate min and max for first plot
  DataList<-c()
  yLabel <- "Frame rendering time (millisecondss)"
  v<-find_run_id(69)
  end<-find_run_id(102)
  if(aveg){
    #yLabel <- "Average frame rendering time (ms)"
    #for(i in v:dim(UN_ID)[1]){
    for(i in v:end){
      DataList <- c(DataList, All_Data[[ i ]]["AVE"][,1])
    }
  }else{
    #yLabel <- "Min and Max frame rendering time (ms)"
    #for(i in v:dim(UN_ID)[1]){
    for(i in v:end){
      DataList <- c(DataList, All_Data[[ i ]]["MIN"][,1],All_Data[[ i ]]["MAX"][,1])
    }
  }
  
  DataMin <- min(DataList)
  DataMax <- max(DataList)
  #color_data <- color_set(data_num)
  ##
  if(!to_file){
    dev.new()
  }else{
    if(myname == ""){
      name <- gsub(" ","",paste( "plots/gpu-nonz_res/single/gpu-z_" , substr(as.matrix(All_Data[[ data_num[1] ]]["IP"])[1,],8,13), ".jpg"))
    }else{
      name <- gsub(" ","",paste("plots/gpu-nonz_res/group/",myname,".jpg"))
    }
    jpeg(filename = name,
         width = 800 , height = 600,quality = 100)
  }
  Plot1 <- seq(DataMin, DataMax, by=((DataMax-DataMin) / (length(All_Data[[ 1 ]]["START"][, 1]) -1 ) ));
  ## -- ploting
  #legendIP <- c();
  par(mar=c(5,5,1,1))
  par(family="Times New Roman")
  #plot(All_Data[[1]]["START"][,1], Plot1,type="n",xlab=xLabel,ylab=yLabel,main=gLabel);
  plot(All_Data[[1]]["START"][,1], Plot1,type="n",xlab=xLabel,ylab=yLabel,cex.lab = 2.3,cex.axis=1.8);
  
  color_num<-1;
  for(p in data_num){
    for(i in p){
      #legendIP[which(data_num_full == i)]<-substr(as.matrix(All_Data[[i]]["IP"])[1,],8,13)
      g<-find_run_id(i)
      xData <- All_Data[[g]]["START"][,1]
      if(aveg){
        for(k in 1:length(xData)){ 
          points(xData[k],All_Data[[g]]["AVE"][,1][k],pch="+",col=DataColor[color_num])
          if(k+1 <= length(xData)) points(c(xData[k], xData[k+1]), c(All_Data[[g]]["AVE"][,1][k], All_Data[[g]]["AVE"][,1][k+1]), type="l",col=DataColor[color_num])
        }
      }else{
        for(k in 1:length(xData)){
          Max <- All_Data[[g]]["MIN"][,1][k] 
          Min <- All_Data[[g]]["MAX"][,1][k]
          points(c(xData[k], xData[k]), c(Max, Min), type="l")
          points(c(xData[k], xData[k]), c(Max, Min), type="o",col=DataColor[color_num])
          points(xData[k],All_Data[[g]]["AVE"][,1][k],pch="+",col=DataColor[color_num])
        }
      }
    }
    color_num<-color_num+1
  }
  title_name<-NULL
  if(length(label) > length(data_num)){
    title_name<-label[1]
    label<- label[2:length(label)]
  }
  legend(min(All_Data[[1]]["START"][, 1]), DataMax, 
         label, DataColor[1:color_num],title=title_name,cex=2.2)
  if(to_file) dev.off()
}
##------------------------------------------END-------------------------------------------------##

PLOT2(F)
PLOT3(c(1,13,14,11,12,8,10,2,3),F,"group_1.jpg",F," (min, average, max)")
PLOT3(c(1,13,14,11,12,8,10,2,3),F,"group_1_aveg.jpg",T," (just average)")

PLOT3(c(15,16,17,18,4,5,6,7,9),F,"group_2.jpg",F," (min, average, max)")
PLOT3(c(15,16,17,18,4,5,6,7,9),F,"group_2_aveg.jpg",T," (just average)")

# GPU stress test
PLOT3(c(21,22,23,24,25,26),F,"group_1.jpg",F," (min, average, max)")
PLOT3(c(21,22,23,24,25,26),F,"group_1_aveg.jpg",T," (just average)")

####------------------------
###-PC2
PLOT4(list( c(69,75,77,79), c(70,76,78,81) ),c("Fast", "Full"),F," ",T,"Safari PC2 (just average)")
PLOT4(list( c(69,75,77,79), c(70,76,78,81) ),c("Fast", "Full"),F," ",F,"Safari PC2 (min, average, max)")
##2nd atemp
PLOT4(list( c(103,105,107), c(104,106,108) ),c("Fast", "Full"),F," ",T,"Safari PC2 (just average)")
PLOT4(list( c(103,105,107), c(104,106,108) ),c("Fast", "Full"),F," ",F,"Safari PC2 (min, average, max)")

PLOT4(list( c(71,83), c(72,84) ),c("Fast", "Full"),F," ",T,"Chrome PC2 (just average)")
PLOT4(list( c(71,83), c(72,84) ),c("Fast", "Full"),F," ",F,"Chrome PC2 (min, average, max)")
##2nd atemp
PLOT4(list( c(109,111,113), c(110,112,114) ),c("Fast", "Full"),F," ",T,"Chrome PC2 (just average)")
PLOT4(list( c(109,111,113), c(110,112,114) ),c("Fast", "Full"),F," ",F,"Chrome PC2 (min, average, max)")

PLOT4(list( c(73), c(74) ),c("Fast", "Full"),F," ",T,"Firefox PC2 (just average)")
PLOT4(list( c(73), c(74) ),c("Fast", "Full"),F," ",F,"Firefox PC2 (min, average, max)")

PLOT4(list( c(109,111,113), c(103,105,107) ),c("PC2","Chrome", "Safari"),F,"iMac_all_browsers_FAST_Average",T,"")
PLOT4(list( c(109,111,113), c(103,105,107) ),c("PC2","Chrome", "Safari"),F,"iMac_all_browsers_FAST_MinMaxAve",F,"")

PLOT4(list( c(110,112,114), c(104,106,108) ),c("PC2","Chrome", "Safari"),F,"iMac_all_browsers_FULL_Average",T,"")
PLOT4(list( c(110,112,114), c(104,106,108) ),c("PC2","Chrome", "Safari"),F,"iMac_all_browsers_FULL_MinMaxAve",F,"")

PLOT4(list( c(109,111,113), c(110,112,114), c(103,105,107), c(104,106,108) ),c("PC2","Chrome (fast)","Chrome (full)", "Safari (fast)", "Safari (full)"),F,"iMac_all_browsers_ALL_Average",T,"")


####------------------------
###-PC1
PLOT4(list( c(85,87,89), c(86,88,90) ),c("Fast", "Full"),F," ",T,"Chrome PC1 (just average)")
PLOT4(list( c(85,87,89), c(86,88,90) ),c("Fast", "Full"),F," ",F,"Chrome PC1 (min, average, max)")
PLOT4(list( c(91,93,95), c(92,94,96) ),c("Fast", "Full"),F," ",T,"Firefox PC1 (just average)")
PLOT4(list( c(91,93,95), c(92,94,96) ),c("Fast", "Full"),F," ",F,"Firefox PC1 (min, average, max)")

##2nd atemp
PLOT4(list( c(115,117,119), c(116,118,120) ),c("Fast", "Full"),F," ",T,"Firefox PC1 (just average)")
PLOT4(list( c(115,117,119), c(116,118,120) ),c("Fast", "Full"),F," ",F,"Firefox PC1 (min, average, max)")
##3rd atemp
PLOT4(list( c(121,123,125), c(122,124,126) ),c("Fast", "Full"),F," ",T,"Firefox PC1 (just average)")
PLOT4(list( c(121,123,125), c(122,124,126) ),c("Fast", "Full"),F," ",F,"Firefox PC1 (min, average, max)")

PLOT4(list( c(97,99,101), c(98,100,102) ),c("Fast", "Full"),F," ",T,"Edge PC1 (just average)")
PLOT4(list( c(97,99,101), c(98,100,102) ),c("Fast", "Full"),F," ",F,"Edge PC1 (min, average, max)")

PLOT4(list( c(85,87,89),  c(115,117,119), c(97,99,101) ),c("PC1","Chrome","Firefox","Edge" ),F,"windows_all_browsers_FAST_average",T,"")
PLOT4(list( c(86,88,90),  c(116,118,120), c(98,100,102) ),c("PC1","Chrome","Firefox","Edge" ),F,"windows_all_browsers_FULL_average",T,"")

PLOT4(list( c(85,87,89), c(115,117,119), c(97,99,101), c(86,88,90),  c(116,118,120), c(98,100,102) ),c("PC1","Chrome (fast)", "Firefox (fast)","Edge (fast)","Chrome (full)", "Firefox (full)","Edge (full)"),F,"windows_all_browsers_ALL_Average",T,"")


####------------------------
###-Compare betwean PC1 and PC1
PLOT4(list( c(85,87,89), c(109,111,113) ),c("PC1", "PC2"),F,"compare_chrome_FAST_average",T,"Chrome (compare fast test)")
PLOT4(list( c(85,87,89), c(71,83) ),c("PC1", "PC2"),F," ",F,"Chrome fast test(min,average,max)")

PLOT4(list( c(86,88,90), c(110,112,114) ),c("PC1", "PC2"),F,"compare_chrome_FULL_average",T,"Chrome (compare full test)")
PLOT4(list( c(86,88,90), c(72,84) ),c("PC1", "PC2"),F," ",F,"Chrome full test(min,avergae,max)")

PLOT4(list( c(91,93,95), c(73) ),c("PC1", "PC2"),F," ",T,"Firefox fast test(just average)")
PLOT4(list( c(91,93,95), c(73) ),c("PC1", "PC2"),F," ",F,"Firefox fast test(min,average,max)")

PLOT4(list( c(92,94,96), c(74) ),c("PC1", "PC2"),F," ",T,"Firefox full test(just average)")
PLOT4(list( c(92,94,96), c(74) ),c("PC1", "PC2"),F," ",F,"Firefox full test(min,avergae,max)")

#Compare between load and no load
PLOT4(list( c(85,87,89), c(131,132,133) ,c(128,129,130) ),c("no load", "game load", "flash load"),F,"compare_chrome_LOAD_fast_average",T,"Chrome (compare fast test)")
PLOT4(list( c(85,87,89), c(131,132,133) ,c(128,129,130) ),c("no load", "game load", "flash load"),F,"compare_chrome_LOAD_fast_(min,max,aver)",F,"Chrome fast test(min,average,max)")


##Safari iMac
# 69 fast test, 70 full test
# 75 fast test, 76 full test
# 77 fast test, 78 full test
# 79 fast test, 81 full test

#2nd Safari atemp
# 103 fast test, 104 full test
# 105 fast test, 106 full test
# 107 fast test, 108 full test

##Chrome iMac                   Windows
# 71 fast test, 72 full test    85 fast test, 86 full test
# 83 fast test, 84 full test    87 fast test, 88 full test
#                               89 fast test, 90 full test
##2nd atemp
# 109 fast test, 110 full test
# 111 fast test, 112 full test
# 113 fast test, 114 full test

##Firefox iMac                  Windows
# 73 fast test, 74 full test    91 fast test, 92 full test ????
#                               93 fast test, 94 full test
#                               95 fast test, 96 full test

##Edge                          Windows
#                               97 fast test, 98 full test
#                               99 fast test, 100 full test
#                               101 fast test, 102 full test

plot(c(100,300,500,1000,2000),c(15.00010,15.00010,15.00010,15.0002,350.00010),type = "n",xlab="run time in milliseconds",ylab="frame rendering time",main="Non z-buffer (min, max)")
data<-c(All_Data[[find_run_id(164)]]["MIN"][,1]   ,All_Data[[find_run_id(164)]]["MAX"][,1])
points(c(100,100),data,type="o")
data<-c(All_Data[[find_run_id(165)]]["MIN"][,1]   ,All_Data[[find_run_id(165)]]["MAX"][,1])
points(c(100,100),data,type="o")
data<-c(All_Data[[find_run_id(166)]]["MIN"][,1]   ,All_Data[[find_run_id(166)]]["MAX"][,1] )
points(c(500,500),data,type="o")
data<-c(All_Data[[find_run_id(167)]]["MIN"][,1]   ,All_Data[[find_run_id(167)]]["MAX"][,1] )
points(c(500,500),data,type="o")
data<-c(All_Data[[find_run_id(168)]]["MIN"][,1]   ,All_Data[[find_run_id(168)]]["MAX"][,1] )
points(c(1000,1000),data,type="o")
data<-c(All_Data[[find_run_id(169)]]["MIN"][,1]   ,All_Data[[find_run_id(169)]]["MAX"][,1] )
points(c(1000,1000),data,type="o")
data<-c(All_Data[[find_run_id(170)]]["MIN"][,1]   ,All_Data[[find_run_id(170)]]["MAX"][,1] )
points(c(1500,1500),data,type="o")
data<-c(All_Data[[find_run_id(171)]]["MIN"][,1]   ,All_Data[[find_run_id(171)]]["MAX"][,1] )
points(c(1500,1500),data,type="o")
data<-c(All_Data[[find_run_id(172)]]["MIN"][,1]   ,All_Data[[find_run_id(172)]]["MAX"][,1] )
points(c(1750,1750),data,type="o")
data<-c(All_Data[[find_run_id(173)]]["MIN"][,1]   ,All_Data[[find_run_id(173)]]["MAX"][,1] )
points(c(1750,1750),data,type="o")
data<-c(All_Data[[find_run_id(174)]]["MIN"][,1]   ,All_Data[[find_run_id(174)]]["MAX"][,1] )
points(c(2000,2000),data,type="o")
data<-c(All_Data[[find_run_id(175)]]["MIN"][,1]   ,All_Data[[find_run_id(175)]]["MAX"][,1] )
points(c(2000,2000),data,type="o")
data<-c(All_Data[[find_run_id(176)]]["MIN"][,1]   ,All_Data[[find_run_id(176)]]["MAX"][,1] )
points(c(2500,2500),data,type="o")
data<-c(All_Data[[find_run_id(177)]]["MIN"][,1]   ,All_Data[[find_run_id(177)]]["MAX"][,1] )
points(c(2500,2500),data,type="o")
data<-c(All_Data[[find_run_id(178)]]["MIN"][,1]   ,All_Data[[find_run_id(178)]]["MAX"][,1] )
points(c(3000,3000),data,type="o")
data<-c(All_Data[[find_run_id(179)]]["MIN"][,1]   ,All_Data[[find_run_id(179)]]["MAX"][,1] )
points(c(3000,3000),data,type="o")


data<-c(All_Data[[find_run_id(180)]]["MIN"][,1]   ,All_Data[[find_run_id(180)]]["MAX"][,1])
points(c(100,100),data,type="o",col="blue")
data<-c(All_Data[[find_run_id(181)]]["MIN"][,1]   ,All_Data[[find_run_id(181)]]["MAX"][,1])
points(c(100,100),data,type="o",col="blue")
data<-c(All_Data[[find_run_id(182)]]["MIN"][,1]   ,All_Data[[find_run_id(182)]]["MAX"][,1] )
points(c(500,500),data,type="o",col="blue")
data<-c(All_Data[[find_run_id(183)]]["MIN"][,1]   ,All_Data[[find_run_id(183)]]["MAX"][,1] )
points(c(500,500),data,type="o",col="blue")
data<-c(All_Data[[find_run_id(184)]]["MIN"][,1]   ,All_Data[[find_run_id(184)]]["MAX"][,1] )
points(c(1000,1000),data,type="o",col="blue")
data<-c(All_Data[[find_run_id(185)]]["MIN"][,1]   ,All_Data[[find_run_id(185)]]["MAX"][,1] )
points(c(1000,1000),data,type="o",col="blue")
data<-c(All_Data[[find_run_id(186)]]["MIN"][,1]   ,All_Data[[find_run_id(186)]]["MAX"][,1] )
points(c(1500,1500),data,type="o",col="blue")
data<-c(All_Data[[find_run_id(187)]]["MIN"][,1]   ,All_Data[[find_run_id(187)]]["MAX"][,1] )
points(c(1500,1500),data,type="o",col="blue")
data<-c(All_Data[[find_run_id(188)]]["MIN"][,1]   ,All_Data[[find_run_id(188)]]["MAX"][,1] )
points(c(1750,1750),data,type="o",col="blue")
data<-c(All_Data[[find_run_id(189)]]["MIN"][,1]   ,All_Data[[find_run_id(189)]]["MAX"][,1] )
points(c(1750,1750),data,type="o",col="blue")
data<-c(All_Data[[find_run_id(190)]]["MIN"][,1]   ,All_Data[[find_run_id(190)]]["MAX"][,1] )
points(c(2000,2000),data,type="o",col="blue")
data<-c(All_Data[[find_run_id(191)]]["MIN"][,1]   ,All_Data[[find_run_id(191)]]["MAX"][,1] )
points(c(2000,2000),data,type="o",col="blue")
data<-c(All_Data[[find_run_id(192)]]["MIN"][,1]   ,All_Data[[find_run_id(192)]]["MAX"][,1] )
points(c(2500,2500),data,type="o",col="blue")
data<-c(All_Data[[find_run_id(193)]]["MIN"][,1]   ,All_Data[[find_run_id(193)]]["MAX"][,1] )
points(c(2500,2500),data,type="o",col="blue")
data<-c(All_Data[[find_run_id(194)]]["MIN"][,1]   ,All_Data[[find_run_id(194)]]["MAX"][,1] )
points(c(3000,3000),data,type="o",col="blue")
data<-c(All_Data[[find_run_id(195)]]["MIN"][,1]   ,All_Data[[find_run_id(195)]]["MAX"][,1] )
points(c(3000,3000),data,type="o",col="blue")

data<-c(All_Data[[find_run_id(196)]]["MIN"][,1]   ,All_Data[[find_run_id(196)]]["MAX"][,1])
points(c(100,100),data,type="o",col="red")
data<-c(All_Data[[find_run_id(197)]]["MIN"][,1]   ,All_Data[[find_run_id(197)]]["MAX"][,1])
points(c(100,100),data,type="o",col="red")
data<-c(All_Data[[find_run_id(198)]]["MIN"][,1]   ,All_Data[[find_run_id(198)]]["MAX"][,1] )
points(c(500,500),data,type="o",col="red")
data<-c(All_Data[[find_run_id(199)]]["MIN"][,1]   ,All_Data[[find_run_id(199)]]["MAX"][,1] )
points(c(500,500),data,type="o",col="red")
data<-c(All_Data[[find_run_id(200)]]["MIN"][,1]   ,All_Data[[find_run_id(200)]]["MAX"][,1] )
points(c(1000,1000),data,type="o",col="red")
data<-c(All_Data[[find_run_id(201)]]["MIN"][,1]   ,All_Data[[find_run_id(201)]]["MAX"][,1] )
points(c(1000,1000),data,type="o",col="red")
data<-c(All_Data[[find_run_id(202)]]["MIN"][,1]   ,All_Data[[find_run_id(202)]]["MAX"][,1] )
points(c(1500,1500),data,type="o",col="red")
data<-c(All_Data[[find_run_id(203)]]["MIN"][,1]   ,All_Data[[find_run_id(203)]]["MAX"][,1] )
points(c(1500,1500),data,type="o",col="red")
data<-c(All_Data[[find_run_id(204)]]["MIN"][,1]   ,All_Data[[find_run_id(204)]]["MAX"][,1] )
points(c(1750,1750),data,type="o",col="red")
data<-c(All_Data[[find_run_id(205)]]["MIN"][,1]   ,All_Data[[find_run_id(205)]]["MAX"][,1] )
points(c(1750,1750),data,type="o",col="red")
data<-c(All_Data[[find_run_id(206)]]["MIN"][,1]   ,All_Data[[find_run_id(206)]]["MAX"][,1] )
points(c(2000,2000),data,type="o",col="red")
data<-c(All_Data[[find_run_id(207)]]["MIN"][,1]   ,All_Data[[find_run_id(207)]]["MAX"][,1] )
points(c(2000,2000),data,type="o",col="red")
data<-c(All_Data[[find_run_id(208)]]["MIN"][,1]   ,All_Data[[find_run_id(208)]]["MAX"][,1] )
points(c(2500,2500),data,type="o",col="red")
data<-c(All_Data[[find_run_id(209)]]["MIN"][,1]   ,All_Data[[find_run_id(209)]]["MAX"][,1] )
points(c(2500,2500),data,type="o",col="red")
data<-c(All_Data[[find_run_id(210)]]["MIN"][,1]   ,All_Data[[find_run_id(210)]]["MAX"][,1] )
points(c(3000,3000),data,type="o",col="red")
data<-c(All_Data[[find_run_id(211)]]["MIN"][,1]   ,All_Data[[find_run_id(211)]]["MAX"][,1] )
points(c(3000,3000),data,type="o",col="red")
legend(100,100.00012,c("No load","Flash","Game"), c("black","blue","red"),title="")


test<-function(black,blue,red,time,main_t){
  plot(c(100,300,500,1000,2000),c(0.00010,0.00010,0.00010,600.0002,80.00010),type = "n",xlab="run time in milliseconds",ylab="frame rendering time",main=main_t)
  for(i in seq(1,length(black),1) ){
    data<-c(All_Data[[find_run_id(black[i])]]["MIN"][,1]   ,All_Data[[find_run_id(black[i])]]["MAX"][,1] )
    points(c(time[i],time[i]),data,type="o")
  }
  for(i in seq(1,length(blue),1) ){
    print(blue[i])
    data<-c(All_Data[[find_run_id(blue[i])]]["MIN"][,1]   ,All_Data[[find_run_id(blue[i])]]["MAX"][,1] )
    points(c(time[i],time[i]),data,type="o",col="blue")
  }
  if(length(red) > 1){
    for(i in seq(1,length(red),1) ){
      data<-c(All_Data[[find_run_id(red[i])]]["MIN"][,1]   ,All_Data[[find_run_id(red[i])]]["MAX"][,1] )
      points(c(time[i],time[i]),data,type="o",col="red")
    }
    legend(100,200,c("No load","Flash","Game"), c("black","blue","red"))
  }else{
    legend(100,200,c("No load","Flash"), c("black","blue"))  
  }
}


black=c(166,167,292,293,168,169,294,295,170,171,172,173,174,175,176,177,178,179)
blue= c(182,183,280,281,184,185,282,283,186,187,188,189,190,191,192,193,194,195)
red=  c(198,199,268,269,200,201,270,271,202,203,204,205,206,207,208,209,210,211)
time= c(500,500,750,750,1000,1000,1250,1250,1500,1500,1750,1750,2000,2000,2500,2500,3000,3000)
test(black,blue,red,time,"Non Z-Buffer min-max (Crhome windows)")

black=c(286,287,216,217,288,289,218,219,220,221,222,223,224,225,226,227)
blue= c(274,275,232,233,276,277,234,235,236,237,238,239,240,241,242,243)
red=  c(262,263,248,249,264,265,250,251,252,253,254,255,256,257,258,259)
time= c(750,750,1000,1000,1250,1250,1500,1500,1750,1750,2000,2000,2500,2500,3000,3000)
test(black,blue,red,time,"Non Z-Buffer min-max (Firefox windows)")

black=c(300,301,302,303,304,305,306,307,308,309,310,311,312,313,314,315,316,317)
blue= c(322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339)
red=  c()
time= c(500,500,750,750,1000,1000,1250,1250,1500,1500,1750,1750,2000,2000,2500,2500,3000,3000)
test(black,blue,red,time,"Non Z-Buffer min-max (Chrome iMac)")

black=c(348,349,346,347,350,351,352,353,354,355,356,357,358,359,360,361)
blue= c(368,369,370,371,372,373,374,375,376,377,378,379,380,381,382,383)
red=  c()
time= c(750,750,1000,1000,1250,1250,1500,1500,1750,1750,2000,2000,2500,2500,3000,3000)
test(black,blue,red,time,"Non Z-Buffer min-max (Firefox iMac)")


black=c(388,389,390,391,392,393,394,395,396,397,398,399,400,401,402,403,404,405)
blue= c(410,411,412,413,414,415,416,417,418,419,420,421,422,423,424,425,426,427)
red=  c()
time= c(500,500,750,750,1000,1000,1250,1250,1500,1500,1750,1750,2000,2000,2500,2500,3000,3000)
test(black,blue,red,time,"Non Z-Buffer min-max (Safari iMac)")

#mobile
black=c(477,478,479,480,481,482,483,484,485,486,487,488,489,490,491,492,493,494,495,496,497,498)
blue=c(456,457,458,459,460,461,454,455,462,463,464,465,466,467,468,469,470,471,473,474,475,476)
red=c()
time=c(100,100,250,250,500,500,750,750,1000,1000,1250,1250,1500,1500,1750,1750,2000,2000,2500,2500,3000,3000)
test(black,blue,red,time,"Locality ( iOS Safari)")

black=c(524,525,526,527,528,529,530,531,532,533,534,535,536,537,538,539,540,541,542,543,544,545)
blue= c(500,501,502,503,504,505,506,507,509,510,511,512,513,514,515,516,517,518,520,521,522,523)
red=c()
time=c(100,100,250,250,500,500,750,750,1000,1000,1250,1250,1500,1500,1750,1750,2000,2000,2500,2500,3000,3000)
test(black,blue,red,time,"Locality ( iOS Chrome)")

black=c(590,591,592,593,594,595,596,597,599,600,601,602,603,604,605,606,607,608,609,610,611,612)
blue= c(546,547,548,549,550,551,552,553,554,555,556,557,558,559,560,561,562,563,564,565,566,567)
red=c()
time=c(100,100,250,250,500,500,750,750,1000,1000,1250,1250,1500,1500,1750,1750,2000,2000,2500,2500,3000,3000)
test(black,blue,red,time,"Locality ( Android Standart)")

black=c(613,614,615,616,617,618,619,620,621,622,623,624,625,626,627,628,629,630,631,632,633,634)
blue= c(568,569,570,571,572,573,574,575,576,577,578,579,580,581,582,583,584,585,586,587,588,589)
red=c()
time=c(100,100,250,250,500,500,750,750,1000,1000,1250,1250,1500,1500,1750,1750,2000,2000,2500,2500,3000,3000)
test(black,blue,red,time,"Locality ( Android Chrome)")
