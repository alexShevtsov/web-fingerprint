rm(list=ls())
setwd("/home/parker/FORTH/fingerprint")
#
##---Read all data from file
A<-read.table("./DATA/gpu_cube.txt",header = T)
#
##---Take all unique id's
UN_ID <- unique(A["RUN_ID"])
#sort id to enc order
UN_ID[] <- sort(UN_ID[,])

##---Put all data to a list
All_Data<-list();
for(data_count in 1:dim(UN_ID)[1]){
  Data_1 <- A[A["RUN_ID"] == UN_ID[data_count,],]
  All_Data[[data_count]] <- unique(Data_1);
}

All_Time <-c();
for(i in 1:dim(UN_ID)[1]){
  All_Time <- c(All_Time , A[A["RUN_ID"] == UN_ID[i,],]["TIME"][,1])
}
All_Time <- sort(All_Time)
##Data explain
#           # TOTALO - Total objects rendered
#           # FPS - frame rate
#           # FO - Objects / fps
#           # TIME -Rendering time
#           # STEP - Step of objects grow
#           # RUN_ID - run id

#as.matrix(All_Data[[12]])[All_Data[[12]]["STEP"] == 50,]

DataColor<-c("orange2", "gray","brown", "blue", "black","yellow", "green", "red", "darkorchid", "deeppink")

color_set <-function(data_set){
  ip <- c()
  for(i in data_set){
    ip <- c(ip, as.vector(All_Data[[i]]["IP"][1,1]) )
  }
  u_ip <- unique(ip)
  color_data <- c()
  for(i in ip){
    color_data <- c(color_data , DataColor[which(u_ip == i)])
  }
  return (color_data)
}

####----------####----------####----------####----------####----------####----------####----------####----------####----------
#---------------------------------------------Plot every single ip in other plot---------------------------------------------#
####----------####----------####----------####----------####----------####----------####----------####----------####----------
PLOT2 <- function(to_file){
  color_c <- 1;
  yLabel <- "Render time (miliseconds)"
  xLabel <- "Number of Objects"
  gLabel <- "Gpu (non z-buffer) rendering"
  for(l in 1:length(All_Data)){
    ##---Plot mean data  
    ##---Calculate min and max for first plot
    #take sub of data
    Data<-All_Data[[l]][All_Data[[l]]["STEP"] == 50,]
    
    ##---Calculate min and max for second plot
    DataMin <- min(Data["MIN"][,1])
    DataMax <- max(Data["MAX"][,1])
    if(!to_file){
      dev.new()
    }else{
      name <- gsub(" ","",paste( "plots/gpu-nonz_res/single/gpu-z_" , substr(as.matrix(Data["IP"])[1,],8,13), ".jpg"))
      jpeg(filename = name,
           width = 800 , height = 600)
    }
    Plot2 <- seq(DataMin, DataMax, by=((DataMax-DataMin) / (length(Data["START"][, 1]) -1 ) ));
    
    plot(Data["START"][, 1],
         Plot2,
         type="n",
         col=DataColor[color_c], 
         xlab=xLabel,
         ylab=yLabel, 
         main=gLabel
    );
    legendIP<-c()
    legendIP[1]<- substr(as.matrix(Data["IP"])[1,],8,13)
    xData <- Data["START"][,1]
    for(k in 1:length(xData)){
      Max <- Data["MIN"][,1][k] 
      Min <- Data["MAX"][,1][k]
      points(c(xData[k], xData[k]), c(Max, Min), type="l")
      points(c(xData[k], xData[k]), c(Max, Min), type="o",col=DataColor[color_c])
      points(xData[k],Data["AVE"][,1][k],pch="+",col=DataColor[color_c])
    }
    
    print(DataColor[color_c])
    print(Data["IP"][1,1])
    
    legend(min(Data["START"][,1]),DataMax, 
           legendIP,DataColor[c(0,color_c)])
    color_c<-color_c+1;
    if(color_c > length(DataColor)) color_c <-1
    if(to_file) dev.off()
  }
}
####----------####----------####----------####----------####----------####----------####----------####----------
#--------------------------------------------Plot all data in groups-------------------------------------------#
####----------####----------####----------####----------####----------####----------####----------####----------
PLOT3<-function(data_num, to_file, myname, my_label,step,typevar){
  ######------------------------Mean Plot------------------------------------------####
  ##---Plot pair data of same ip 
  
  color_c <- 1;
  yLabel <- "Objects/fps"
  xLabel <- "Rendering time (miliseconds)"
  gLabel <- paste("Gpu (cube) rendering ",my_label)
  
  ##---Calculate min and max for first plot
  DataList<-c()
  
  for(i in 1:19){
    DataList <- c(DataList, All_Data[[i]][All_Data[[i]]["FPS"] != 0,][typevar][,1])
  }

  All_Time <- c()
  for(i in data_num){
    All_Time <- c(All_Time, All_Data[[i]][All_Data[[i]]["STEP"] == step, ]["TIME"][,1])  
  }
  
  DataMin <- min(DataList)
  DataMax <- max(DataList)

  color_data <- color_set(data_num)
  
  ##
  if(!to_file){
    dev.new()
  }else{
    if(myname == ""){
      name <- gsub(" ","",paste( "plots/gpu-cube_res/single/gpu-z_" , substr(as.matrix(All_Data[[ data_num[1] ]]["IP"])[1,],8,13), ".jpg"))
    }else{
      name <- gsub(" ","",paste("plots/gpu-cube_res/group/",myname))
    }
    jpeg(filename = name,
         width = 800 , height = 600)
  }
  Plot1 <- seq(DataMin, DataMax, by=((DataMax-DataMin) / (length(All_Time) -1 ) ));
  ## -- ploting
  legendIP <- c();
  plot(All_Time, Plot1,type="n",xlab=xLabel,ylab=yLabel,main=gLabel);
  
  for(i in data_num){
    legendIP[which(data_num == i)]<-substr(as.matrix(All_Data[[i]]["IP"])[1,],8,13)
    
    xData <- All_Data[[i]][All_Data[[i]]["STEP"] == step,]["TIME"][,1]
    
    for(k in 1:length(xData)){
      if(k+1 <= length(xData)){
        points(c(xData[k], xData[k+1]) ,
               c(All_Data[[i]][All_Data[[i]]["STEP"] == step,][typevar][,1][k] , All_Data[[i]][All_Data[[i]]["STEP"] == step,][typevar][,1][k+1] ),
               type = "l")
      }
      points(xData[k],All_Data[[i]][All_Data[[i]]["STEP"] == step,][typevar][,1][k],pch="o",col=color_data[which(data_num == i)])
    }

    print(DataColor[i])
    print(All_Data[[i]]["IP"][1,1])
  }
  
  legend(min(All_Time),DataMax, 
         legendIP,color_data)
  if(to_file) dev.off()
}
# -- data_num, to_file, myname, my_label,step
PLOT2(F)

PLOT3(c(1,13,14,11,12,8,10,2,3),F,"group_1_10.jpg"," ","10","FO")
PLOT3(c(1,13,14,11,12,8,10,2,3),F,"group_1_20.jpg"," ","20","FO")
PLOT3(c(1,13,14,11,12,8,10,2,3),F,"group_1_30.jpg"," ","30","FO")
PLOT3(c(1,13,14,11,12,8,10,2,3),F,"group_1_40.jpg"," ","40","FO")
PLOT3(c(1,13,14,11,12,8,10,2,3),F,"group_1_50.jpg"," ","50","FO")
PLOT3(c(1,13,14,11,12,8,10,2,3),F,"group_1_60.jpg"," ","60","FO")
PLOT3(c(1,13,14,11,12,8,10,2,3),F,"group_1_70.jpg"," ","70","FO")


PLOT3(c(15,16,17,18,4,5,6,7,9),F,"group_2_10.jpg"," ","10","FO")
PLOT3(c(15,16,17,18,4,5,6,7,9),F,"group_2_20.jpg"," ","20","FO")
PLOT3(c(15,16,17,18,4,5,6,7,9),F,"group_2_30.jpg"," ","30","FO")
PLOT3(c(15,16,17,18,4,5,6,7,9),F,"group_2_40.jpg"," ","40","FO")
PLOT3(c(15,16,17,18,4,5,6,7,9),F,"group_2_50.jpg"," ","50","FO")
PLOT3(c(15,16,17,18,4,5,6,7,9),F,"group_2_60.jpg"," ","60","FO")
PLOT3(c(15,16,17,18,4,5,6,7,9),F,"group_2_70.jpg"," ","70","FO")
