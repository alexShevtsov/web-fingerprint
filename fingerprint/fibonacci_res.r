rm(list=ls())
setwd("/home/parker/FORTH/fingerprint")
#
##---Read all data from file
A<-read.table("./DATA/fib.txt",header = T)
A<-read.table("./DATA/test_fib.txt",header = T)
#
##---Take all unique id's
UN_ID <- unique(A["RUN_ID"])
#
##---Put all data to a list
All_Data<-list();
for(data_count in 1:dim(UN_ID)[1]){
  Data_1 <- A[A["RUN_ID"] == UN_ID[data_count,],]
  All_Data[[data_count]] <- Data_1[];

}

##########################################################
####---Help function that find position of RUN_ID (id)--##
##########################################################
find_run_id<-function(id){
  for(i in 1:length(All_Data)){
    if(All_Data[[i]]["RUN_ID"][1,] == id){
      return(i)
    }
  }
}
DataColor<-c("orange2", "brown", "gray", "red", "black", "blue", "green","yellow", "darkorchid", "deeppink")
#DataColor<-c("orange2", "gray","brown", "blue", "yellow", "black", "green", "red", "darkorchid", "deeppink")

color_set <-function(data_set){
  ip <- c()
  for(i in data_set){
    ip <- c(ip, as.vector(All_Data[[i]]["IP"][1,1]) )
  }
  u_ip <- unique(ip)
  color_data <- c()
  for(i in ip){
    color_data <- c(color_data , DataColor[which(u_ip == i)])
  }
  return (color_data)
}

################################################################################
######----------------------------------------------------------------------####
################################################################################
PLOT2 <- function(varType,to_file){
  options("scipen"=100, "digits"=8)
  color_c <- 1;
  for(l in 1:length(All_Data)){
    ##---Plot mean data  
    ##---Calculate min and max for first plot
    yLabel <- paste(varType,"results"," ")
    
    ##---Calculate min and max for second plot
    DataMin <-  10000
    DataMax <- -10000
    xData <- All_Data[[l]]["DEP"][,1]*All_Data[[l]]["REP"][,1]
    for(k in 1:length(xData)){
      errorMax <- All_Data[[l]][varType][,1][k] + All_Data[[l]]["VAR"][,1][k]
      errorMin <- All_Data[[l]][varType][,1][k] - All_Data[[l]]["VAR"][,1][k]
      DataMin<-min(DataMin,errorMin)
      DataMax<-max(DataMax,errorMax)
    }

    if(!to_file){
      dev.new()
    }else{
      name <- gsub(" ","",paste( paste("fib_res/single/",tolower(varType)) , substr(as.matrix(All_Data[[l]]["IP"])[1,],8,13), ".jpg"))
      jpeg(filename = name,
           width = 800 , height = 600)
    }
    Plot2 <- seq(DataMin, DataMax, by=((DataMax-DataMin) / 80));
    
    plot(All_Data[[1]]["DEP"][, 1]*All_Data[[1]]["REP"][, 1],
         Plot2,
         type="n",
         xlab="Deep and repeat relation",
         ylab=yLabel, 
         main=paste("Fibonacci ", yLabel, " ")
    );
    points(All_Data[[l]]["DEP"][,1]*All_Data[[l]]["REP"][,1] , All_Data[[l]][varType][,1],pch="o",col=DataColor[color_c]);
    legendIP<-c()
    legendIP[1]<- substr(as.matrix(All_Data[[l]]["IP"])[1,],8,13)
    xData <- All_Data[[l]]["DEP"][,1]*All_Data[[l]]["REP"][,1]
    for(k in 1:length(xData)){
      errorMax <- All_Data[[l]][varType][,1][k] + All_Data[[l]]["VAR"][,1][k]
      errorMin <- All_Data[[l]][varType][,1][k] - All_Data[[l]]["VAR"][,1][k]
      points(c(xData[k], xData[k]), c(errorMax, errorMin), type="l")
    }
    max_v <- max(All_Data[[l]]["VAR"][,1])
    legendIP[2]<-paste("Max variance ('+' or '-'): ",max_v, " ")
    print(DataColor[color_c])
    print(All_Data[[l]]["IP"][1,1])
    
    legend(max(All_Data[[l]]["DEP"][,1]*All_Data[[l]]["REP"][,1])*0.35,DataMin+(DataMax-DataMin)/8, 
           legendIP,DataColor[c(0,color_c)])
    color_c<-color_c+1;
    if(color_c > length(DataColor)) color_c <-1
    if(to_file) dev.off()
  }
}
###############################################################################################
####---------------------------------------------------------------------------------------####
###############################################################################################
PLOT3<-function(varType, data_num, to_file, info_var, myname, error_cor){
  options("scipen"=100, "digits"=8)
  ######------------------------Mean Plot------------------------------------------####
  ##---Plot pair data of same ip 
  ##---Calculate min and max for first plot
  yLabel <- paste(varType,"results"," ")
  ##---Calculate min and max for second plot
  DataMin <-  10000
  DataMax <- -10000
  
  color_data <- color_set(data_num)
  
  for(l in 1:19){
    xData <- All_Data[[l]]["DEP"][,1]*All_Data[[l]]["REP"][,1]
    for(k in 1:19){
      if(error_cor){
        ## with error correction
        errorMax <- All_Data[[l]][varType][,1][k] + All_Data[[l]]["VAR"][,1][k]
        errorMin <- All_Data[[l]][varType][,1][k] - All_Data[[l]]["VAR"][,1][k]
      }else{
        ## without  error correction
        errorMax <- All_Data[[l]][varType][,1][k]
        errorMin <- All_Data[[l]][varType][,1][k]
      }
      DataMin<-min(DataMin,errorMin)
      DataMax<-max(DataMax,errorMax)
    }
  }
  ##
  if(!to_file){
    dev.new()
  }else{
    if(myname == ""){
      name <- gsub(" ","",paste( paste("fib_res/group/",tolower(varType)) , substr(as.matrix(All_Data[[data_num[1]]]["IP"])[1,],8,13), ".jpg"))
    }else{
      name <- gsub(" ","",paste("fib_res/group/",myname))
    }
      jpeg(filename = name,
         width = 800 , height = 600)
  }
  Plot1<-seq(DataMin,DataMax,by=((DataMax-DataMin)/80));
  ## -- ploting
  legendIP <- c();
  plot(All_Data[[1]]["DEP"][,1]*All_Data[[1]]["REP"][,1], Plot1,type="n",xlab="Deep and repeat relation",ylab=yLabel,main=paste("Fibonacci ",yLabel," "));
  
  for(i in data_num){
    legendIP[which(data_num == i)]<-substr(as.matrix(All_Data[[i]]["IP"])[1,],8,13)
    points(All_Data[[i]]["DEP"][,1]*All_Data[[i]]["REP"][,1] , All_Data[[i]][varType][,1],pch="o",col=color_data[which(data_num == i)]);
    xData <- All_Data[[i]]["DEP"][,1]*All_Data[[i]]["REP"][,1]
    for(k in 1:length(xData)){
      errorMax <- All_Data[[i]][varType][,1][k] + All_Data[[i]]["VAR"][,1][k]
      errorMin <- All_Data[[i]][varType][,1][k] - All_Data[[i]]["VAR"][,1][k]
      
      points(c(xData[k], xData[k]), c(errorMax, errorMin), type="l")
    }
    max_v <- max(All_Data[[i]]["VAR"][,1])
    if(info_var) legendIP[which(data_num == i) + length(data_num)] <- paste("Max variance ('+' or '-'): ",max_v, " ")
    print(color_data[i])
    print(All_Data[[i]]["IP"][1,1])
  }
  if(error_cor){
    legend(max(All_Data[[1]]["DEP"][,1]*All_Data[[1]]["REP"][,1])*0.1,DataMin+(DataMax-DataMin)/1.1, 
           legendIP,color_data)
  }else{
    legend(max(All_Data[[1]]["DEP"][,1]*All_Data[[1]]["REP"][,1])*0.1,DataMax, 
           legendIP,color_data)
  }
  if(to_file) dev.off()
}
####----------####----------####----------####----------####----------####----------####----------####----------
#--------------------------------------------Plot all data in groups of fast and small test-------------------------------------------#
####----------####----------####----------####----------####----------####----------####----------####----------
PLOT4<-function(varType, data_num, label, to_file, myname, my_header,my_label){
  #options("scipen"=100, "digits"=8)
  ######------------------------Mean Plot------------------------------------------####
  ##---Plot pair data of same ip 
  ##---Calculate min and max for first plot
  yLabel <-""
  DataList<-c()
  v<-find_run_id(69)
  for(i in v:dim(UN_ID)[1]){
    if(i != 67 && i != 69 && i != 50 && i!=97){
      DataList <- c(DataList, All_Data[[ i ]][varType][,1])
    }
  }
  DataMin <- min(DataList)
  DataMax <- max(DataList)

  #color_data <- color_set(data_num)
  ##
  if(varType == "DEV"){
    yLabel<-"Deviation"
  }else{
    yLabel<-"Meaning"
  }
  
  if(!to_file){
    dev.new()
  }else{
    if(myname == ""){
      #name <- gsub(" ","",paste( paste("fib_res/group/",tolower(varType)) , substr(as.matrix(All_Data[[i]]["IP"])[1,],8,13), ".jpg"))
    }else{
      name <- gsub(" ","",paste("plots/fib_res/group/",myname))
    }
    jpeg(filename = name,
         width = 800 , height = 600)
  }
  Plot1<-seq(DataMin,DataMax,by=((DataMax-DataMin)/80));
  ## -- ploting
  legendIP <- c();
  plot(All_Data[[1]]["DEP"][,1]*All_Data[[1]]["REP"][,1], Plot1,type="n",xlab="Deep and repeat relation",ylab=yLabel,main=paste("",paste(my_label,my_header," ")," "));
  color_num<-1;
  for(p in data_num){
    for(i in p){
      g<-find_run_id(i)
      points(All_Data[[g]]["DEP"][,1]*All_Data[[g]]["REP"][,1] , All_Data[[g]][varType][,1],pch="o",col=DataColor[color_num]);
      xData <- All_Data[[g]]["DEP"][,1]*All_Data[[g]]["REP"][,1]
      for(k in 1:length(xData)){
        errorMax <- All_Data[[g]][varType][,1][k] + All_Data[[g]]["VAR"][,1][k]
        errorMin <- All_Data[[g]][varType][,1][k] - All_Data[[g]]["VAR"][,1][k]
        
        points(c(xData[k], xData[k]), c(errorMax, errorMin), type="l")
      }
      max_v <- max(All_Data[[g]]["VAR"][,1])
    }
    color_num<-color_num+1
  }
  
  title_name<-NULL
  if(length(label) > length(data_num)){
    title_name<-label[1]
    label<- label[2:length(label)]
  }
  legend(max(All_Data[[1]]["DEP"][,1]*All_Data[[1]]["REP"][,1])*0.1,DataMax, 
         label, DataColor[1:color_num],title=title_name)
  if(to_file) dev.off()
}
##------------------------------------------END-------------------------------------------------##

PLOT2("MEAN",T)
PLOT2("DEV",T)
PLOT3("MEAN",c(6,7,19),F,T,"",F)
#varType, data_num, to_file, info_var, myname, error_cor
PLOT3("MEAN",c(6,7,19), T)#for good plot need y_level of label compute as DataMin+(DataMax-DataMin)/2
PLOT3("DEV",c(6,7,19), T)#same as prev
PLOT3("MEAN",c(15,16), T)#for good plot need y_level of label compute as DataMin+(DataMax-DataMin)/6
PLOT3("DEV",c(15,16), T)#same as prev
PLOT3("MEAN",c(1,17,18), T)#for good plot need y_level of label compute as DataMin+(DataMax-DataMin)/5
PLOT3("DEV",c(1,17,18), T)#same as prev


PLOT3("MEAN", c(6,7,19,15,16,14,1,17,18),F,F,"mean_group1",T)
PLOT3("MEAN", c(2,3,4,5,8,9,10,11,12,13),F,F,"mean_group2",T)
PLOT3("MEAN", c(6,7,19,15,16,14,1,17,18),F,F,"mean_group1_nonError",F)
PLOT3("MEAN", c(2,3,4,5,8,9,10,11,12,13),F,F,"mean_group2_nonError",F)

PLOT3("DEV", c(6,7,19,15,16,14,1,17,18),F,F,"dev_group1",T)
PLOT3("DEV", c(2,3,4,5,8,9,10,11,12,13),F,F,"dev_group2",T)
PLOT3("DEV", c(6,7,19,15,16,14,1,17,18),F,F,"dev_group1_nonError",F)
PLOT3("DEV", c(2,3,4,5,8,9,10,11,12,13),F,F,"dev_group2_nonError",F)

####------------------------
###-PC2
PLOT4("DEV" ,list( c(69,75,77,79), c(70,76,78,81) ),c("Fast", "Full"),F,"","Safari (PC2)","Deviation")
PLOT4("MEAN",list( c(69,75,77,79), c(70,76,78,81) ),c("Fast", "Full"),F,"","Safari (PC2)","Meaning")
##2nd atemp
PLOT4("DEV" ,list( c(103,105,107), c(104,106,108) ),c("Fast", "Full"),F,"","Safari (PC2)","Deviation")
PLOT4("MEAN",list( c(103,105,107), c(104,106,108) ),c("Fast", "Full"),F,"","Safari (PC2)","Meaning")

PLOT4("DEV" ,list( c(71,83), c(72,84) ),c("Fast", "Full"),F,"","Chrome(PC2)","Deviation")
PLOT4("MEAN",list( c(71,83), c(72,84) ),c("Fast", "Full"),F,"","Chrome(PC2)","Meaning")
##2nd atemp
PLOT4("DEV" ,list( c(109,111,113), c(110,112,114) ),c("Fast", "Full"),F,"","Chrome(PC2)","Deviation")
PLOT4("MEAN",list( c(109,111,113), c(110,112,114) ),c("Fast", "Full"),F,"","Chrome(PC2)","Meaning")

PLOT4("DEV" ,list( c(73), c(74) ),c("Fast", "Full"),F,"","Firefox(PC2)","Deviation")
PLOT4("MEAN",list( c(73), c(74) ),c("Fast", "Full"),F,"","Firefox(PC2)","Meaning")


PLOT4("DEV",list( c(110,112,114), c(104,106,108) ),c("PC2","Chrome", "Safari"),F,"iMac_all_browsers_FULL_Dev","","")
PLOT4("MEAN",list( c(110,112,114), c(104,106,108) ),c("PC2","Chrome", "Safari"),F,"iMac_all_browsers_FULL_Mean","","")
PLOT4("DEV",list( c(109,111,113), c(110,112,114) ),c("PC2","Chrome", "Safari"),F,"iMac_all_browsers_FAST_Dev","","")
PLOT4("MEAN",list( c(109,111,113), c(110,112,114) ),c("PC2","Chrome", "Safari"),F,"iMac_all_browsers_FAST_Mean","","")

PLOT4("DEV",list( c(109,111,113), c(110,112,114), c(103,105,107), c(104,106,108) ),c("PC2","Chrome (fast)","Chrome (full)", "Safari (fast)", "Safari (full)"),F,"iMac_all_browsers_ALL_Dev","","")
PLOT4("MEAN",list( c(109,111,113), c(110,112,114), c(103,105,107), c(104,106,108) ),c("PC2","Chrome (fast)","Chrome (full)", "Safari (fast)", "Safari (full)"),F,"iMac_all_browsers_ALL_Mean","","")


####------------------------
###-PC1
PLOT4("DEV" ,list( c(85,87,89), c(86,88,90) ),c("Fast", "Full"),F,"","Chrome(PC1)","Deviation")
PLOT4("MEAN",list( c(85,87,89), c(86,88,90) ),c("Fast", "Full"),F,"","Chrome(PC1)","Meaning")
PLOT4("DEV" ,list( c(91,93,95), c(92,94,96) ),c("Fast", "Full"),F,"","Firefox(PC1)","Deviation")
PLOT4("MEAN",list( c(91,93,95), c(92,94,96) ),c("Fast", "Full"),F,"","Firefox(PC1)","Meaning")
##2nd atemp
PLOT4("DEV" ,list( c(115,117,119), c(116,118,120) ),c("Fast", "Full"),F,"","Firefox(PC1)","Deviation")
PLOT4("MEAN",list( c(115,117,119), c(116,118,120) ),c("Fast", "Full"),F,"","Firefox(PC1)","Meaning")

PLOT4("DEV" ,list( c(97,99,101), c(98,100,102) ),c("Fast", "Full"),F,"","Edge(PC1)","Deviation")
PLOT4("MEAN",list( c(97,99,101), c(98,100,102) ),c("Fast", "Full"),F,"","Edge(PC1)","Meaning")

PLOT4("DEV",list( c(85,87,89),  c(115,117,119), c(97,99,101) ),c("PC1","Chrome","Firefox","Edge" ),F,"windows_all_browsers_FAST_Dev","","")
PLOT4("MEAN",list( c(85,87,89),  c(115,117,119), c(97,99,101) ),c("PC1","Chrome","Firefox","Edge" ),F,"windows_all_browsers_FAST_Mean","","")
PLOT4("DEV",list( c(86,88,90),  c(116,118,120), c(98,100,102) ),c("PC1","Chrome","Firefox","Edge" ),F,"windows_all_browsers_FULL_Dev","","")
PLOT4("MEAN",list( c(86,88,90),  c(116,118,120), c(98,100,102) ),c("PC1","Chrome","Firefox","Edge" ),F,"windows_all_browsers_FULL_Mean","","")

PLOT4("DEV",list( c(85,87,89), c(115,117,119), c(97,99,101), c(86,88,90),  c(116,118,120), c(98,100,102) ),c("PC1","Chrome (fast)", "Firefox (fast)","Edge (fast)","Chrome (full)", "Firefox (full)","Edge (full)"),F,"windows_all_browsers_ALL_Dev","","")
PLOT4("MEAN",list( c(85,87,89), c(115,117,119), c(97,99,101), c(86,88,90),  c(116,118,120), c(98,100,102) ),c("PC1","Chrome (fast)", "Firefox (fast)","Edge (fast)","Chrome (full)", "Firefox (full)","Edge (full)"),F,"windows_all_browsers_ALL_Mean","","")


####------------------------
###-Compare betwean PC1 and PC1
PLOT4("DEV",list( c(85,87,89), c(109,111,113) ),c("PC1", "PC2"),F,"compare_chrome_FAST_Dev","","Chrome (compare fast test)")
PLOT4("MEAN",list( c(85,87,89), c(109,111,113) ),c("PC1", "PC2"),F,"compare_chrome_FAST_Mean","","Chrome (compare fast test)")

PLOT4("DEV",list( c(86,88,90), c(110,112,114) ),c("PC1", "PC2"),F,"compare_chrome_FULL_Dev","","Chrome (compare full test)")
PLOT4("MEAN",list( c(86,88,90), c(110,112,114) ),c("PC1", "PC2"),F,"compare_chrome_FULL_Mean","","Chrome (compare full test)")

PLOT4("DEV" ,list( c(85,87,89), c(71,83) ),c("PC1", "PC2"),F,"","(Chrome fast test)","Deviation")
PLOT4("MEAN",list( c(85,87,89), c(71,83) ),c("PC1", "PC2"),F,"","(Chrome fast test)","Meaning")

PLOT4("DEV" ,list( c(86,88,90), c(72,84) ),c("PC1", "PC2"),F,"","(Chrome full test)","Deviation")
PLOT4("MEAN",list( c(86,88,90), c(72,84) ),c("PC1", "PC2"),F,"","(Chrome full test)","Meaning")

PLOT4("DEV" ,list( c(91,93,95), c(73) ),c("PC1", "PC2"),F,"","(Firefox fast test)","Deviation")
PLOT4("MEAN",list( c(91,93,95), c(73) ),c("PC1", "PC2"),F,"","(Firefox fast test)","Meaning")

PLOT4("DEV" ,list( c(92,94,96), c(74) ),c("PC1", "PC2"),F,"","(Firefox full test)","Deviation")
PLOT4("MEAN",list( c(92,94,96), c(74) ),c("PC1", "PC2"),F,"","(Firefox full test)","Meaning")

#compare between load and no load
PLOT4("DEV",list( c(85,87,89), c(131,132,133) ,c(128,129,130) ),c("no load", "game load", "flash load"),T,"compare_chrome_LOAD_fast_Dev","","Chrome (compare fast test)")
PLOT4("MEAN",list( c(85,87,89), c(131,132,133) ,c(128,129,130) ),c("no load", "game load", "flash load"),T,"compare_chrome_LOAD_fast_Mean","","Chrome (compare fast test)")


##Safari iMac
# 69 fast test, 70 full test
# 75 fast test, 76 full test
# 77 fast test, 78 full test
# 79 fast test, 81 full test

#2nd Safari atemp
# 103 fast test, 104 full test
# 105 fast test, 106 full test
# 107 fast test, 108 full test

##Chrome iMac                   Windows
# 71 fast test, 72 full test    85 fast test, 86 full test
# 83 fast test, 84 full test    87 fast test, 88 full test
#                               89 fast test, 90 full test
##2nd atemp
# 109 fast test, 110 full test
# 111 fast test, 112 full test
# 113 fast test, 114 full test

##Firefox iMac                  Windows
# 73 fast test, 74 full test    91 fast test, 92 full test ????
#                               93 fast test, 94 full test
#                               95 fast test, 96 full test

##Edge                          Windows
#                               97 fast test, 98 full test
#                               99 fast test, 100 full test
#                               101 fast test, 102 full test


plot(c(100,300,500,1000,2000),c(0.0001,0.0001,0.00009,0.0002,0.0001),type = "n")
data<-c(All_Data[[find_run_id(142)]]["MEAN"][,1]   ,All_Data[[find_run_id(143)]]["MEAN"][,1])
points(c(1000,1000),data,type="o")
data<-c(All_Data[[find_run_id(144)]]["MEAN"][,1]   ,All_Data[[find_run_id(145)]]["MEAN"][,1] )
points(c(2000,2000),data,type="o")
data<-c(All_Data[[find_run_id(146)]]["MEAN"][,1]   ,All_Data[[find_run_id(147)]]["MEAN"][,1] )
points(c(500,500),data,type="o")
data<-c(All_Data[[find_run_id(148)]]["MEAN"][,1]   ,All_Data[[find_run_id(149)]]["MEAN"][,1] )
points(c(100,100),data,type="o")
data<-c(All_Data[[find_run_id(150)]]["MEAN"][,1]   ,All_Data[[find_run_id(151)]]["MEAN"][,1] )
points(c(300,300),data,type="o")

data<-c(All_Data[[find_run_id(152)]]["MEAN"][,1]   ,All_Data[[find_run_id(153)]]["MEAN"][,1])
points(c(1000,1000),data,type="o",col="red")
data<-c(All_Data[[find_run_id(154)]]["MEAN"][,1]   ,All_Data[[find_run_id(155)]]["MEAN"][,1])
points(c(2000,2000),data,type="o",col="red")
data<-c(All_Data[[find_run_id(156)]]["MEAN"][,1]   ,All_Data[[find_run_id(157)]]["MEAN"][,1])
points(c(500,500),data,type="o",col="red")
data<-c(All_Data[[find_run_id(158)]]["MEAN"][,1]   ,All_Data[[find_run_id(159)]]["MEAN"][,1])
points(c(100,100),data,type="o",col="red")
data<-c(All_Data[[find_run_id(162)]]["MEAN"][,1]   ,All_Data[[find_run_id(163)]]["MEAN"][,1])
points(c(300,300),data,type="o",col="red")