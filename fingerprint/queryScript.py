import mysql.connector
import sys
total = len(sys.argv)
cmdargs = str(sys.argv)

def have_least_pos(c,pos,er):
    if c+pos < total :
        return c+pos;
    else :
        print er
        sys.exit(0);
def execute_query_to_file(q, out,col):
    cnx = mysql.connector.connect(user='root', password='123qwe!@#QWE' , database='fingerprint')
    cursor = cnx.cursor()

    cursor.execute(q)
    for row in cursor.fetchall():
        i=0;
        str=""
        while i<col :
            str+=row[i]
            i+=1
            if i!=col:
                str+="\t"
        out.write(str+"\n")
    cursor.close()
    cnx.close()
    return 0;

def execute_query(q,col):
    cnx = mysql.connector.connect(user='root', password='123qwe!@#QWE' , database='fingerprint')
    cursor = cnx.cursor()

    cursor.execute(q)
    for row in cursor.fetchall():
        i=0;
        str=""
        while i<col :
            str+=row[i]
            i+=1
            if i!=col:
                str+="\t"
        print str
    cursor.close()
    cnx.close()
    return 0;

if total > 2 :
    table=""
    keyExpr=""
    column=""
    columnCount=0
    fileName=""
    outColNames=""
    count=1
    while(count < total):
        if str(sys.argv[count])=="-t" :
            count = have_least_pos(count,1,"wrong table parameter size!")
            if str(sys.argv[count])=="loc" :
                table="locality"
                columnCount=8
		column="IP, MEAN, DEV, VAR, MED, DEP, REP, RUN_ID "
            elif str(sys.argv[count])=="nonl" :
                table="nonlocality"
                columnCount=8
		column="IP, MEAN, DEV, VAR, MED, DEP, REP, RUN_ID "
            elif str(sys.argv[count])=="fib" :
                table="fibonacci"
                columnCount=8
		column="IP, MEAN, DEV, VAR, MED, DEP, REP, RUN_ID "
            elif str(sys.argv[count])=="z" :
                table="gpu_z"
                columnCount=11
		column="IP, TOTALF, OBJF, FFT, MIN, MAX, AVE, DATA, START, STEP, RUN_ID "
            elif str(sys.argv[count])=="nonz" :
                table="gpu_nonz"
                columnCount=11
		column="IP, TOTALF, OBJF, FFT, MIN, MAX, AVE, DATA, START, STEP, RUN_ID "
            elif str(sys.argv[count])=="cube" :
                table="gpu_cube"
                columnCount=7
		column="IP, TOTALO, FPS, FO, TIME, STEP, RUN_ID "
            elif str(sys.argv[count])=="hard" :
                table="hardware_info"
                columnCount=8
		column="IP, CPU_CORE, CPU_WORK, CPU_SAMPLE, RUN_ID "
            elif str(sys.argv[count])=="cpu" :
                table="cpu"
		keyExpr = "RUN_ID <> 'NULL';"
                columnCount=7
		column="IP, CPU, GPU, RAM, RAMSPEED, OS, ARCH, ID "
            else:
                print "Table name is wrimport mysql.connectorong!\nType one of the following list (loc,nonl,fib,z,nonz,cube,hard,cpu)"
        elif str(sys.argv[count])=="-k" :
            count = have_least_pos(count,2 ,"wrong key parameter's size!")
            if str(sys.argv[count-1]).lower()=="ip" :
                keyExpr= "IP like '%"+str(sys.argv[count])+"%'"
            elif str(sys.argv[count-1]).lower()=="id" :
                keyExpr= "RUN_ID = "+str(sys.argv[count])+"'"
            elif str(sys.argv[count-1]).lower()=="time" :
                keyExpr= "TIME_STMP like '%"+str(sys.argv[count])+"%'"
            else:
                print "Key name is wrong!\nYou can use :ip , id, time!"
        elif str(sys.argv[count])=="-c" :
            count = have_least_pos(count,1 ,"wrong column parameter size!\n(ex. -c 2 IP TIME_STMP)")
            size=int(sys.argv[count]);
            columnCount=size
	    column=""
            count = have_least_pos(count,size,"wrong column's name size!\n(ex. -c 2 IP TIME_STMP)")
            while(size > 0):
                column+=str(sys.argv[count - (size-1)])
		outColNames+=str(sys.argv[count - (size -1)])+"\t"
                size-=1
                if size !=0:
                    column+=" ,"
	    outColNames+="\n"
        elif str(sys.argv[count]=="-o") :
            count = have_least_pos(count,1 ,"wrong output file parameter!\n(ex. -o out.txt)")
            fileName = str(sys.argv[count])
        count+=1
    if column == "":
        column= "*"
    
    if keyExpr == "":
	query = "select "+column+" from "+table+";"
    else :	
    	query = "select "+column+" from "+table+ " where "+ keyExpr
    
    if fileName == "" :
	print outColNames
        execute_query(query,columnCount)
    else:
	print "Columnt count :",columnCount
        fileOut = open(fileName,"w")
	fileOut.write(column.replace(", ","\t"))
	fileOut.write("\n")
        execute_query_to_file(query,fileOut,columnCount)

    print query
elif total == 2:
    if str(sys.argv[1] == "--help") :
        print "ICS FORTH script. Just for research use."
        print "This script use parameters:\n-t\t :Table name. List of tables"
        print "\t\t (loc, nonl, fib, z, nonz, cube, cpu, hard)"
        print "\t Example \" -t nonl\"\n"
        print "-k\t :Key name and value. List of key : \n\t\t ip , id (its RUN_ID) ,time (its TIME_STMP)"
        print "\t Example \"-k ip 127.0.0.1\"\n"
        print "-c\t :Column name to show.\n\t  List its depents of table and it's case sensitive.\n\t  First argument nee to be a number of column's."
        print "\t Note: if dont use \"-c\" all column's gonna be selected!"
        print "\t Example \"-c 2 IP RUN_ID\"\n"
        print "-o\t :Output file name. Declarate outout file name."
        print "\t  Without \"-o\" parameter output go to the console."
        print "\t  Example \"-o out.txt\"\n"
        print "--help\t To show this messange.\n"
        print "Tables column name's:"
        print "loc:\n\t TIME_STMP, IP, AGENT, MEAN, DEV, VAR, MED,\n\t DEP, REP, RUN_ID"
        print "nonl:\n\t TIME_STMP, IP, AGENT, MEAN, DEV, VAR, MED,\n\t DEP, REP, RUN_ID"
        print "fib:\n\t TIME_STMP, IP, AGENT, MEAN, DEV, VAR, MED,\n\t DEP, REP, RUN_ID"
        print "z:\n\t TIME_STMP, IP, AGENT, TOTALF, OBJF, FFT, MIN,\n\t MAX, AVE, DATA, START, STEP, RUN_ID"
        print "nonz:\n\t TIME_STMP, IP, AGENT, TOTALF, OBJF, FFT, MIN,\n\t MAX, AVE, DATA, START, STEP, RUN_ID"
        print "cube:\n\t TIME_STMP, IP, AGENT, TOTALO, FPS, FO, TIME,\n\t STEP, RUN_ID"
        print "cpu:\n\t TIME_STMP, IP, AGENT, CPU_CORE, CPU_SAMPLE,\n\t CPU_WORK, RUN_ID"
        print "hard:\n\t ID, CPU, GPU, RAM, RAMSPEED, OS, ARCH, IP"
