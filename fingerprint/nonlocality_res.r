rm(list=ls())
setwd("/home/parker/FORTH/fingerprint/fingerprint")
#
##---Read all data from file
A<-read.table("./DATA/nonloc.txt",header = T)
A<-read.table("./DATA/test_nonl.txt",header = T)
#
##---Take all unique id's
UN_ID <- unique(A["RUN_ID"])
#
##---Put all data to a list
All_Data<-list();
for(data_count in 1:dim(UN_ID)[1]){
  Data_1 <- A[A["RUN_ID"] == UN_ID[data_count,],]
  i<-0
  start<-1
  end<-dim(Data_1)[1]
  for(ind in start:end){
    if(Data_1[ind,7]==3000 && Data_1[ind,6]==3000 && i == 0)
      i<-ind
  }
  print(i)
  if(i == 0)
    i<-end
  if(data_count == 7){
    All_Data[[data_count]] <- Data_1[];
  }else{
    All_Data[[data_count]] <- Data_1[1:i,];
  }
  
}

DataColor<-c("orange2", "brown", "gray", "red", "black", "blue", "green","yellow", "darkorchid", "deeppink")


##########################################################
####---Help function that find position of RUN_ID (id)--##
##########################################################
find_run_id<-function(id){
  for(i in 1:length(All_Data)){
    if(All_Data[[i]]["RUN_ID"][1,] == id){
      return(i)
    }
  }
}



#mobile
black=c(477,478,479,480,481,482,483,484,485,486,487,488,489,490,491,492,493,494,495,496,497,498)
blue=c(456,457,458,459,460,461,454,455,462,463,464,465,466,467,468,469,470,471,473,474,475,476)
red=c()
time=c(100,100,250,250,500,500,750,750,1000,1000,1250,1250,1500,1500,1750,1750,2000,2000,2500,2500,3000,3000)
test(black,blue,red,time,"Locality ( iOS Safari)")

black=c(524,525,526,527,528,529,530,531,532,533,534,535,536,537,538,539,540,541,542,543,544,545)
blue= c(500,501,502,503,504,505,506,507,509,510,511,512,513,514,515,516,517,518,520,521,522,523)
red=c()
time=c(100,100,250,250,500,500,750,750,1000,1000,1250,1250,1500,1500,1750,1750,2000,2000,2500,2500,3000,3000)
test(black,blue,red,time,"Locality ( iOS Chrome)")

black=c(590,591,592,593,594,595,596,597,599,600,601,602,603,604,605,606,607,608,609,610,611,612)
blue= c(546,547,548,549,550,551,552,553,554,555,556,557,558,559,560,561,562,563,564,565,566,567)
red=c()
time=c(100,100,250,250,500,500,750,750,1000,1000,1250,1250,1500,1500,1750,1750,2000,2000,2500,2500,3000,3000)
test(black,blue,red,time,"Locality ( Android Standart)")

black=c(613,614,615,616,617,618,619,620,621,622,623,624,625,626,627,628,629,630,631,632,633,634)
blue= c(568,569,570,571,572,573,574,575,576,577,578,579,580,581,582,583,584,585,586,587,588,589)
red=c()
time=c(100,100,250,250,500,500,750,750,1000,1000,1250,1250,1500,1500,1750,1750,2000,2000,2500,2500,3000,3000)
test(black,blue,red,time,"Locality ( Android Chrome)")
DataColor<-c("orange2", "brown", "gray", "red", "black", "blue", "green","yellow", "darkorchid", "deeppink")
#DataColor<-c("orange2", "gray","brown", "blue", "yellow", "black", "green", "red", "darkorchid", "deeppink")

color_set <-function(data_set){
  ip <- c()
  for(i in data_set){
    ip <- c(ip, as.vector(All_Data[[i]]["IP"][1,1]) )
  }
  u_ip <- unique(ip)
  color_data <- c()
  for(i in ip){
    color_data <- c(color_data , DataColor[which(u_ip == i)])
  }
  return (color_data)
}


PLOT2 <- function(varType,to_file){
  color_c <- 1;
  for(l in 1:length(All_Data)){
    ##---Plot mean data  
    ##---Calculate min and max for first plot
    yLabel <- paste(varType,"results"," ")
    
    ##---Calculate min and max for second plot
    DataMin <- min(All_Data[[l]][varType][,1])
    DataMax <- max(All_Data[[l]][varType][,1])
    if(!to_file){
      dev.new()
    }else{
      name <- gsub(" ","",paste( paste("nonlocality_res/single/",tolower(varType)) , substr(as.matrix(All_Data[[l]]["IP"])[1,],8,13), ".jpg"))
      jpeg(filename = name,
           width = 800 , height = 600)
    }
    Plot2 <- seq(DataMin, DataMax, by=((DataMax-DataMin) / length(All_Data[[l]][varType][,1])));
    
    plot(All_Data[[l]]["DEP"][, 1]*All_Data[[l]]["REP"][, 1],
         All_Data[[l]][varType][, 1],
         pch="o",
         col=DataColor[color_c], 
         xlab="Deep and repeat relation",
         ylab=yLabel, 
         main=paste("Nonlocality", yLabel, " ")
    );
    legendIP<-c()
    legendIP[1]<- substr(as.matrix(All_Data[[l]]["IP"])[1,],8,13)
    xData <- All_Data[[l]]["DEP"][,1]*All_Data[[l]]["REP"][,1]
    for(k in 1:length(xData)){
      errorMax <- All_Data[[l]][varType][,1][k] + All_Data[[l]]["VAR"][,1][k]
      errorMin <- All_Data[[l]][varType][,1][k] - All_Data[[l]]["VAR"][,1][k]
      points(c(xData[k], xData[k]), c(errorMax, errorMin), type="l")
    }
    max_v <- max(All_Data[[l]]["VAR"][,1])
    legendIP[2]<-paste("Max variance ('+' or '-'): ",max_v, " ")
    print(DataColor[color_c])
    print(All_Data[[l]]["IP"][1,1])
    
    legend(max(All_Data[[l]]["DEP"][,1]*All_Data[[l]]["REP"][,1])*0.10,DataMax, 
           legendIP,DataColor[c(0,color_c)])
    color_c<-color_c+1;
    if(color_c > length(DataColor)) color_c <-1
    if(to_file) dev.off()
  }
}
########################################################################################
###----------------------------------------------------------------------------------###
########################################################################################
PLOT3<-function(varType, data_num, to_file, info_var, myname){
  options("scipen"=100, "digits"=8)
  ######------------------------Mean Plot------------------------------------------####
  ##---Plot pair data of same ip 
  ##---Calculate min and max for first plot
  yLabel <- paste(varType,"results"," ")
  DataList<-c()
  for(i in 1:19){
    DataList <- c(DataList, All_Data[[ i ]][varType][,1])
  }
  
  DataMin <- min(DataList)
  DataMax <- max(DataList)
  color_data <- color_set(data_num)
  ##
  if(!to_file){
    dev.new()
  }else{
    if(myname == ""){
    name <- gsub(" ","",paste( paste("nonlocality_res/group/",tolower(varType)) , substr(as.matrix(All_Data[[i]]["IP"])[1,],8,13), ".jpg"))
    }else{
      name <- gsub(" ","",paste("nonlocality_res/group/",myname))
    }
    jpeg(filename = name,
         width = 800 , height = 600)
  }
  Plot1<-seq(DataMin,DataMax,by=((DataMax-DataMin)/80));
  ## -- ploting
  legendIP <- c();
  plot(All_Data[[1]]["DEP"][,1]*All_Data[[1]]["REP"][,1], Plot1,type="n",xlab="Deep and repeat relation",ylab=yLabel,main=paste("Nonlocality",yLabel," "));
  
  for(i in data_num){
    legendIP[which(data_num == i)]<-substr(as.matrix(All_Data[[i]]["IP"])[1,],8,13)
    points(All_Data[[i]]["DEP"][,1]*All_Data[[i]]["REP"][,1] , All_Data[[i]][varType][,1],pch="o",col=color_data[which(data_num == i)]);
    xData <- All_Data[[i]]["DEP"][,1]*All_Data[[i]]["REP"][,1]
    for(k in 1:length(xData)){
      errorMax <- All_Data[[i]][varType][,1][k] + All_Data[[i]]["VAR"][,1][k]
      errorMin <- All_Data[[i]][varType][,1][k] - All_Data[[i]]["VAR"][,1][k]
      
      points(c(xData[k], xData[k]), c(errorMax, errorMin), type="l")
    }
    max_v <- max(All_Data[[i]]["VAR"][,1])
    if(info_var) legendIP[which(data_num == i) + length(data_num)] <- paste("Max variance ('+' or '-'): ",max_v, " ")
    print(color_data[i])
    print(All_Data[[i]]["IP"][1,1])
  }
  
  legend(max(All_Data[[1]]["DEP"][,1]*All_Data[[1]]["REP"][,1])*0.1,DataMax, 
         legendIP,color_data)
  if(to_file) dev.off()
}
####----------####----------####----------####----------####----------####----------####----------####----------
#--------------------------------------------Plot all data in groups of fast and small test-------------------------------------------#
####----------####----------####----------####----------####----------####----------####----------####----------
PLOT4<-function(varType, data_num, label, to_file, myname, my_header,my_label){
  options("scipen"=100, "digits"=8)
  ######------------------------Mean Plot------------------------------------------####
  ##---Plot pair data of same ip 
  ##---Calculate min and max for first plot
  yLabel <-""
  DataList<-c()
  v<-find_run_id(69)
  end<-find_run_id(102)
  #for(i in v:dim(UN_ID)[1]){
  for(i in v:end){
    DataList <- c(DataList, All_Data[[ i ]][varType][,1])
  }
  
  DataMin <- min(DataList)
  DataMax <- max(DataList)
  #color_data <- color_set(data_num)
  ##
  if(varType == "DEV"){
    yLabel<-"Deviation"
  }else{
    yLabel<-"Meaning values"
  }
  
  if(!to_file){
    dev.new()
  }else{
    if(myname == ""){
      #name <- gsub(" ","",paste( paste("nonlocality_res/group/",tolower(varType)) , substr(as.matrix(All_Data[[i]]["IP"])[1,],8,13), ".jpg"))
    }else{
      name <- gsub(" ","",paste("plots/nonlocality_res/group/",myname))
    }
    jpeg(filename = name,
         width = 800 , height = 600, quality = 100)
  }
  Plot1<-seq(DataMin,DataMax,by=((DataMax-DataMin)/80));
  ## -- ploting
  par(mar=c(5,5,1,1))
  par(family="Times New Roman")
  legendIP <- c();
  #plot(All_Data[[1]]["DEP"][,1]*All_Data[[1]]["REP"][,1], Plot1,type="n",xlab="Deep and repeat relation",ylab=yLabel,main=paste("",paste(my_label,my_header," ")," "));
  plot(All_Data[[1]]["DEP"][,1]*All_Data[[1]]["REP"][,1], Plot1,type="n",xlab="Deep and repeat relation",ylab=yLabel,cex.lab = 2.3,cex.axis=1.8);
  color_num<-1;
  for(p in data_num){
    for(i in p){
      g<-find_run_id(i)
      points(All_Data[[g]]["DEP"][,1]*All_Data[[g]]["REP"][,1] , All_Data[[g]][varType][,1],pch="o",col=DataColor[color_num],lwd = 10);
      xData <- All_Data[[g]]["DEP"][,1]*All_Data[[g]]["REP"][,1]
      for(k in 1:length(xData)){
        errorMax <- All_Data[[g]][varType][,1][k] + All_Data[[g]]["VAR"][,1][k]
        errorMin <- All_Data[[g]][varType][,1][k] - All_Data[[g]]["VAR"][,1][k]
        
        points(c(xData[k], xData[k]), c(errorMax, errorMin), type="l")
      }
      max_v <- max(All_Data[[g]]["VAR"][,1])
    }
    color_num<-color_num+1
  }
  title_name<-NULL
  if(length(label) > length(data_num)){
    title_name<-label[1]
    label<- label[2:length(label)]
  }
  legend(max(All_Data[[1]]["DEP"][,1]*All_Data[[1]]["REP"][,1])*0.1,DataMax, 
         label, DataColor[1:color_num],title=title_name,cex=2.2)
  if(to_file) dev.off()
}
##------------------------------------------END-------------------------------------------------##
####################################
#############TEST###################
####################################

  varType<-"DEV"
  data_num<-list( c(110,112,114), c(104,106,108) )
  label<-c("PC2","Chrome", "Safari")
  to_file<-F
  myname<-"iMac_all_browsers_FULL_Dev"
  my_header<-""
  my_label<-""
  options("scipen"=100, "digits"=8)
  ######------------------------Mean Plot------------------------------------------####
  ##---Plot pair data of same ip 
  ##---Calculate min and max for first plot
  yLabel <-""
  DataList<-c()
  v<-find_run_id(69)
  for(i in v:dim(UN_ID)[1]){
    DataList <- c(DataList, All_Data[[ i ]][varType][,1])
  }
  
  DataMin <- min(DataList)
  DataMax <- max(DataList)
  #color_data <- color_set(data_num)
  ##
  if(varType == "DEV"){
    yLabel<-"Deviation"
  }else{
    yLabel<-"Meaning"
  }
  
  if(!to_file){
    dev.new()
  }else{
    if(myname == ""){
      #name <- gsub(" ","",paste( paste("nonlocality_res/group/",tolower(varType)) , substr(as.matrix(All_Data[[i]]["IP"])[1,],8,13), ".jpg"))
    }else{
      name <- gsub(" ","",paste("plots/nonlocality_res/group/",myname))
    }
    jpeg(filename = name,
         width = 800 , height = 600)
  }
  Plot1<-seq(DataMin,DataMax,by=((DataMax-DataMin)/80));
  ## -- ploting
  legendIP <- c();
  plot(All_Data[[85]]["DEP"][,1]*All_Data[[85]]["REP"][,1], Plot1,type="n",xlab="Deep and repeat relation",ylab=yLabel,main=paste("",paste(my_label,my_header," ")," "));
  color_num<-1;
  for(p in data_num){
    for(i in p){
      g<-find_run_id(i)
      points(All_Data[[g]]["DEP"][,1]*All_Data[[g]]["REP"][,1] , All_Data[[g]][varType][,1],pch="o",col=DataColor[color_num]);
      xData <- All_Data[[g]]["DEP"][,1]*All_Data[[g]]["REP"][,1]
      for(k in 1:length(xData)){
        errorMax <- All_Data[[g]][varType][,1][k] + All_Data[[g]]["VAR"][,1][k]
        errorMin <- All_Data[[g]][varType][,1][k] - All_Data[[g]]["VAR"][,1][k]
        
        points(c(xData[k], xData[k]), c(errorMax, errorMin), type="l")
      }
      max_v <- max(All_Data[[g]]["VAR"][,1])
    }
    color_num<-color_num+1
  }
  title_name<-NULL
  if(length(label) > length(data_num)){
    title_name<-label[1]
    label<- label[2:length(label)]
  }
  legend(max(All_Data[[1]]["DEP"][,1]*All_Data[[1]]["REP"][,1])*0.1,DataMax, 
         label, DataColor[1:color_num],title=title_name)
  if(to_file) dev.off()


####################################
############TEST####################
####################################


PLOT2("MEAN",T)
PLOT2("DEV",T)
PLOT3("MEAN",c(6,7,19),F)

PLOT3("MEAN",c(6,7,19), T)
PLOT3("DEV",c(6,7,19), T)
PLOT3("MEAN",c(15,16), T)
PLOT3("DEV",c(15,16), T)
PLOT3("MEAN",c(1,17,18), T)
PLOT3("DEV",c(1,17,18), T)

PLOT3("MEAN", c(6,7,19,15,16,14,1,17,18),F,F,"mean_group1")
PLOT3("MEAN", c(2,3,4,5,8,9,10,11,12,13),F,F,"mean_group2")

PLOT3("DEV", c(6,7,19,15,16,14,1,17,18),F,F,"dev_group1")
PLOT3("DEV", c(2,3,4,5,8,9,10,11,12,13),F,F,"dev_group2")

PLOT("MEAN")
PLOT("DEV")


####------------------------
###-PC2
PLOT4("DEV" ,list( c(69,75,77,79), c(70,76,78,81) ),c("Fast", "Full"),F,"","Safari (PC2)","Deviation")
PLOT4("MEAN",list( c(69,75,77,79), c(70,76,78,81) ),c("Fast", "Full"),F,"","Safari (PC2)","Meaning")
##2nd atemp
PLOT4("DEV" ,list( c(103,105,107), c(104,106,108) ),c("Fast", "Full"),F,"","Safari (PC2)","Deviation")
PLOT4("MEAN",list( c(103,105,107), c(104,106,108) ),c("Fast", "Full"),F,"","Safari (PC2)","Meaning")

PLOT4("DEV" ,list( c(71,83), c(72,84) ),c("Fast", "Full"),F,"","Chrome(PC2)","Deviation")
PLOT4("MEAN",list( c(71,83), c(72,84) ),c("Fast", "Full"),F,"","Chrome(PC2)","Meaning")
##2nd atemp
PLOT4("DEV" ,list( c(109,111,113), c(110,112,114) ),c("Fast", "Full"),F,"","Chrome(PC2)","Deviation")
PLOT4("MEAN",list( c(109,111,113), c(110,112,114) ),c("Fast", "Full"),F,"","Chrome(PC2)","Meaning")

PLOT4("DEV" ,list( c(73), c(74) ),c("Fast", "Full"),F,"","Firefox(PC2)","Deviation")
PLOT4("MEAN",list( c(73), c(74) ),c("Fast", "Full"),F,"","Firefox(PC2)","Meaning")

PLOT4("DEV",list( c(110,112,114), c(104,106,108) ),c("PC2","Chrome", "Safari"),F,"iMac_all_browsers_FULL_Dev","","")
PLOT4("MEAN",list( c(110,112,114), c(104,106,108) ),c("PC2","Chrome", "Safari"),F,"iMac_all_browsers_FULL_Mean","","")
PLOT4("DEV",list( c(109,111,113), c(110,112,114) ),c("PC2","Chrome", "Safari"),F,"iMac_all_browsers_FAST_Dev","","")
PLOT4("MEAN",list( c(109,111,113), c(110,112,114) ),c("PC2","Chrome", "Safari"),F,"iMac_all_browsers_FAST_Mean","","")

PLOT4("DEV",list( c(109,111,113), c(110,112,114), c(103,105,107), c(104,106,108) ),c("PC2","Chrome (fast)","Chrome (full)", "Safari (fast)", "Safari (full)"),F,"iMac_all_browsers_ALL_Dev","","")
PLOT4("MEAN",list( c(109,111,113), c(110,112,114), c(103,105,107), c(104,106,108) ),c("PC2","Chrome (fast)","Chrome (full)", "Safari (fast)", "Safari (full)"),F,"iMac_all_browsers_ALL_Mean","","")



####------------------------
###-PC1
PLOT4("DEV" ,list( c(85,87,89), c(86,88,90) ),c("Fast", "Full"),F,"","Chrome(PC1)","Deviation")
PLOT4("MEAN",list( c(85,87,89), c(86,88,90) ),c("Fast", "Full"),F,"","Chrome(PC1)","Meaning")
PLOT4("DEV" ,list( c(91,93,95), c(92,94,96) ),c("Fast", "Full"),F,"","Firefox(PC1)","Deviation")
PLOT4("MEAN",list( c(91,93,95), c(92,94,96) ),c("Fast", "Full"),F,"","Firefox(PC1)","Meaning")
##2nd atemp
PLOT4("DEV" ,list( c(115,117,119), c(116,118,120) ),c("Fast", "Full"),F,"","Firefox(PC1)","Deviation")
PLOT4("MEAN",list( c(115,117,119), c(116,118,120) ),c("Fast", "Full"),F,"","Firefox(PC1)","Meaning")

PLOT4("DEV" ,list( c(97,99,101), c(98,100,102) ),c("Fast", "Full"),F,"","Edge(PC1)","Deviation")
PLOT4("MEAN",list( c(97,99,101), c(98,100,102) ),c("Fast", "Full"),F,"","Edge(PC1)","Meaning")

PLOT4("DEV",list( c(85,87,89),  c(115,117,119), c(97,99,101) ),c("PC1","Chrome","Firefox","Edge" ),F,"windows_all_browsers_FAST_Dev","","")
PLOT4("MEAN",list( c(85,87,89),  c(115,117,119), c(97,99,101) ),c("PC1","Chrome","Firefox","Edge" ),F,"windows_all_browsers_FAST_Mean","","")
PLOT4("DEV",list( c(86,88,90),  c(116,118,120), c(98,100,102) ),c("PC1","Chrome","Firefox","Edge" ),T,"windows_all_browsers_FULL_Dev.jpg","","")
PLOT4("MEAN",list( c(86,88,90),  c(116,118,120), c(98,100,102) ),c("PC1","Chrome","Firefox","Edge" ),F,"windows_all_browsers_FULL_Mean","","")

PLOT4("DEV",list( c(85,87,89), c(115,117,119), c(97,99,101), c(86,88,90),  c(116,118,120), c(98,100,102) ),c("PC1","Chrome (fast)", "Firefox (fast)","Edge (fast)","Chrome (full)", "Firefox (full)","Edge (full)"),F,"windows_all_browsers_ALL_Dev","","")
PLOT4("MEAN",list( c(85,87,89), c(115,117,119), c(97,99,101), c(86,88,90),  c(116,118,120), c(98,100,102) ),c("PC1","Chrome (fast)", "Firefox (fast)","Edge (fast)","Chrome (full)", "Firefox (full)","Edge (full)"),F,"windows_all_browsers_ALL_Mean","","")



####------------------------
###-Compare betwean PC1 and PC1
PLOT4("DEV",list( c(85,87,89), c(109,111,113) ),c("PC1", "PC2"),F,"compare_chrome_FAST_Dev","","Chrome (compare fast test)")
PLOT4("MEAN",list( c(85,87,89), c(109,111,113) ),c("PC1", "PC2"),F,"compare_chrome_FAST_Mean","","Chrome (compare fast test)")

PLOT4("DEV",list( c(86,88,90), c(110,112,114) ),c("PC1", "PC2"),F,"compare_chrome_FULL_Dev","","Chrome (compare full test)")
PLOT4("MEAN",list( c(86,88,90), c(110,112,114) ),c("PC1", "PC2"),F,"compare_chrome_FULL_Mean","","Chrome (compare full test)")

PLOT4("DEV" ,list( c(85,87,89), c(71,83) ),c("PC1", "PC2"),F,"","(Chrome fast test)","Deviation")
PLOT4("MEAN",list( c(85,87,89), c(71,83) ),c("PC1", "PC2"),F,"","(Chrome fast test)","Meaning")

PLOT4("DEV" ,list( c(86,88,90), c(72,84) ),c("PC1", "PC2"),F,"","(Chrome full test)","Deviation")
PLOT4("MEAN",list( c(86,88,90), c(72,84) ),c("PC1", "PC2"),F,"","(Chrome full test)","Meaning")

PLOT4("DEV" ,list( c(91,93,95), c(73) ),c("PC1", "PC2"),F,"","(Firefox fast test)","Deviation")
PLOT4("MEAN",list( c(91,93,95), c(73) ),c("PC1", "PC2"),F,"","(Firefox fast test)","Meaning")

PLOT4("DEV" ,list( c(92,94,96), c(74) ),c("PC1", "PC2"),F,"","(Firefox full test)","Deviation")
PLOT4("MEAN",list( c(92,94,96), c(74) ),c("PC1", "PC2"),F,"","(Firefox full test)","Meaning")

#compare between load and no load
PLOT4("DEV",list( c(85,87,89), c(131,132,133) ,c(128,129,130) ),c("no load", "game load", "flash load"),T,"compare_chrome_LOAD_fast_Dev","","Chrome (compare fast test)")
PLOT4("MEAN",list( c(85,87,89), c(131,132,133) ,c(128,129,130) ),c("no load", "game load", "flash load"),T,"compare_chrome_LOAD_fast_Mean","","Chrome (compare fast test)")

##Safari iMac
# 69 fast test, 70 full test
# 75 fast test, 76 full test
# 77 fast test, 78 full test
# 79 fast test, 81 full test

#2nd Safari atemp
# 103 fast test, 104 full test
# 105 fast test, 106 full test
# 107 fast test, 108 full test

##Chrome iMac                   Windows
# 71 fast test, 72 full test    85 fast test, 86 full test
# 83 fast test, 84 full test    87 fast test, 88 full test
#                               89 fast test, 90 full test
##2nd atemp
# 109 fast test, 110 full test
# 111 fast test, 112 full test
# 113 fast test, 114 full test

##Firefox iMac                  Windows
# 73 fast test, 74 full test    91 fast test, 92 full test ????
#                               93 fast test, 94 full test
#                               95 fast test, 96 full test

##Edge                          Windows
#                               97 fast test, 98 full test
#                               99 fast test, 100 full test
#                               101 fast test, 102 full test

test<-function(black,blue,red,time,main_t,to_file,myname){
  if(!to_file){
    dev.new()
  }else{
    if(myname == ""){
      #name <- gsub(" ","",paste( paste("locality_res/group/",tolower(varType)) , substr(as.matrix(All_Data[[i]]["IP"])[1,],8,13), ".jpg"))
    }else{
      name <- gsub(" ","",paste("plots/mobile/",myname))
    }
    jpeg(filename = name,
         width = 800 , height = 600, quality = 100)
  }
  par(mar=c(5,5,1,1))
  par(family="Times New Roman")
  plot(c(100,250,300,500,750,1000,1250,1500,2000,3000),c(0.0100,0.00010,0.00030,0.00025,0.00010,0.0002,0.00010,0.00010,0.0002,0.00010),type = "n",xlab="Run time (milliseconds)",ylab="Meaning values",cex.lab = 2.3,cex.axis=1.8)
  for(i in seq(1,length(black),2) ){
    data<-c(All_Data[[find_run_id(black[i])]]["MEAN"][,1]   ,All_Data[[find_run_id(black[i+1])]]["MEAN"][,1] )
    points(c(time[i],time[i+1]),data,type="o")
  }
  for(i in seq(1,length(blue),2) ){
    data<-c(All_Data[[find_run_id(blue[i])]]["MEAN"][,1]   ,All_Data[[find_run_id(blue[i+1])]]["MEAN"][,1] )
    points(c(time[i],time[i+1]),data,type="o",col="blue")
  }
  if(length(red) > 1){
    for(i in seq(1,length(red),2) ){
      data<-c(All_Data[[find_run_id(red[i])]]["MEAN"][,1]   ,All_Data[[find_run_id(red[i+1])]]["MEAN"][,1] )
      points(c(time[i],time[i+1]),data,type="o",col="red")
    }
    legend(100,0.00008,c("No load","Flash","Game"), c("black","blue","red"),cex=2.2)
  }else{
    legend(100,0.010,c("No load","Background"), c("black","blue"),cex=2.2)  
  }
  if(to_file) dev.off()
}


black=c(164,165,290,291,166,167,292,293,168,169,294,295,170,171,172,173,174,175,176,177,178,179)
blue= c(180,181,278,279,182,183,280,281,184,185,282,283,186,187,188,189,190,191,192,193,194,195)
red=  c(196,197,266,267,198,199,268,269,200,201,270,271,202,203,204,205,206,207,208,209,210,211)
time= c(100,100,250,250,500,500,750,750,1000,1000,1250,1250,1500,1500,1750,1750,2000,2000,2500,2500,3000,3000)
test(black,blue,red,time,"Non-Locality (Crhome windows)",F,"nonloc_chrome_windows.jpg")

black=c(212,213,284,285,214,215,286,287,216,217,288,289,218,219,220,221,222,223,224,225,226,227)
blue= c(228,229,272,273,230,231,274,275,232,233,276,277,234,235,236,237,238,239,240,241,242,243)
red=  c(244,245,260,261,246,247,262,263,248,249,264,265,250,251,252,253,254,255,256,257,258,259)
time= c(100,100,250,250,500,500,750,750,1000,1000,1250,1250,1500,1500,1750,1750,2000,2000,2500,2500,3000,3000)
test(black,blue,red,time,"Non-Locality (Firefox windows)",F,"nonloc_firefox_windows.jpg")

black=c(296,297,298,299,300,301,302,303,304,305,306,307,308,309,310,311,312,313,314,315,316,317)
blue= c(318,319,320,321,322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339)
red=  c()
time= c(100,100,250,250,500,500,750,750,1000,1000,1250,1250,1500,1500,1750,1750,2000,2000,2500,2500,3000,3000)
test(black,blue,red,time,"Non-Locality (Chrome iMac)",F,"nonloc_chrome_imac.jpg")

black=c(340,341,342,343,344,345,348,349,346,347,350,351,352,353,354,355,356,357,358,359,360,361)
blue= c(362,363,364,365,366,367,368,369,370,371,372,373,374,375,376,377,378,379,380,381,382,383)
red=  c()
time= c(100,100,250,250,500,500,750,750,1000,1000,1250,1250,1500,1500,1750,1750,2000,2000,2500,2500,3000,3000)
test(black,blue,red,time,"Non-Locality (Firefox iMac)",F,"nonloc_firefox_imac.jpg")


black=c(384,385,386,387,388,389,390,391,392,393,394,395,396,397,398,399,400,401,402,403,404,405)
blue= c(406,407,408,409,410,411,412,413,414,415,416,417,418,419,420,421,422,423,424,425,426,427)
red=  c()
time= c(100,100,250,250,500,500,750,750,1000,1000,1250,1250,1500,1500,1750,1750,2000,2000,2500,2500,3000,3000)
test(black,blue,red,time,"Non-Locality (Safari iMac)",F,"nonloc_safari_imac.jpg")

#mobile
black=c(477,478,479,480,481,482,483,484,485,486,487,488,489,490,491,492,493,494,495,496,497,498)
blue=c(456,457,458,459,460,461,454,455,462,463,464,465,466,467,468,469,470,471,473,474,475,476)
red=c()
time=c(100,100,250,250,500,500,750,750,1000,1000,1250,1250,1500,1500,1750,1750,2000,2000,2500,2500,3000,3000)
test(black,blue,red,time,"Non-locality ( iOS Safari)",F,"nonloc_safari_ios.jpg")

black=c(524,525,526,527,528,529,530,531,532,533,534,535,536,537,538,539,540,541,542,543,544,545)
blue= c(500,501,502,503,504,505,506,507,509,510,511,512,513,514,515,516,517,518,520,521,522,523)
red=c()
time=c(100,100,250,250,500,500,750,750,1000,1000,1250,1250,1500,1500,1750,1750,2000,2000,2500,2500,3000,3000)
test(black,blue,red,time,"Non-locality ( iOS Chrome)",F,"nonloc_chrome_ios.jpg")

black=c(590,591,592,593,594,595,596,597,599,600,601,602,603,604,605,606,607,608,609,610,611,612)
blue= c(546,547,548,549,550,551,552,553,554,555,556,557,558,559,560,561,562,563,564,565,566,567)
red=c()
time=c(100,100,250,250,500,500,750,750,1000,1000,1250,1250,1500,1500,1750,1750,2000,2000,2500,2500,3000,3000)
test(black,blue,red,time,"Non-lcality ( Android Standart)",F,"nonloc_standart_android.jpg")

black=c(613,614,615,616,617,618,619,620,621,622,623,624,625,626,627,628,629,630,631,632,633,634)
blue= c(568,569,570,571,572,573,574,575,576,577,578,579,580,581,582,583,584,585,586,587,588,589)
red=c()
time=c(100,100,250,250,500,500,750,750,1000,1000,1250,1250,1500,1500,1750,1750,2000,2000,2500,2500,3000,3000)
test(black,blue,red,time,"Non-locality ( Android Chrome)",T,"nonloc_chrome_andoid.jpg")