/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Alexander
 */
public class Page extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
 //       response.setContentType("text/html;charset=UTF-8");
//        try(PrintWriter out = response.getWriter()){
//            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet Servlet</title>");
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>Servlet NewServlet at " + request.getContextPath() + "</h1>");
//            out.println("</body>");
//            out.println("</html>");
//        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // String userAgent = request.getHeader("User-Agent");
        //
        // System.out.println("User Agent: "+userAgent+ " end\n");
        // String ip = request.getRemoteAddr();
        //
        // String cpu = request.getParameter("cpu");
        // String gpu = request.getParameter("gpu");
        // String ram = request.getParameter("ram");
        // String ramSpeed = request.getParameter("ramspeed");
        // String os = request.getParameter("os");
        // String arch = request.getParameter("arch");
        //
        // String JDBC_DRIVER="com.mysql.jdbc.Driver";
        // String DB_URL="jdbc:mysql://localhost/fingerprint";
        //
        // //  Database credentials
        // String USER = "root";
        // String PASS = "123qwe!@#QWE";
        //
        // try {
        //     Connection con;
        //     //con = DriverManager.getConnection(con_address, "root", "toor");
        //     Class.forName("com.mysql.jdbc.Driver");
        //
        //     // Open a connection
        //     con = DriverManager.getConnection(DB_URL,USER,PASS);
        //     Statement stmt = con.createStatement();
        //     if(cpu!=null && gpu!=null && ram!=null && os!=null && arch!=null){
        //         if(os.contains("Windows") || os.contains("windows") || os.contains("OSX") || os.contains("osx")){
        //             ram = RamSummary(ram);
        //         }
        //         if(arch.contains("64")){
        //             arch="64-bit";
        //         }else if(arch.contains("32")){
        //             arch="32-bit";
        //         }
        //         if(ramSpeed.length()>0 && ramSpeed.indexOf(" ") != -1){
        //             ramSpeed = ramSpeed.substring(0,ramSpeed.indexOf(" "));
        //         }
        //         if(ramSpeed.length()>0 && !ramSpeed.contains("MHz")){
        //             ramSpeed+="MHz";
        //         }
        //         String query ="INSERT INTO "
        //                     +"hardware_info" +
        //                     " (CPU, GPU, RAM, RAMSPEED, OS, ARCH, IP) "
        //                     + " VALUES ("
        //                     + "'" + cpu + "',"
        //                     + "'"+gpu+"',"
        //                     +"'"+ram+"',"
        //                     +"'"+ramSpeed+"',"
        //                     +"'"+os+"',"
        //                     +"'"+arch+"',"
        //                     +"'"+ip+"')";
        //
        //         stmt.executeUpdate(query);
        //     }
        // } catch (SQLException | ClassNotFoundException ex) {
        //     Logger.getLogger(Servlet.class.getName()).log(Level.SEVERE, null, ex);
        // }
        // response.setContentType("text/html;charset=UTF-8");
        // try(PrintWriter out = response.getWriter()){
        //     /* TODO output your page here. You may use following sample code. */
        //
        //     out.println("Phase 2 Done Thanks you!");
        //
        // }
        // processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String cpu_sample = request.getParameter("cpu_s");
        Integer ID;
        //mysql drivers for connection
        String JDBC_DRIVER="com.mysql.jdbc.Driver";
        String DB_URL="jdbc:mysql://localhost/fingerprint";

        //  Database credentials
        //shinigami (petsas) username and password for mysql database
        String USER = "root";
        String PASS = "123qwe!@#QWE";

        //my local test pc pass
        //        String PASS = "mazahaka";
        try {

            Connection con;
            Class.forName("com.mysql.jdbc.Driver");

            // Open a connection to mysql database
            con = DriverManager.getConnection(DB_URL,USER,PASS);
            Statement stmt = con.createStatement();

            String query;
            query ="SELECT * FROM "
                        +"max_run_id";
            ResultSet results = stmt.executeQuery(query);
            results.next();
            ID = Integer.parseInt( results.getString( "ID" ) );
            Integer tmp = ID;
            ID++;
            query = "update max_run_id set ID = '"+ ID + "' where ID = '"+tmp+"'";
            stmt.executeUpdate(query);

            //write response html page
            //with user run id
            response.setContentType("text/html");


            PrintWriter out = response.getWriter();

            out.println("<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "  <title>Fingerprint</title>\n" +
                "  <meta charset=\"UTF-8\">\n" +
                "  <link href = \"style.css\" rel=\"stylesheet\">\n" +
                "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "</head>\n" +
                "\n" +
                "<body>\n" +
                "  <div id=\"ggpu\"></div>\n" +
                "  <script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js\"></script><!-- Jquery-->\n" +
                "<script src=\"core-estimator-master/core-estimator.min.js\"></script><!--Cpu library-->\n" +
                "<script src=\"three.js.original/build/three.min.js\"></script><!--Gpu library-->\n" +
                "<script src=\"three.js.original/examples/js/Detector.js\"></script><!--Gpu library-->\n" +
                "<!--<script src=\"libraries/stats.js-r9/src/Stats.js\"></script>-->\n" +
                "<script src=\"benchmark/libraries/d3.v3/d3_002.js\"></script><!--Gpu library-->\n" +
                "<script src=\"benchmark/libraries/d3.tip.min.js\"></script><!--Gpu library-->\n" +
                "<script src=\"benchmark/libraries/stats.js-r9/src/Stats.js\"></script><!--Gpu library-->\n" +
                "\n" +
                "  <div id=\"page\">\n" +
                "  <header><br><h1>Fingerprint</h1></header>\n" +
                "  <nav><a href=\"index.html\">Home</a>\n" +
                "      <a href=\"data.html\">Data</a>\n" +
                "      <a href=\"about.html\">About</a>\n" +
                "      <a href=\"contact.html\">Contact</a>\n" +
                "      </nav>\n" +
                "  <section>\n" +
                "    <figure>\n" +
                "      <div id = \"right\">\n" +
                "        <img src=\"images/fingerprint.png\" alt=\"fingerpirnt\" >\n" +
                "      </div>\n" +
                "    </figure>\n" +
                "<h4>RUN ID is: "+ID+"</h4>"+
                "    <h4>To run all fingerprint tests press button: <input type=\"button\" onClick=\"run_benchmarks();return true;\" value=\"Run\"><br>\n" +
                "    <h4> To run fast test :<input type =\"button\" onClick=\"run_fast_mode();return true;\" value=\"Press\"><br>\n"+
                "      <form action=\"/fingerprint/Servlet\" method=\"POST\" id=\"file_form\">\n" +
                "        <input type=\"text\" name=\"id_run\" value=\"0\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"loc_dev\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"loc_var\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"loc_med\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"loc_rep\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"loc_w\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"nonl_mean\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"nonl_dev\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"nonl_var\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"nonl_med\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"nonl_rep\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"nonl_w\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"fib_mean\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"fib_dev\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"fib_var\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"fib_med\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"fib_rep\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"fib_w\" value=\"\" hidden=\"hidden\">\n" +
                // "\n" +
                // "        <input type=\"text\" name=\"nonz_totalf\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"nonz_fobject\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"nonz_finaltime\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"nonz_min\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"nonz_max\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"nonz_avg\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"nonz_stats\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"nonz_start\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"nonz_step\" value=\"\" hidden=\"hidden\">\n" +
                // "\n" +
                // "        <input type=\"text\" name=\"z_totalf\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"z_fobject\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"z_finaltime\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"z_min\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"z_max\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"z_avg\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"z_stats\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"z_start\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"z_step\" value=\"\" hidden=\"hidden\">\n" +
                // "\n" +
                // "        <input type=\"text\" name=\"cube_total\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"cube_fps\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"cube_ft\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"cube_time\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"cube_add\" value=\"\" hidden=\"hidden\">\n" +
                // "\n" +
                // "        <input type=\"text\" name=\"cpu_cor\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"cpu_w\" value=\"\" hidden=\"hidden\">\n" +
                // "        <input type=\"text\" name=\"cpu_s\" value=\"\" hidden=\"hidden\">\n" +
                 "      To get script press:<input type=\"submit\" value=\"Script\">\n" +
                "      </form>\n" +
                "      state of run:<br><div id=\"state\">Ready</div><br>\n" +
                "      results:<br><div id=\"results\"></div></h4>\n" +
                "      <input type=\"button\" onClick=\"run_locality(false)\" value=\"Locality\">\n" +
                "\n" +
                "      <div id=\"set\">\n" +
                "      LOOP:<div id=\"set2\">\n" +
                "      Start:<input type=\"number\" id=\"loc_loop_start\" min=\"0\" step=\"1\" value=\"1000\">\n" +
                "      End:<input type=\"number\" id=\"loc_loop_end\" min=\"0\" step=\"1\" value=\"3000\">\n" +
                "      Step:<input type=\"number\" id=\"loc_loop_step\" min=\"0\" step=\"1\" value=\"250\"></div>\n" +
                "      DEEP:<div id=\"set2\">\n" +
                "      Start:<input type=\"number\" id=\"loc_deep_start\" min=\"0\" step=\"1\" value=\"1000\">\n" +
                "      End:<input type=\"number\" id=\"loc_deep_end\" min=\"0\" step=\"1\" value=\"3000\">\n" +
                "      Step:<input type=\"number\" id=\"loc_deep_step\" min=\"0\" step=\"1\" value=\"250\"></div></div>\n" +
                "\n" +
                "      <input type=\"button\" onClick=\"run_nonlocality(false)\" value=\"Non Locality\">\n" +
                "\n" +
                "      <div id=\"set\">\n" +
                "      LOOP:<div id=\"set2\">\n" +
                "      Start:<input type=\"number\" id=\"non_loop_start\" min=\"0\" step=\"1\" value=\"1000\">\n" +
                "      End:<input type=\"number\" id=\"non_loop_end\" min=\"0\" step=\"1\" value=\"3000\">\n" +
                "      Step:<input type=\"number\" id=\"non_loop_step\" min=\"0\" step=\"1\" value=\"250\"></div>\n" +
                "      DEEP:<div id=\"set2\">\n" +
                "      Start:<input type=\"number\" id=\"non_deep_start\" min=\"0\" step=\"1\" value=\"1000\">\n" +
                "      End:<input type=\"number\" id=\"non_deep_end\" min=\"0\" step=\"1\" value=\"3000\">\n" +
                "      Step:<input type=\"number\" id=\"non_deep_step\" min=\"0\" step=\"1\" value=\"250\"></div></div>\n" +
                "\n" +
                "      <input type=\"button\" onClick=\"run_fibonacci(false)\" value=\"Fibonacci\">\n" +
                "\n" +
                "      <div id=\"set\">\n" +
                "      LOOP:<div id=\"set2\">\n" +
                "      Start:<input type=\"number\" id=\"fib_loop_start\" min=\"0\" step=\"1\" value=\"1000\">\n" +
                "      End:<input type=\"number\" id=\"fib_loop_end\" min=\"0\" step=\"1\" value=\"3000\">\n" +
                "      Step:<input type=\"number\" id=\"fib_loop_step\" min=\"0\" step=\"1\" value=\"250\"></div>\n" +
                "      DEEP:<div id=\"set2\">\n" +
                "      Start:<input type=\"number\" id=\"fib_deep_start\" min=\"0\" step=\"1\" value=\"1000\">\n" +
                "      End:<input type=\"number\" id=\"fib_deep_end\" min=\"0\" step=\"1\" value=\"3000\">\n" +
                "      Step:<input type=\"number\" id=\"fib_deep_step\" min=\"0\" step=\"1\" value=\"250\"></div></div>\n" +
                "\n" +
                "      <input type=\"button\" onClick=\"GPU_TEST(false,false,-1)\" value=\"GPU Non zbuf\">\n" +
                "\n" +
                "      <div id=\"set\">\n" +
                "      START:<div id=\"set2\">\n" +
                "      Start:<input type=\"number\" id=\"gstart_start\" min=\"0\" step=\"1\" value=\"1300\">\n" +
                "      End:<input type=\"number\" id=\"gstart_end\" min=\"0\" step=\"1\" value=\"2800\">\n" +
                "      Step:<input type=\"number\" id=\"gstart_step\" min=\"0\" step=\"1\" value=\"300\"></div>\n" +
                "      STEP:<div id=\"set2\">\n" +
                "      Start:<input type=\"number\" id=\"gstep_start\" min=\"0\" step=\"1\" value=\"50\">\n" +
                "      End:<input type=\"number\" id=\"gstep_end\" min=\"0\" step=\"1\" value=\"100\">\n" +
                "      Step:<input type=\"number\" id=\"gstep_step\" min=\"0\" step=\"1\" value=\"10\"></div></div>\n" +
                "\n" +
                "      <input type=\"button\" onClick=\"GPU_TEST(true,false,-1)\" value=\"GPU zbuf\">\n" +
                "\n" +
                "      <div id=\"set\">\n" +
                "      START:<div id=\"set2\">\n" +
                "      Start:<input type=\"number\" id=\"g2start_start\" min=\"0\" step=\"1\" value=\"1300\">\n" +
                "      End:<input type=\"number\" id=\"g2start_end\" min=\"0\" step=\"1\" value=\"2800\">\n" +
                "      Step:<input type=\"number\" id=\"g2start_step\" min=\"0\" step=\"1\" value=\"300\"></div>\n" +
                "      STEP:<div id=\"set2\">\n" +
                "      Start:<input type=\"number\" id=\"g2step_start\" min=\"0\" step=\"1\" value=\"50\">\n" +
                "      End:<input type=\"number\" id=\"g2step_end\" min=\"0\" step=\"1\" value=\"100\">\n" +
                "      Step:<input type=\"number\" id=\"g2step_step\" min=\"0\" step=\"1\" value=\"10\"></div></div>\n" +
                "\n" +
                "      <input type=\"button\" onClick=\"GPU_CUBES(false,-1,-1)\" value=\"GPU cubes\">\n" +
                "\n" +
                "      <div id=\"set\">\n" +
                "      TIME (milliseconds):<div id=\"set2\">\n" +
                "      Start:<input type=\"number\" id=\"ctime_start\" min=\"0\" step=\"1\" value=\"5000\">\n" +
                "      End:<input type=\"number\" id=\"ctime_end\" min=\"0\" step=\"1\" value=\"25000\">\n" +
                "      step:<input type=\"number\" id=\"ctime_step\" min=\"0\" step=\"1\" value=\"2500\"></div>\n" +
                "      STEP:<div id=\"set2\">\n" +
                "      Start:<input type=\"number\" id=\"cstep_start\" min=\"0\" step=\"1\" value=\"10\">\n" +
                "      End:<input type=\"number\" id=\"cstep_end\" min=\"0\" step=\"1\" value=\"70\">\n" +
                "      Step:<input type=\"number\" id=\"cstep_step\" min=\"0\" step=\"1\" value=\"10\"></div></div>\n" +
                "\n" +
                "      <input type=\"button\" onClick=\"cpu_cores()\" value=\"CPU\">\n" +
                "\n" +
                "      <div id=\"set\">\n" +
                "      SAMPLES:<div id=\"set2\">\n" +
                "      Start:<input type=\"number\" id=\"cpu_samples_start\" min=\"0\" step=\"1\" value=\"40\">\n" +
                "      End:<input type=\"number\" id=\"cpu_samples_end\" min=\"0\" step=\"1\" value=\"70\">\n" +
                "      step:<input type=\"number\" id=\"cpu_samples_step\" min=\"0\" step=\"1\" value=\"5\"></div>\n" +
                "      WORKERS:<div id=\"set2\">\n" +
                "      Start:<input type=\"number\" id=\"cpu_workers_start\" min=\"0\" step=\"1\" value=\"16\">\n" +
                "      End:<input type=\"number\" id=\"cpu_workers_end\" min=\"0\" step=\"1\" value=\"20\">\n" +
                "      Step:<input type=\"number\" id=\"cpu_workers_step\" min=\"0\" step=\"1\" value=\"4\"></div></div>\n" +
                "\n" +
                "      <form action=\"/fingerprint/Servlet\" method=\"POST\" id=\"sub_form\">\n" +
                "        <input type=\"text\" id=\"id_run\" name=\"id_run\" value=\""+ID+"\" hidden=\"hidden\">\n" +
                "      <input type=\"text\" id=\"loc_mean\" name=\"loc_mean\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"loc_dev\" name=\"loc_dev\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"loc_var\" name=\"loc_var\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"loc_med\" name=\"loc_med\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"loc_rep\" name=\"loc_rep\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"loc_w\" name=\"loc_w\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"nonl_mean\" name=\"nonl_mean\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"nonl_dev\" name=\"nonl_dev\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"nonl_var\" name=\"nonl_var\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"nonl_med\" name=\"nonl_med\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"nonl_rep\" name=\"nonl_rep\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"nonl_w\" name=\"nonl_w\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"fib_mean\" name=\"fib_mean\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"fib_dev\" name=\"fib_dev\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"fib_var\" name=\"fib_var\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"fib_med\" name=\"fib_med\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"fib_rep\" name=\"fib_rep\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"fib_w\" name=\"fib_w\" value=\"\" >\n" +
                "\n" +
                "      <input type=\"text\" id=\"nonz_totalf\" name=\"nonz_totalf\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"nonz_fobject\" name=\"nonz_fobject\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"nonz_finaltime\" name=\"nonz_finaltime\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"nonz_min\" name=\"nonz_min\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"nonz_max\" name=\"nonz_max\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"nonz_avg\" name=\"nonz_avg\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"nonz_stats\" name=\"nonz_stats\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"nonz_start\" name=\"nonz_start\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"nonz_step\" name=\"nonz_step\" value=\"\" >\n" +
                "\n" +
                "      <input type=\"text\" id=\"z_totalf\" name=\"z_totalf\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"z_fobject\" name=\"z_fobject\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"z_finaltime\" name=\"z_finaltime\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"z_min\" name=\"z_min\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"z_max\" name=\"z_max\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"z_avg\" name=\"z_avg\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"z_stats\" name=\"z_stats\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"z_start\" name=\"z_start\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"z_step\" name=\"z_step\" value=\"\" >\n" +
                "\n" +
                "      <input type=\"text\" id=\"cube_total\" name=\"cube_total\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"cube_fps\" name=\"cube_fps\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"cube_ft\" name=\"cube_ft\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"cube_time\" name=\"cube_time\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"cube_add\" name=\"cube_add\" value=\"\" >\n" +
                "\n" +
                "      <input type=\"text\" id=\"cpu_cor\" name=\"cpu_cor\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"cpu_w\" name=\"cpu_w\" value=\"\" >\n" +
                "      <input type=\"text\" id=\"cpu_s\" name=\"cpu_s\" value=\"\" >\n" +
                "      <input type=\"submit\" id=\"Sub_button\" value=\"Submit\">\n" +
                "      </form>\n" +
                "      <script>\n" +
                "      $(document).ready(function () {\n" +
                "        $('#sub_form').on('submit', function(e) {\n" +
                "            e.preventDefault();\n" +
                "            $.ajax({\n" +
                "                url : $(this).attr('action'),\n" +
                "                type: $(this).attr('method'),\n" +
                "                data: $(this).serialize(),\n" +
                "                success: function (data) {\n" +
                "                },\n" +
                "                error: function (jXHR, textStatus, errorThrown) {\n" +
                "                    alert(errorThrown);\n" +
                "                }\n" +
                "            });\n" +
                "        });\n" +
                "        });\n" +
                "      </script>\n" +
                "  </section>\n" +
                "  <footer><p><strong>This page made by ics forth</strong></p>\n" +
                "  </footer>\n" +
                "  </div>\n" +
//                "  <script src=\"finger_sep.js\"></script>\n" +
		  "  <script src=\"fig_2.js\"></script>\n"+
                "</body>\n" +
                "</html>");

        processRequest(request, response);
      } catch (SQLException | ClassNotFoundException ex) {
          Logger.getLogger(Page.class.getName()).log(Level.SEVERE, null, ex);
      }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
