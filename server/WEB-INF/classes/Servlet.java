/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Alexander
 */
public class Servlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
 //       response.setContentType("text/html;charset=UTF-8");
//        try(PrintWriter out = response.getWriter()){
//            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet Servlet</title>");
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>Servlet NewServlet at " + request.getContextPath() + "</h1>");
//            out.println("</body>");
//            out.println("</html>");
//        }

    }
    private String RamSummary(String ram){
//        System.out.println("Ram recieved is:"+ram+"and substring is:"+ram.substring(0, ram.indexOf(" ")-1)+" number is:");
        String tmp;
        if(ram.indexOf(" ") == -1){
            tmp = ram;
            ram = "";
        }else{
            tmp = ram.substring(0, ram.indexOf(" "));
        }
        Long k = Long.parseLong(tmp);

        if( (ram.length()-2) > ram.indexOf(" ")){
            ram = ram.substring(ram.indexOf(" ")+2 , ram.length());
            if(ram.length() > 2){
                k+=Long.parseLong(ram.substring(0,ram.indexOf(" ")));
            }
        }
            k= k/1048576;
            ram = k.toString()+"MiB";
        return ram;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String userAgent = request.getHeader("User-Agent");

//        int a = userAgent.indexOf("(", 0);
//        int b = userAgent.indexOf(")",a);
//        userAgent = userAgent.subSequence(a+1, b).toString();
//        if(userAgent.contains("windows") || userAgent.contains("Windows") || userAgent.contains("WINDOWS")){
//          if(userAgent.contains("64")){
//              System.out.println("Windows x64\n");
//          }else{
//              System.out.println("Windows x32\n");
//          }
//        }else if(userAgent.contains("Linux") || userAgent.contains("linux") || userAgent.contains("LINUX")){
//            if(userAgent.contains("64")){
//              System.out.println("Linux x64\n");
//            }else{
//              System.out.println("Linux x32\n");
//            }
//        }else if(userAgent.contains("os x") || userAgent.contains("osx") || userAgent.contains("OS X") || userAgent.contains("OSX")){
//            if(userAgent.contains("32") || userAgent.contains("686")){
//              System.out.println("OS X x32\n");
//            }else{
//              System.out.println("OS X x64\n");
//            }
//        }else{
//            System.out.println("OS Unknow\n");
//        }
        System.out.println("User Agent: "+userAgent+ " end\n");
        String ip = request.getRemoteAddr();
//        String ip = request.getRemoteAddr();
//        System.out.println("Ip: "+ip+"end\n");
//        String sip = request.getHeader("X-FORWARDED-FOR");
//        System.out.println("SIp:"+sip+"end\n");
//        sip = request.getHeader("Proxy-Client-IP");
//        System.out.println("SIp:"+sip+"end\n");
//        sip = request.getHeader("WL-Proxy-Client-IP");
//        System.out.println("SIp:"+sip+"end\n");
//        sip = request.getHeader("HTTP_CLIENT_IP");
//        System.out.println("SIp:"+sip+"end\n");
//        sip = request.getHeader("HTTP_X_FORWARDED_FOR");
//        System.out.println("SIp:"+sip+"end\n");
//        if(sip!= null && !sip.isEmpty()){
//            ip=ip+"-"+sip;
//        }else{
//            ip=ip+"-none";
//        }
        String cpu = request.getParameter("cpu");
        String gpu = request.getParameter("gpu");
        String ram = request.getParameter("ram");
        String ramSpeed = request.getParameter("ramspeed");
        String os = request.getParameter("os");
        String arch = request.getParameter("arch");
//        String con_address = "jdbc:derby://localhost"+":1527/"+
//                "fingerprint";
        String JDBC_DRIVER="com.mysql.jdbc.Driver";
        String DB_URL="jdbc:mysql://localhost/fingerprint";

        //  Database credentials
        String USER = "root";
        String PASS = "123qwe!@#QWE";

        try {
            Connection con;
            //con = DriverManager.getConnection(con_address, "root", "toor");
            Class.forName("com.mysql.jdbc.Driver");

            // Open a connection
            con = DriverManager.getConnection(DB_URL,USER,PASS);
            Statement stmt = con.createStatement();
            if(cpu!=null && gpu!=null && ram!=null && os!=null && arch!=null){
                if(os.contains("Windows") || os.contains("windows") || os.contains("OSX") || os.contains("osx")){
                    ram = RamSummary(ram);
                }
                if(arch.contains("64")){
                    arch="64-bit";
                }else if(arch.contains("32")){
                    arch="32-bit";
                }
                if(ramSpeed.length()>0 && ramSpeed.indexOf(" ") != -1){
                    ramSpeed = ramSpeed.substring(0,ramSpeed.indexOf(" "));
                }
                if(ramSpeed.length()>0 && !ramSpeed.contains("MHz")){
                    ramSpeed+="MHz";
                }
                String query ="INSERT INTO "
                            +"hardware_info" +
                            " (CPU, GPU, RAM, RAMSPEED, OS, ARCH, IP) "
                            + " VALUES ("
                            + "'" + cpu + "',"
                            + "'"+gpu+"',"
                            +"'"+ram+"',"
                            +"'"+ramSpeed+"',"
                            +"'"+os+"',"
                            +"'"+arch+"',"
                            +"'"+ip+"')";

                stmt.executeUpdate(query);
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Servlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        response.setContentType("text/html;charset=UTF-8");
        try(PrintWriter out = response.getWriter()){
            /* TODO output your page here. You may use following sample code. */

            out.println("Phase 2 Done Thanks you!");

        }
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String run_id = request.getParameter("id_run");
        //if ID_RUN is zero that mean that test is done and all data have been send already
        if( run_id.equalsIgnoreCase("0") ){
          //in that case we response with html that have link to our script for hardware info

          String userAgent = request.getHeader("User-Agent");
          int a = userAgent.indexOf("(", 0);
          int b = userAgent.indexOf(")",a);
          String OS,os_data,os_arch;
          OS="";
          os_data="";
          os_arch="";
          String userAgent_tmp = userAgent.subSequence(a+1, b).toString();
          if(userAgent_tmp.contains("windows") || userAgent_tmp.contains("Windows") || userAgent_tmp.contains("WINDOWS")){
            if(userAgent_tmp.contains("64")){
                System.out.println("Windows x64\n");
                OS="Windows x64";
                os_data="windows";
                os_arch="64";
            }else{
                OS="Windows x86";
                os_data="windows";
                os_arch="32";
                System.out.println("Windows x32\n");
            }
          }else if(userAgent_tmp.contains("Linux") || userAgent_tmp.contains("linux") || userAgent_tmp.contains("LINUX")){
              if(userAgent_tmp.contains("64")){
                System.out.println("Linux x64\n");
                OS="Linux x64";
                os_data="linux";
                os_arch="64";
              }else{
                System.out.println("Linux x32\n");
                OS="Linux x32";
                os_data="linux";
                os_arch="32";
              }
          }else if(userAgent_tmp.contains("os x") || userAgent_tmp.contains("osx") || userAgent_tmp.contains("OS X") || userAgent_tmp.contains("OSX")){
              if(userAgent_tmp.contains("32") || userAgent_tmp.contains("686")){
                System.out.println("OS X x32\n");
                OS="OS X x86";
                os_data="os x";
                os_arch="32";
              }else{
                System.out.println("OS X x64\n");
                OS="OS X x64";
                os_data="mac";
                os_arch="64";
              }
          }else{
              System.out.println("OS Unknow\n");
          }
          System.out.println("User Agent: "+userAgent+ " end\n");

          response.setContentType("text/html");
          PrintWriter out = response.getWriter();

          out.println("<!DOCTYPE html>\n" +
                      "<html>\n" +
                      "<head>\n" +
                      "  <title>Fingerprint</title>\n" +
                      "  <meta charset=\"UTF-8\">\n" +
                      "  <link href = \"style.css\" rel=\"stylesheet\">\n" +
                      "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                      "</head>\n" +
                      "\n" +
                      "<body>\n" +
                      "  <div id=\"page\">\n" +
                      "  <header><br><h1>Fingerprint</h1></header>\n" +
                      "  <nav><a href=\"index.html\">Home</a>\n" +
                      "      <a href=\"about.html\">About</a>\n" +
                      "      <a href=\"contact.html\">Contact</a>\n" +
                      "      <a href=\"stats.html\">Statistics</a>\n" +
                      "      </nav>\n" +
                      "  <section>\n" +
                      "    <figure>\n" +
                      "      <div id = \"right\">\n" +
                      "        <img src=\"images/fingerprint.png\" alt=\"fingerpirnt\" >\n" +
                      "      </div>\n" +
                      "    </figure>\n");
          out.println("<h4>Step 1 done ! Now download and execute simple script , that is the last step.<br>To download script for:"+OS+
                  " press button:<form action=\"/fingerprint/File\" method=\"POST\">"+
                  "<input type=\"text\" name=\"os\" value=\""+os_data+"\""+
                  " hidden=\"hidden\""+
                  ">"+
                  "<input type=\"text\" name=\"arch\" value=\""+os_arch+"\""+
                  " hidden=\"hidden\""+
                  ">"+
                  "<input type=\"submit\" value=\"Download\">"+
                  "</form>");
          out.println("<br>Or type:<form action=\"/fingerprint/File\" method=\"POST\">"+
                  "OS :<input type=\"text\" name=\"os\" value=\"\"> (example : windows, linux or mac)<br>"+
                  "Arch :<input type=\"text\" name=\"arch\" value=\"\"> (example : 64 or 32)<br>"+
                  "<input type=\"submit\" value=\"Download\">"+
                  "</form>");
          out.println("  </section>\n" +
              "  <footer><p><strong>This page made by ics forth</strong></p>\n" +
              "  </footer>\n" +
              "  </div>\n" +
              "</body>\n" +
              "</html>\n" +
              "");
          processRequest(request, response);

        }else{
          //if id_run is not a zero that mean that we need insert data to our database
          String userAgent = request.getHeader("User-Agent");
          //data for time stamp
          Calendar calendar = Calendar.getInstance();
          java.util.Date now = calendar.getTime();
          java.sql.Timestamp timeStp = new java.sql.Timestamp(now.getTime());
          String stamp = timeStp.toString();

          String ip = request.getRemoteAddr();
          System.out.println("Ip: "+ip+"end\n");

          String loc_mean = request.getParameter("loc_mean");
          String loc_dev = request.getParameter("loc_dev");
          String loc_var = request.getParameter("loc_var");
          String loc_med = request.getParameter("loc_med");
          String loc_rep = request.getParameter("loc_rep");
          String loc_dp = request.getParameter("loc_w");
          System.out.println("locality: mean:"+loc_mean+
                  ", dev:"+loc_dev+", var:"+loc_var+
                  ", med:"+loc_med+", loop:"+loc_rep+", deep:"+loc_dp+"\n");

          String nonloc_mean = request.getParameter("nonl_mean");
          String nonloc_dev = request.getParameter("nonl_dev");
          String nonloc_var = request.getParameter("nonl_var");
          String nonloc_med = request.getParameter("nonl_med");
          String nonloc_rep = request.getParameter("nonl_rep");
          String nonloc_dp = request.getParameter("nonl_w");

          String fib_mean = request.getParameter("fib_mean");
          String fib_dev = request.getParameter("fib_dev");
          String fib_var = request.getParameter("fib_var");
          String fib_med = request.getParameter("fib_med");
          String fib_rep = request.getParameter("fib_rep");
          String fib_dp = request.getParameter("fib_w");

          String nonz_totalf = request.getParameter("nonz_totalf");
          String nonz_fobject = request.getParameter("nonz_fobject");
          String nonz_finaltime = request.getParameter("nonz_finaltime");
          String nonz_min = request.getParameter("nonz_min");
          String nonz_max = request.getParameter("nonz_max");
          String nonz_avg = request.getParameter("nonz_avg");
          String nonz_stats = request.getParameter("nonz_stats");
          String nonz_start = request.getParameter("nonz_start");
          String nonz_step = request.getParameter("nonz_step");

          String z_totalf = request.getParameter("z_totalf");
          String z_fobject = request.getParameter("z_fobject");
          String z_finaltime = request.getParameter("z_finaltime");
          String z_min = request.getParameter("z_min");
          String z_max = request.getParameter("z_max");
          String z_avg = request.getParameter("z_avg");
          String z_stats = request.getParameter("z_stats");
          String z_start = request.getParameter("z_start");
          String z_step = request.getParameter("z_step");

          String cube_total = request.getParameter("cube_total");
          String cube_fps = request.getParameter("cube_fps");
          String cube_ft = request.getParameter("cube_ft");
          String cube_time = request.getParameter("cube_time");
          String cube_add = request.getParameter("cube_add");

          String cpu_cores = request.getParameter("cpu_cor");
          String cpu_workers = request.getParameter("cpu_w");
          String cpu_sample = request.getParameter("cpu_s");

          //String con_address = "jdbc:derby://localhost"+":1527/"+
          //        "fingerprint";
  //      String JDBC_DRIVER="com.mysql.jdbc.Driver";
  //      String DB_URL="jdbc:mysql://localhost/fingerprint";
  //
  //      //  Database credentials
  //        String USER = "root";
  //        String PASS = "mazahaka";
          String JDBC_DRIVER="com.mysql.jdbc.Driver";
          String DB_URL="jdbc:mysql://localhost/fingerprint";

          //  Database credentials
          String USER = "root";
          String PASS = "123qwe!@#QWE";
          try {
              //Class.forName("org.apache.derby.jdbc.ClientDriver");
              Connection con;
              //con = DriverManager.getConnection(con_address, "root", "toor");
              Class.forName("com.mysql.jdbc.Driver");

              // Open a connection
              con = DriverManager.getConnection(DB_URL,USER,PASS);
              Statement stmt = con.createStatement();
              String query;
              if( (!loc_mean.equalsIgnoreCase("")) && (!loc_dev.equalsIgnoreCase(""))
                      && (!loc_med.equalsIgnoreCase("")) && (!loc_dp.equalsIgnoreCase(""))
                      && (!loc_rep.equalsIgnoreCase("")) && (!loc_var.equalsIgnoreCase("")) ){
                  query ="INSERT INTO "
                              +"locality" +
                              " (TIME_STMP, IP, AGENT, MEAN, DEV, VAR, MED, DEP, REP,RUN_ID) "
                              + " VALUES ("
                              + "'" +stamp+ "','" +ip+ "','"+userAgent + "','" +loc_mean+ "','" + loc_dev + "','"
                              + loc_var + "','" + loc_med + "','" + loc_dp + "','"+ loc_rep + "','"+ run_id+  "')";

                  stmt.executeUpdate(query);
              }
              if( (!nonloc_mean.equalsIgnoreCase("")) && (!nonloc_dev.equalsIgnoreCase(""))
                      && (!nonloc_med.equalsIgnoreCase("")) && (!nonloc_dp.equalsIgnoreCase(""))
                      && (!nonloc_rep.equalsIgnoreCase("")) && (!nonloc_var.equalsIgnoreCase("")) ){
                  query ="INSERT INTO "
                              +"nonlocality" +
                              " (TIME_STMP, IP, AGENT, MEAN, DEV, VAR, MED, DEP, REP , RUN_ID) "
                              + " VALUES ("
                              + "'" +stamp+ "','" +ip+ "','"+userAgent + "','" +nonloc_mean+ "','" + nonloc_dev + "','"
                              + nonloc_var + "','" + nonloc_med + "','" + nonloc_dp + "','" + nonloc_rep + "','" + run_id + "')";

                  stmt.executeUpdate(query);
              }
              if( (!fib_mean.equalsIgnoreCase("")) && (!fib_dev.equalsIgnoreCase(""))
                      && (!fib_med.equalsIgnoreCase("")) && (!fib_dp.equalsIgnoreCase(""))
                      && (!fib_rep.equalsIgnoreCase("")) && (!fib_var.equalsIgnoreCase("")) ){
                  query ="INSERT INTO "
                              +"fibonacci" +
                              " (TIME_STMP, IP, AGENT, MEAN, DEV, VAR, MED, DEP, REP, RUN_ID) "
                              + " VALUES ("
                              + "'" +stamp+ "','" +ip+ "','"+userAgent + "','" +fib_mean+ "','" + fib_dev + "','"
                              + fib_var + "','"+ fib_med + "','" + fib_dp + "','" + fib_rep + "','"+ run_id+ "')";

                  stmt.executeUpdate(query);
              }
              if( (!z_totalf.equalsIgnoreCase("")) && (!z_fobject.equalsIgnoreCase(""))
                      && (!z_finaltime.equalsIgnoreCase("")) && (!z_min.equalsIgnoreCase(""))
                      && (!z_max.equalsIgnoreCase("")) && (!z_avg.equalsIgnoreCase(""))
                      && (!z_stats.equalsIgnoreCase("")) && (!z_start.equalsIgnoreCase(""))
                      && (!z_step.equalsIgnoreCase("")) ){
                  query ="INSERT INTO "
                              +"gpu_z" +
                              " (TIME_STMP, IP, AGENT, TOTALF, OBJF, FFT, MIN, MAX, AVE, DATA, START, STEP, RUN_ID) "
                              + " VALUES ("
                              + "'" +stamp+ "','" +ip+ "','"+userAgent + "','" +z_totalf+ "','" + z_fobject + "','"
                              + z_finaltime + "','"+ z_min + "','" + z_max + "','" +z_avg+ "','" + z_stats + "','"
                              + z_start+ "','" + z_step + "','" + run_id + "')";

                  stmt.executeUpdate(query);
              }
              if( (!nonz_totalf.equalsIgnoreCase("")) && (!nonz_fobject.equalsIgnoreCase(""))
                      && (!nonz_finaltime.equalsIgnoreCase("")) && (!nonz_min.equalsIgnoreCase(""))
                      && (!nonz_max.equalsIgnoreCase("")) && (!nonz_avg.equalsIgnoreCase(""))
                      && (!nonz_stats.equalsIgnoreCase("")) && (!nonz_start.equalsIgnoreCase(""))
                      && (!nonz_step.equalsIgnoreCase("")) ){
                  query ="INSERT INTO "
                              +"gpu_nonz" +
                              " (TIME_STMP, IP, AGENT, TOTALF, OBJF, FFT, MIN, MAX, AVE, DATA, START, STEP, RUN_ID) "
                              + " VALUES ("
                              + "'" +stamp+ "','" +ip+ "','"+userAgent + "','" +nonz_totalf+ "','" + nonz_fobject + "','"
                              + nonz_finaltime + "','" + nonz_min + "','" + nonz_max + "','"
                              + nonz_avg+ "','" + nonz_stats + "','"+nonz_start+ "','" + nonz_step + "','" + run_id + "')";

                  stmt.executeUpdate(query);
              }
              if( (!cube_total.equalsIgnoreCase("")) && (!cube_fps.equalsIgnoreCase(""))
                      && (!cube_ft.equalsIgnoreCase("")) && (!cube_time.equalsIgnoreCase(""))
                      && (!cube_add.equalsIgnoreCase("")) ){
                  query ="INSERT INTO "
                              +"gpu_cube" +
                              " (TIME_STMP, IP, AGENT, TOTALO, FPS, FO, TIME, STEP, RUN_ID) "
                              + " VALUES ("
                              + "'" +stamp+ "','" +ip+ "','"+userAgent + "','" +cube_total+ "','" + cube_fps
                              + "','" + cube_ft + "','" + cube_time + "','" + cube_add + "','" + run_id + "')";

                  stmt.executeUpdate(query);
              }
              if( (!cpu_cores.equalsIgnoreCase("")) && (!cpu_sample.equalsIgnoreCase(""))
                      && (!cpu_workers.equalsIgnoreCase("")) ){
                  query ="INSERT INTO "
                              +"cpu" +
                              " (TIME_STMP, IP, AGENT, CPU_CORE, CPU_SAMPLE, CPU_WORK , RUN_ID) "
                              + " VALUES ("
                              + "'" +stamp+ "','" +ip+ "','"+userAgent + "','" +cpu_cores+ "','"
                              + cpu_sample + "','" + cpu_workers + "','" + run_id+ "')";

                  stmt.executeUpdate(query);
              }
          } catch (SQLException | ClassNotFoundException ex) {
              Logger.getLogger(Servlet.class.getName()).log(Level.SEVERE, null, ex);
          }
      }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
